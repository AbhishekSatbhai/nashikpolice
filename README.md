# Nashik Police
![Covid app gif](nashik.gif)
# Live testing demo
URL User: [Live Testing Demo](http://nashikpolice.staging.webphoenix.in/)
URL Admin: [Admin Demo](http://nashikpolice.staging.webphoenix.in/admin)

# Problem Statement
* During this COVID-19 pandemic, several citizens of Nashik wanted to help the Nashik City Police by donating a variety of different items as a repayment of their service to their city
* The problems addressed were, the police noticed that every time new items were delivered to the stations, it was hard to track which person, what day, what time, and what item was donated
* The police also didn’t know how to collect the donations while still maintaining the safety of the citizens and the police staff
* The police also noticed that they had an abundance of some items, while a lack of necessary items
* One of their bigger problems included how to maintain transparency in the system

# Proposed Solution
* The application allows donors to put in desired items with a listed quantity on a web app
* While the donors fill out the application, they can see what items and what amount are required by the police, and so donate accordingly to that data
* Application also lets citizens stay in the safety of their homes instead of individually traveling to a police station and donating their items there
* In other words, the application lets volunteers and police officers come to the doorstep of citizens’ homes and let the volunteers pick up the donations with the precautions needed to keep everyone safe
* Application also maintains transparency by being able to track the location of the items, quantity and the person responsible to maintain stock

# Objective 
* Contribute to the COVID-situation in our community by creating an efficient and effective web application for my city police

