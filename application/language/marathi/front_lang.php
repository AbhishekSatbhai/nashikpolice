<?php
$lang['Request as Provider'] = "प्रदाता किंवा देणगीदार म्हणून विनंती";
$lang['Name'] = "नाव";
$lang['Contact Number'] = "संपर्क नंबर";
$lang['Your Location'] = "आपले स्थान";
$lang['Email ID'] = "इमेल आयडी";
$lang['Your Help By'] = "आपण कोणत्या प्रकारची मदत करू इच्छिता";
$lang['Other Help'] = "इतर वस्तू";
$lang['Qty'] = "संख्या";
$lang['Where to Help?'] = "कोठे मदत करावी?";
$lang['Nearest Police Station'] = "जवळचे पोलीस स्टेशन";
$lang['Help Date'] = "मदतीची तारीख / दिनांक";
$lang['Remark'] = "टिप्पणी";
$lang['Your Identity Number'] = "तुमचा ओळख क्रमांक";
$lang['Aadhar Number or NGO registration number'] = "आधार कार्ड व इतर पुरावे";
$lang['Upload Identity Photo'] = "तुमचा ओळख पुरावा फोटो";
$lang['only PNG, JPG, JPEG format accept'] = "फक्त PNG, JPG, JPEG फॉरमॅट घेतले जातील";
$lang['Submit'] = "संपादित करा";
$lang['Pin Code'] = "पिन कोड";

$lang['Please enter Name'] = "कृपया नाव टाका";
$lang['Please enter Contact Number'] = "कृपया संपर्क नंबर टाका";
$lang['Please enter valid email address'] = "कृपया इमेल आयडी टाका";
$lang['Please select your Help'] = "कृपया आपली मदत टाका";
$lang['Please enter help count'] = "कृपया संख्या टाका";
$lang['Please enter help place'] = "कृपया कोठे मदत करावी टाका";
$lang['Please enter your ID number'] = "कृपया ओळख क्रमांक टाका";
$lang['Please upload your ID photo'] = "कृपया आधार कार्ड व इतर पुरावे टाका";
$lang['Please enter your Location'] = "कृपया आपले स्थान टाका";

$lang['Your Request Submitted Successfully'] = "तुमची विनंती पूर्ण संपादित केलेली आहे. लवकरच आम्ही तुमच्याशी संपर्क साधू, धन्यवाद!";
?>