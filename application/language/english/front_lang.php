<?php
$lang['Request as Provider'] = "Request as Provider";
$lang['Name'] = "Name";
$lang['Contact Number'] = "Contact Number";
$lang['Your Location'] = "Pickup Location";
$lang['Email ID'] = "Email ID";
$lang['Your Help By'] = "Helping For";
$lang['Other Help'] = "Item Name";
$lang['Qty'] = "Quantity";
$lang['Where to Help?'] = "Where to Help?";
$lang['Nearest Police Station'] = "Nearest Police Station";
$lang['Help Date'] = "Pickup Date ";
$lang['Remark'] = "Remark";
$lang['Your Identity Number'] = "Your Identity Number";
$lang['Aadhar Number or NGO registration number'] = "Aadhar Number or NGO registration number";
$lang['Upload Identity Photo'] = "Upload Identity Photo";
$lang['only PNG, JPG, JPEG format accept'] = "only PNG, JPG, JPEG format accept";
$lang['Submit'] = "Submit";
$lang['Pin Code'] = "Pin Code";

$lang['Please enter Name'] = "Please enter Name";
$lang['Please enter Contact Number'] = "Please enter Contact Number";
$lang['Please enter valid email address'] = "Please enter valid email address";
$lang['Please select your Help'] = "Please select your Help";
$lang['Please enter help count'] = "Please enter help count";
$lang['Please enter help place'] = "Please enter help place";
$lang['Please enter your ID number'] = "Please enter your ID number";
$lang['Please upload your ID photo'] = "Please upload your ID photo";
$lang['Please enter your Location'] = "Please enter your Location";

$lang['Your Request Submitted Successfully'] = "Your Request Submitted Successfully.";
?>