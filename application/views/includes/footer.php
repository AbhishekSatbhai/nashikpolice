<footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h3>Tips</h3>
                        <ul class="list-unstyled">
							<li><a href="<?php echo base_url()?>">Request as Provider</a></li>
                            <li><a href="<?php echo base_url()?>aboutus">About</a></li>
                            <li><a href="<?php echo base_url()?>contactus">Contact</a></li>
                        </ul>
                    </div>
                    <?php /*<div class="col-md-3">
                        <h3>Important links</h3>
                        <ul class="list-unstyled">
                            <li><a href="<?php echo base_url()?>">Request For Food</a></li>
                            <li><a href="<?php echo base_url()?>apply_for_provider">Request as Provider</a></li>
							<li><a href="<?php echo base_url()?>apply_for_delivery_person">Request As Delivery Person</a></li>
                        </ul>
                    </div>*/?>
                    <div class="col-md-2">
                        <h3>Emergency Contacts</h3>
                        <!-- <ul class="list-unstyled">
                            <li><a href="tel:9403165140">+91 9403165140</a></li>
                            <li><a href="tel:9373800019">+91 9373800019</a></li>
							<li><a href="tel:7350166999">+91 7350166999</a></li>
							<li><a href="tel:7248922877">+91 7248922877</a></li>
							<li><a href="tel:9403165132">+91 9403165132</a></li>
							<li><a href="tel:8485810477">+91 8485810477</a></li>
                        </ul> -->
                        <p class="list-unstyled" style="color:white;">8055775242 <br> API Ahire Sir <br> (Training Branch, Nashik City)</p>
                    </div>
					
					<div class="col-md-2">
                        <h3>Emergency Contacts</h3>
                        <!-- <ul class="list-unstyled">
							<li><a href="tel:7709295534">+91 7709295534</a></li>
							<li><a href="tel:7248906877">+91 7248906877</a></li>
							<li><a href="tel:7058946877">+91 7058946877</a></li>
							<li><a href="tel:7248903877">+91 7248903877</a></li>
							<li><a href="tel:7020583176">+91 7020583176</a></li>
                        </ul> -->
                        <p class="list-unstyled" style="color:white;">7558262754 <br> Darshan Sir <br> (Police Welfare Branch, Nashik City)</p>
                    

                    </div>
					
					<div class="col-md-2">
						<h3>Industrial Permission</h3>
                        <ul class="list-unstyled">
                            <li><a href="tel:02532971233">0253 2971233</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2">
                        <h3>Follow us</h3>
                        <ul class="list-inline social">
                            <li><a href="https://www.facebook.com/nashikcitypolice"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/nashikpolice?s=09"><i class="fa fa-twitter"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <span style="color:#fff !important;">&copy; 2020 Nashik City Police. All right reserved.</span><br>
                        <span style="color:#44484G !important;">Design and Developed By Webphoenix & Weeamo Technologies , Website Powered By Nashik Police.</span>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?php echo FRONT_ASSETS; ?>js/jquery.min.js"></script>
        <script src="<?php echo FRONT_ASSETS; ?>bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo FRONT_ASSETS; ?>flex-slider/jquery.flexslider.js"></script>
        <script src="<?php echo FRONT_ASSETS; ?>js/custom.js"></script>
		<script src="<?php echo ASSETS;?>jquery_validation/js/jquery.validate.js?ver=<?php echo VERSION;?>"></script>
		<link href="<?php echo ASSETS;?>jquery_validation/css/cmxform.css?ver=<?php echo VERSION;?>" rel="stylesheet">