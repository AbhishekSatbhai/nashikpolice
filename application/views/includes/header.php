<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo SITE_TITLE;?></title>
        <!-- Bootstrap -->
        <link href="<?php echo FRONT_ASSETS; ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo FRONT_ASSETS; ?>font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo FRONT_ASSETS; ?>flex-slider/flexslider.css" rel="stylesheet">
        <link href="<?php echo FRONT_ASSETS; ?>css/style.css?ver=1.3" rel="stylesheet">
		
		<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162958374-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-162958374-1');
        </script>
    </head>
    <body>
	
		<div class="loader" style="display:none;"></div>
		
        <div class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <ul class="list-inline">
                            <li><a href="mailto:raikar.rahul@gmail.com"><i class="fa fa-copyright"></i> Powered By Nashik City Police</a></li>

                        </ul>
                    </div>
                    <div class="col-sm-8 text-right">
                        <a href="<?php echo base_url()?>index/change_language/english">
							English
                        </a>
						<a href="javascript:void(0);">
							|
                        </a>
						<a href="<?php echo base_url()?>index/change_language/marathi">
							मराठी
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Static navbar -->
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo FRONT_ASSETS; ?>images/Nashik-Police-logo.jpg" style="width: 153px;" alt=""></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo base_url()?>">Help(Donate)</a></li>
                        <li><a href="<?php echo base_url()?>aboutus">About</a></li>
                        <li><a href="<?php echo base_url()?>contactus">Contact</a></li>
						<li><a href="http://corona.nashikcitypolice.gov.in/" target="_blank">Emergency Permission</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>
		
		<?php /*<div class="popular-sectors" style="background: #003663;">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-6">
						<a href="<?php echo base_url()?>">
							<i class="fa fa-cutlery"></i>
							<h3>Request for food</h3>
						</a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
						<a href="<?php echo base_url()?>apply_for_provider">
							<i class="fa fa-money"></i>
							<h3>Request as provider or Doner</h3>
						</a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
						<a href="<?php echo base_url()?>apply_for_delivery_person">
							<i class="fa fa-user"></i>
							<h3>Request as delivery Person</h3>
						</a>
                    </div>
                </div>
            </div>
		</div>*/?>
		
		<?php if($this->session->flashdata('success_message')!=null){?>
		<div class="row">
			<div class="container">
				<div class="col-sm-12">
					<div class="alert alert-success text-center" style="margin-top:20px;">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<?php echo $this->session->flashdata('success_message');?>
					</div>
				</div>
			</div>
		</div>
		<?php }?>