<?php $this->load->view('includes/header');?>		
	<div class="intro">
		<div class="container">
			<div class="row">
				<div class="col-sm-5">
					<img src="<?php echo FRONT_ASSETS; ?>images/Nashik-Police-logo.jpg" class="img-responsive auto-img" alt="">
					<img src="<?php echo FRONT_ASSETS; ?>images/info3.jpeg" style="width: 100%;" alt="">

				</div>
				<div class="col-sm-7">
					<h2 class="section-title"><strong>Welcome</strong> at Nashik City Police</h2>
					<img src="<?php echo FRONT_ASSETS; ?>images/about.JPEG" class="img-responsive auto-img" alt="">
				</div>
			</div>
		</div>
	</div>

	<div class="our-teachers">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 text-center">
					<div class="person yellow">
						<a data-fancybox href="<?php echo FRONT_ASSETS; ?>images/about2.jpg">
						<img src="<?php echo FRONT_ASSETS; ?>images/about2.jpg" class="img-responsive auto-img" alt="">
						</a>
					</div>
				</div>
				<div class="col-sm-3 text-center">
					<div class="person red">
						<a data-fancybox href="<?php echo FRONT_ASSETS; ?>images/about3.jpg">
						<img src="<?php echo FRONT_ASSETS; ?>images/about3.jpg" class="img-responsive auto-img" alt="">
						</a>
					</div>
				</div>
				<div class="col-sm-3 text-center">
					<div class="person green">
						<a data-fancybox href="<?php echo FRONT_ASSETS; ?>images/about4.jpg">
						<img src="<?php echo FRONT_ASSETS; ?>images/about4.jpg" class="img-responsive auto-img" alt="">
						</a>
					</div>
				</div>
				<div class="col-sm-3 text-center">
					<div class="person blue">
						<a data-fancybox href="<?php echo FRONT_ASSETS; ?>images/about5.jpg">
						<img src="<?php echo FRONT_ASSETS; ?>images/about5.jpg" class="img-responsive auto-img" alt="">
						</a>
					</div>
				</div>
				<div class="col-sm-3 text-center">
					<div class="person red">
						<a data-fancybox href="<?php echo FRONT_ASSETS; ?>images/about6.jpg">
						<img src="<?php echo FRONT_ASSETS; ?>images/about6.jpg" class="img-responsive auto-img" alt="">
						</a>
					</div>
				</div>
				<div class="col-sm-3 text-center">
					<div class="person green">
						<a data-fancybox href="<?php echo FRONT_ASSETS; ?>images/about7.jpg">
						<img src="<?php echo FRONT_ASSETS; ?>images/about7.jpg" class="img-responsive auto-img" alt="">
						</a>
					</div>
				</div>
				<div class="col-sm-3 text-center">
					<div class="person blue">
						<a data-fancybox href="<?php echo FRONT_ASSETS; ?>images/about8.jpg">
						<img src="<?php echo FRONT_ASSETS; ?>images/about8.jpg" class="img-responsive auto-img" alt="">
						</a>
					</div>
				</div>
				<div class="col-sm-3 text-center">
					<div class="person red">
						<a data-fancybox href="<?php echo FRONT_ASSETS; ?>images/about9.jpg">
						<img src="<?php echo FRONT_ASSETS; ?>images/about9.jpg" class="img-responsive auto-img" alt="">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	
<?php $this->load->view('includes/footer');?>	

<script type="text/javascript" src="<?php echo ASSETS;?>fancybox/jquery.fancybox.min.js"></script>
<link href="<?php echo ASSETS;?>fancybox/jquery.fancybox.min.css" rel="stylesheet">

</body>
</html>