<?php $this->load->view('includes/header');?>	
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<div class="course-reserve">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-6 col-sm-offset-3 request_form">
					<span class="text-center">
						<h2 class="section-title">Request For <strong>Food</strong></h2>
					</span>
					<form class="" method="post" id="registerFrm" name="registerFrm">
						
								<div class="form-group">
									<label class="control-label">Name *</label>
									<input type="text" class="form-control" name="st_name" id="st_name" value="<?php echo($this->input->post('st_name')!=NULL) ? $this->input->post('st_name') : ''?>">
									<label id="st_name-error" class="error" for="st_name" style="display:<?php echo(form_error('st_name')) ? 'block' : 'none';?>"></label>
								</div>
								<div class="form-group">
									<label class="control-label">Contact Number *</label>
									<input type="text" class="form-control" name="st_mobile_number" id="st_mobile_number" value="<?php echo($this->input->post('st_mobile_number')!=NULL) ? $this->input->post('st_mobile_number') : ''?>">
									<label id="st_mobile_number-error" class="error" for="st_mobile_number" style="display:<?php echo(form_error('st_mobile_number')) ? 'block' : 'none';?>"></label>
								</div>
								<div class="form-group">
									<label class="control-label">Location *</label>
									<input type="text" class="form-control" name="st_address" id="st_address" value="<?php echo($this->input->post('st_address')!=NULL) ? $this->input->post('st_address') : ''?>">
									<label id="st_address-error" class="error" for="st_address" style="display:<?php echo(form_error('st_address')) ? 'block' : 'none';?>"></label>
								</div>
								
								<div class="form-group">
									<label class="control-label">Number of Persons *</label>
									<input type="text" class="form-control" name="st_person" id="st_person" value="<?php echo($this->input->post('st_person')!=NULL) ? $this->input->post('st_person') : ''?>">
									<label id="st_person-error" class="error" for="st_person" style="display:<?php echo(form_error('st_person')) ? 'block' : 'none';?>"></label>
								</div>
								
								<div class="form-group">
									<label class="control-label">Nearest Police Station</label>
									<?php $st_nearest_police_station = ($this->input->post('st_nearest_police_station')!=NULL) ? $this->input->post('st_nearest_police_station') : ''?>
									<select class="form-control"  name="st_nearest_police_station" id="st_nearest_police_station" >
										<?php if(!empty($police_stations)){ foreach($police_stations as $row){?>
										<option value="<?php echo $row->in_id?>" <?php echo ($st_nearest_police_station == $row->in_id) ? 'selected' : '';?>><?php echo $row->st_name?></option>
										<?php }}?>
									</select>
									<label id="st_nearest_police_station-error" class="error" for="st_nearest_police_station" style="display:<?php echo(form_error('st_nearest_police_station')) ? 'block' : 'none';?>"></label>
								</div>
								
								<div class="control-group">
									<div class="column one g-recaptcha" data-sitekey="<?php echo GOOGLE_CAPTCHA_SITE_KEY;?>" style="transform:scale(0.85);-webkit-transform:scale(0.85);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
									<div id="robot_message" class="alert alert-danger" style="display:none;"></div>
								</div>
							
							<div class="clearfix"></div>
							<div class="text-center">
							<button type="submit" class="btn btn-blue btn-lg">Submit</button>
							
							</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	
<?php $this->load->view('includes/footer');?>	

<script>
$(function () {
	
	$("#registerFrm").validate({
		rules: {
			st_name: {
				required: true,
			},
			st_address: {
				required: true
			},					
			st_mobile_number: {
				required: true
			},
			st_person: {
				required: true
			},
		},
	
		messages: {
			st_name: "Please enter name",
			st_address: "Please enter address",
			st_mobile_number: "Enter your contact number",
			st_person: "How many of persons",
		},
		
		 submitHandler: function (form) {
			
			//var formdata = $("#frmSubmit").serialize();
			$('.loader').show();
			$('#robot_message').html('');
			$('#robot_message').hide();
			var site_url = '<?php echo base_url();?>index/action';
				
			var formData = new FormData($('#registerFrm')[0]);
				
			$.ajax({
				type:'POST',
				url : site_url,
				data: formData,
				processData: false,
				contentType: false,
				
				success: function(result_data){
					data_result_array = JSON.parse(result_data);
					if(data_result_array.result==0)
					{
						if(data_result_array.message){
							var data_result_array_message = data_result_array.message;
							for(var i in data_result_array_message)
							{
								$('#'+i+'-error').html(data_result_array_message[i]);
								$('#'+i+'-error').show();
							}
						}
						if(data_result_array.robot_message)
						{
							$('#robot_message').html(data_result_array.robot_message);
							$('#robot_message').show();
						}
						$('.loader').hide();
					}
					else
					{
						$('.loader').hide();
						window.location.href="";
						
					}

				}
			});	
		},
	});	
		
});
</script>
</body>
</html>