<?php $this->load->view('includes/header');?>	
	
	<div class="get-in-touch">
		<div class="container">
			 <h2 class="section-title"><strong>Get</strong> in touch</h2>
					<p class="lead">Contact us to get help</p>
					<div style="height: 30px"></div>
			<div class="row">
				<div class="col-md-6">
				   
					<form class="" method="post" id="registerFrm" name="registerFrm">
						<div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<input type="text" class="form-control" name="st_name" id="st_name" value="<?php echo($this->input->post('st_name')!=NULL) ? $this->input->post('st_name') : ''?>" placeholder="Name *">
									<label id="st_name-error" class="error" for="st_name" style="display:<?php echo(form_error('st_name')) ? 'block' : 'none';?>"></label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="st_email" id="st_email" value="<?php echo($this->input->post('st_email')!=NULL) ? $this->input->post('st_email') : ''?>" placeholder="Email">
									<label id="st_email-error" class="error" for="st_email" style="display:<?php echo(form_error('st_email')) ? 'block' : 'none';?>"></label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="st_mobile_number" id="st_mobile_number" value="<?php echo($this->input->post('st_mobile_number')!=NULL) ? $this->input->post('st_mobile_number') : ''?>" placeholder="Contact Number *">
									<label id="st_mobile_number-error" class="error" for="st_mobile_number" style="display:<?php echo(form_error('st_mobile_number')) ? 'block' : 'none';?>"></label>
								</div>
								<div class="form-group">
									<textarea class="form-control" placeholder="Message *" rows="5" name="st_message" id="st_message"><?php echo($this->input->post('st_message')!=NULL) ? $this->input->post('st_message') : ''?></textarea>
									<label id="st_message-error" class="error" for="st_message" style="display:<?php echo(form_error('st_message')) ? 'block' : 'none';?>"></label>
								</div>
							</div>
							<div class="col-sm-12 text-left">
								<button type="submit" class="btn btn-blue btn-lg">Submit</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col-sm-6">
				 
					<div class="row">
						<div class="col-sm-8">
							<h5>Contact us</h5>
							<p><i class="fa fa-map-marker"></i> Nashik City Police</p>
							<p><i class="fa fa-envelope"></i> Email: raikar.rahul@gmail.com</p>
							<h3>WhatsApp Helpline Numbers</h3>
							<ul class="list-unstyled">
								<li><a href="tel:9403165140">+91 9403165140</a></li>
								<li><a href="tel:9373800019">+91 9373800019</a></li>
								<li><a href="tel:7350166999">+91 7350166999</a></li>
								<li><a href="tel:7248922877">+91 7248922877</a></li>
								<li><a href="tel:9403165132">+91 9403165132</a></li>
								<li><a href="tel:8485810477">+91 8485810477</a></li>
								<li><a href="tel:7709295534">+91 7709295534</a></li>
								<li><a href="tel:7248906877">+91 7248906877</a></li>
								<li><a href="tel:7058946877">+91 7058946877</a></li>
								<li><a href="tel:7248903877">+91 7248903877</a></li>
								<li><a href="tel:7020583176">+91 7020583176</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
<?php $this->load->view('includes/footer');?>	


<script>
$(function () {
	
	$("#registerFrm").validate({
		rules: {
			st_name: {
				required: true,
			},
			st_mobile_number: {
				required: true
			},					
			st_message: {
				required: true
			},
			st_email: {
				email: true
			},
		},
	
		messages: {
			st_name: "Please enter name",
			st_mobile_number: "Enter your contact number",
			st_message: "Please enter your Message",
			st_email: "Please enter valid email address",
		},
		
		 submitHandler: function (form) {
			
			//var formdata = $("#frmSubmit").serialize();
			$('.loader').show();
			var site_url = '<?php echo base_url();?>contactus/action';
				
			var formData = new FormData($('#registerFrm')[0]);
				
			$.ajax({
				type:'POST',
				url : site_url,
				data: formData,
				processData: false,
				contentType: false,
				
				success: function(result_data){
					data_result_array = JSON.parse(result_data);
					if(data_result_array.result==0)
					{
						var data_result_array_message = data_result_array.message;
						for(var i in data_result_array_message)
						{
							$('#'+i+'-error').html(data_result_array_message[i]);
							$('#'+i+'-error').show();
						}
						$('.loader').hide();
					}
					else
					{
						$('.loader').hide();
						window.location.href="<?php echo base_url().'contactus'?>";
						
					}

				}
			});	
		},
	});	
		
});
</script>
</body>
</html>