<?php
$class_name = $this->router->fetch_class();
$function_name = $this->router->fetch_method();
?>

<nav class="navbar-aside navbar-static-side" role="navigation">
	<div class="sidebar-collapse nano">
		<div class="nano-content">
			<ul class="nav metismenu" id="side-menu">
				<li <?php if($class_name=="dashboard") {?> class="active" <?php }?>>
					<a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
				</li>
				
				<li <?php if($class_name=="system_users") {?> class="active" <?php }?>>
					<a href="<?php echo base_url();?>admin/system_users"><i class="fa fa-users"></i> <span class="nav-label">Administration</span></a>
				</li>
				
				<li <?php if($class_name=="items") {?> class="active" <?php }?>>
					<a href="<?php echo base_url();?>admin/items"><i class="fa fa-list"></i> <span class="nav-label">Items</span></a>
				</li>
				
				<?php /*<li <?php if($class_name=="food_request") {?> class="active" <?php }?>>
					<a href="<?php echo base_url();?>admin/food_request"><i class="fa fa-cutlery"></i> <span class="nav-label">Food Requests</span></a>
				</li>*/?>
				
				<li <?php if($class_name=="supplier_request") {?> class="active" <?php }?>>
					<a href="<?php echo base_url();?>admin/supplier_request"><i class="fa fa-money"></i> <span class="nav-label">Provider Requests</span></a>
				</li>
				
				<li <?php if($class_name=="completed_request") {?> class="active" <?php }?>>
					<a href="<?php echo base_url();?>admin/completed_request"><i class="fa fa-money"></i> <span class="nav-label">Completed Requests</span></a>
				</li>
				

				<?php /*<li <?php if($class_name=="delivery_request") {?> class="active" <?php }?>>
					<a href="<?php echo base_url();?>admin/delivery_request"><i class="fa fa-user"></i> <span class="nav-label">Delivery Persons</span></a>
				</li>
				
				<li <?php if($class_name=="feedback") {?> class="active" <?php }?>>
					<a href="<?php echo base_url();?>admin/feedback"><i class="fa fa-quote-right"></i> <span class="nav-label">Feedback</span></a>
				</li>*/?>
				
				<li <?php if($class_name=="settings") {?> class="active" <?php }?> >
					<a href="<?php echo base_url();?>admin/settings"><i class="fa fa-gear"></i> <span class="nav-label">Settings</span></a>
				</li>
				
					
				
			
			</ul>

		</div>
	</div>
</nav>