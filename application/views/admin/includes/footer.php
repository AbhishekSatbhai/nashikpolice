<div class="modal fade" id="authenticateModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Authentication</h4>
        </div>
        <form method="post" role="form" name="frmAuthenticate" id="frmAuthenticate" action="" enctype="multipart/form-data" >
        <div class="modal-body">
          <!-- form group-Admin-->
           <section class="panel panel-default">
                    <header class="panel-heading font-bold">
                    VOGCE
					<span id="modal_title"> Authentication</span>	  
                    </header>
                    <div class="panel-body">
                      	<div class="form-group">
                          <!-- first name-->
                          <input type="text" class="form-control" placeholder="Username" name="username" id="username">
						  <label id="username-error" class="error" for="username" style="display:<?php echo(form_error('username')) ? 'block' : 'none';?>"></label>
                        </div>
                        <div class="form-group">
                          <!-- Last name-->
                          <input type="password" class="form-control" placeholder="Passowrd" name="password" id="password">
						  <label id="password-error" class="error" for="password" style="display:<?php echo(form_error('password')) ? 'block' : 'none';?>"></label>
                        </div>
						<input type="hidden" id="redirect" value="">
						<div class="resultlogin"></div>
						<label id="valid_user-error" class="error" for="valid_user" style="display:<?php echo(form_error('username')) ? 'block' : 'none';?>"></label>
                    </div>
                  </section>
          <!-- end form group-Admin-->

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><!-- close-->
          <button type="button" class="btn btn-info" onclick="$('#frmAuthenticate').submit();">Authenticate</button><!-- save-->
        </div>
        </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>

		<script type="text/javascript" src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
		<script type="text/javascript" src="<?php echo ADMIN_ASSETS;?>js/jquery.min.js?ver=<?php echo VERSION;?>"></script>
        <script type="text/javascript" src="<?php echo ADMIN_ASSETS;?>bootstrap/js/bootstrap.min.js?ver=<?php echo VERSION;?>"></script>
        <script src="<?php echo ADMIN_ASSETS;?>js/metisMenu.min.js?ver=<?php echo VERSION;?>"></script>
        <script src="<?php echo ADMIN_ASSETS;?>js/jquery.nanoscroller.min.js?ver=<?php echo VERSION;?>"></script>
        <script src="<?php echo ADMIN_ASSETS;?>js/jquery-jvectormap-1.2.2.min.js?ver=<?php echo VERSION;?>"></script>
		<script src="<?php echo ADMIN_ASSETS;?>js/pace.min.js?ver=<?php echo VERSION;?>"></script>
        <script src="<?php echo ADMIN_ASSETS;?>js/waves.min.js?ver=<?php echo VERSION;?>"></script>
		<script src="<?php echo ADMIN_ASSETS;?>js/jquery-jvectormap-world-mill-en.js?ver=<?php echo VERSION;?>"></script>
		<script type="text/javascript" src="<?php echo ADMIN_ASSETS;?>js/custom.js?ver=<?php echo VERSION;?>"></script>
		
		<script type="text/javascript" src="<?php echo base_url()?>plugins/JavaScript-autoComplete-master/auto-complete.js"></script>
		
		<script src="<?php echo ADMIN_ASSETS;?>jquery_validation/js/jquery.validate.js?ver=<?php echo VERSION;?>"></script>
		<link href="<?php echo ADMIN_ASSETS;?>jquery_validation/css/cmxform.css?ver=<?php echo VERSION;?>" rel="stylesheet">
		
		<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>plugins/JavaScript-autoComplete-master/auto-complete.css">
		
		<script src="<?php echo ADMIN_ASSETS;?>js/bootbox.min.js"></script>
		
		<script type="text/javascript" src="<?php echo ASSETS;?>fancybox/jquery.fancybox.min.js"></script>
		<link href="<?php echo ASSETS;?>fancybox/jquery.fancybox.min.css" rel="stylesheet">
		
		<script>
		
		$(document).ready(function () { // Will only run once the page Document Object Model (DOM) is ready for JavaScript code 
			// Create a jQuery object containing the html element 'input'
			// Create a .not() method to exclude buttons for keypress event
			//$(":input:not(:disabled)").not($(":button")).keypress(function(evt) {
			
			$('input[tabindex="1"]').focus();
			$('textarea[tabindex="1"]').focus();
			$('select[tabindex="1"]').focus();
			
			$(":input:not(:disabled)").keypress(function(evt) { 
				// If the keypress event code is 13 (Enter)
				if (evt.keyCode == 13) {
					// get the attribute type and if the type is not submit
					itype = $(this).prop('type');
					if (itype !== 'submit') {
						currentTabindex = $(this).attr('tabindex');
						if (currentTabindex) {
							nextInput = $('input[tabindex^="'+ (parseInt(currentTabindex)+1) +'"]');
							nextTextarea = $('textarea[tabindex^="'+ (parseInt(currentTabindex)+1) +'"]');
							nextSelect = $('select[tabindex^="'+ (parseInt(currentTabindex)+1) +'"]');
							nextButtton = $('button[tabindex^="'+ (parseInt(currentTabindex)+1) +'"]');
							if (nextInput.length) {
								nextInput.focus();
								return false;
							}
							else if (nextTextarea.length) {
								nextTextarea.focus();
								return false;
							}
							else if(nextSelect.length) {
								nextSelect.focus();
								return false;
							}
							else if(nextButtton.length) { 
								$('#frmSubmit').submit();
							}
							
						}
					}
				}
			});
		});
		
		function authenticate_modal(redirect_url)
		{
			$('#redirect').val(redirect_url);
			$('#authenticateModal').modal('show');
		}
		
		$("#frmAuthenticate").validate({
			rules: {
				username: {
					required: true
				},
				password: {
					required: true
				},
			},
		
			messages: {
				username: "Please enter username",
				password: "Please enter password",
			},
			
			 submitHandler: function (form) {
				//var formdata = $("#frmSubmit").serialize();
				$('.loader').show();
				
				var site_url = '<?php echo base_url();?>dashboard/authenticate';
				
					
				$.ajax({
				  type:'POST',
				  url : site_url,
				  data: new FormData($('#frmAuthenticate')[0]),
				  processData: false,
				  contentType: false,
				  success: function(result_data){
					data_result_array = JSON.parse(result_data);
					if(data_result_array.result==0)
					{
						var data_result_array_message = data_result_array.message;
						for(var i in data_result_array_message)
						{
							$('#'+i+'-error').html(data_result_array_message[i]);
							$('#'+i+'-error').show();
						}
						$('.loader').hide();
					}
					else
					{
						$('.loader').hide();
						
						$(".resultlogin").html("<div class='alert alert-success login wow fadeIn animated'>"+data_result_array.message+"</div>");
						window.location.href=$('#redirect').val();
						
					}
					
				  }
				});
			},
		});
		
		/*window.setInterval(function(){
			load_db_backup();
		}, 50000);
		
		function load_db_backup()
		{
			var site_url = '<?php echo base_url();?>db_backup/ajax_backup';
			$.ajax({
			  url : site_url,
			  success: function(result_data){
			  }
			});
		}*/
		
		</script>		