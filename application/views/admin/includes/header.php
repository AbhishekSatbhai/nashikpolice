<?php
$company_details = get_settings();

$class_name = $this->router->fetch_class();
$function_name = $this->router->fetch_method();

$page_title = $company_details->st_organisation_name;

if($class_name == 'index')
	$page_title = "Welcome to ".$company_details->st_organisation_name;
else if($class_name != 'index' && $function_name == 'index')
	$page_title = $company_details->st_organisation_name." : ".ucfirst($class_name);
else if($class_name != 'index' && $function_name != 'index')
	$page_title = $company_details->st_organisation_name." : ".ucfirst($class_name)." - ".ucfirst($function_name);

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo $page_title;?></title>

        <!-- Bootstrap -->
        <link href="<?php echo ADMIN_ASSETS;?>bootstrap/css/bootstrap.min.css?ver=<?php echo VERSION;?>" rel="stylesheet">
        <link href="<?php echo ADMIN_ASSETS;?>css/waves.min.css?ver=<?php echo VERSION;?>" type="text/css" rel="stylesheet">
  		<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/nanoscroller.css?ver=<?php echo VERSION;?>">
        <link href="<?php echo ADMIN_ASSETS;?>css/menu-light.css?ver=<?php echo VERSION;?>" type="text/css" rel="stylesheet">
        <link href="<?php echo ADMIN_ASSETS;?>css/style.css?ver=<?php echo VERSION;?>" type="text/css" rel="stylesheet">
        <link href="<?php echo ADMIN_ASSETS;?>font-awesome/css/font-awesome.min.css?ver=<?php echo VERSION;?>" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        
		<style>
		.error {color:#FF3333;}
		.modal-dialog {top: 10% !important;}
		</style>
		<style>
		  .loader {
				position: fixed;
				left: 0px;
				top: 0px;
				width: 100%;
				height: 100%;
				z-index: 9999;
				opacity: 0.61;
				background: url('<?php echo base_url();?>assets/admin/images/loading.gif') 50% 50% no-repeat rgb(249,249,249);
			}
		  </style>
    </head>
    <body>
        <!-- Static navbar -->
		<div class="loader" style="display:none;"></div>	

        <nav class="navbar navbar-default yamm navbar-fixed-top">
            <div class="container-fluid">
			
				
				<?php if($this->session->userdata('admin_logged')){?>
                <button type="button" class="navbar-minimalize minimalize-styl-2  pull-left "><i class="fa fa-bars"></i></button>                        
                <div class="navbar-header">
					
					<ul class="nav navbar-nav">
					
						<li class="dropdown">
							<a class="dropdown-toggle button-wave" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="#">
								<?php echo $this->session->userdata('admin_name');?>  <span class="caret"></span></a>
							<ul class="dropdown-menu animated fadeInRight m-t-xs">
								<li><a href="<?php echo base_url()?>admin/profile"><i class="fa fa-user"></i> My Profile</a></li>
								<li class="divider"></li>
								<li><a href="<?php echo base_url()?>admin/logout"><i class="fa fa-key"></i> Logout</a></li>
							</ul>
						</li>
					</ul>
					
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
					
                </div>
				
				<div id="navbar" class="navbar-collapse collapse">
					  <ul class="nav navbar-nav">
					  	
						 <li>
                            <a href="<?php echo base_url()?>admin/dashboard" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-home"></i></a>
                         </li>
						
						<li>
                            <a href="<?php echo base_url()?>admin/logout" role="button" aria-haspopup="true" aria-expanded="false" class="btn btn-danger"><i class="fa fa-key"></i> Logout</a>
                        </li>
						
                    </ul>
				
                
            </div><!--/.container-fluid -->
				<?php }else{?>
				
				<div id="navbar" class="navbar-collapse collapse">
					  <ul class="nav navbar-nav">
					  	
						 <li>
                            <a href="<?php echo base_url()?>admin" role="button" aria-haspopup="true" aria-expanded="false" class="btn btn-danger"><i class="fa fa-key"></i> Login</a>
                         </li>
					</ul>
				</div>
				
				<?php }?>
        </nav>