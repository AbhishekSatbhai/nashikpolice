<?php $this->load->view('admin/includes/header');?> 

<section class="page">

	<?php $this->load->view('admin/includes/sidebar');?> 

	<div id="wrapper">
		<div class="content-wrapper container">
			<div class="row">
				<div class="col-sm-6">
					<div class="page-title">
						<h1>Users  <small></small></h1>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url()?>admin/dashboard"><i class="fa fa-home"></i></a></li>
							<li class="active">User list</li>
						</ol>
					</div>
				</div>
				<div class="col-sm-6 text-right" <?php echo privilege('flg_new_employee');?>>
					<a href="<?php echo base_url()?>admin/system_users/add" class="btn btn-success">New User</a>
				</div>
			</div><!-- end .page title-->
			
			<?php if($this->session->flashdata('success_message')!=null){?>
			<div class="row">
				<div class="col-sm-12 margin-b-30">
					<div class="alert alert-success" style="margin-bottom:0px;">
						<?php echo $this->session->flashdata('success_message');?>
					</div>
				</div>
			</div>
			<?php }?>
			<div class="row users-row">
			<?php if(!empty($result)) {
			foreach($result as $row) {
			?>
			
			<div class="col-md-6">
				<div class="user-col">
					<div class="media">
						<a class="pull-left" href="#">
							<?php $image = (!empty($row->st_image)) ? $row->st_image : 'profile_pic.png'?>
							<img class="media-object img-circle" src="<?php echo MEDIA.'employee_images/'.$image;?>" alt="100x100" data-src="holder.js/100x100" style="width: 100px; height: 100px;">
						</a>
						<div class="media-body">
							<h3 class="follower-name"><?php echo $row->st_name;?></h3>
	
							<div style="height: 20px;"></div>
	
							<div class="btn-toolbar">
								<div class="btn-group" <?php echo privilege('flg_edit_employee');?>>
									<a href="<?php echo base_url()?>admin/system_users/edit/<?php echo $row->in_user_id;?>" class="btn btn-info btn-3d btn-sm"><i class="fa fa-edit"></i> Edit Profile</a>
								</div><!-- btn-group -->
								<div class="btn-group" <?php echo privilege('flg_delete_employee');?>>
									<a href="<?php echo base_url()?>admin/system_users/delete/<?php echo $row->in_user_id;?>" class="btn btn-danger btn-3d btn-sm" onclick="return confirm('Are you sure you want to delete this?');"><i class="fa fa-trash"></i> Delete</a>
								</div><!-- btn-group -->
									
								<div class="btn-group">
									<a href="<?php echo base_url()?>admin/system_users/status/<?php echo $row->in_user_id;?>" class="btn btn-primary btn-3d btn-sm" ><?php echo($row->flg_is_active==1) ? 'Active' : 'Inactive' ;?></a>
								</div>
	
							</div><!-- btn-toolbar -->
						</div><!-- media-body -->
					</div>
				</div>
			</div><!--.col-6-->
			
			<?php } }?>
			</div>
		</div> 
	</div>
</section>

<?php $this->load->view('admin/includes/footer');?> 

</body>
</html>
