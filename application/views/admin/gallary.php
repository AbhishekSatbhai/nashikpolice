<header class="masthead">
  <aside class="right_bar">
    <a class="right_bar_toggler" href="javascript:void(0);">
      <i class="fa fa-bars"></i>
    </a>
    <a class="right_bar_admission" href="javascript:void(0);">
      Admissions
      <i class="fa fa-long-arrow-up"></i>
    </a>
    <a class="right_bar_search" href="javascript:void(0);">
       <i class="fa fa-search"></i>
    </a>
    <div class="container menu_items">
      <a class="close_btn" href="javascript:void(0);"></a>
      <div class="row">
        <div class="mega_menu_left col-lg-3">
          <span class="mega_menu_brand">
            <a href="<?php echo base_url();?>" class="main-site-logo"><img src="images/logo.png" alt="VOGCE University" /></a>
          </span>
        </div>
      <div class="mega_menu col-lg-9">
    </div>
  </aside>
</header>
    <!-- #EndLibraryItem -->
    <!-- About Section -->
<section class="inner-page">
  <div class="inner-hadding">
    
    <div class="container">
      <div class="top-menu-sec">
        <h4>Gallery</h4>
          <ul>
            <li class="active"><a href="<?php echo base_url();?>gallary">Gallery</a></li>
            <li><a href="<?php echo base_url();?>gallary/media">Media Coverage</a></li>
          </ul>
      </div>
    </div>                     
                
      <section class="inner_section pt-3 infra-study-sec1 infra_study">
        <div class="container">
          <h1>Gallery</h1>
            <div class="row no-gutters pt-5 about_campus">


<?php foreach ($result as $row) 
                          {?>
              <div class="col-md-4 col-md-4 col-lg-2 col-xl-4 pr-4 colfull">
                <section class="card-columns1 waterfall" style="height: 467.63px;">
                  <div class="cardgroup r1 c0" style="width: 361.333px; left: 0px; top: 0px;">
                    <a  href="<?php echo base_url();?>gallary/view/<?php echo $row->in_id;?>">
                      <div class="card-head">
                        <?php $image = $row->st_image;?>
                        <img class="card-img-top img-fluid" src="<?php echo MEDIA.'events_images/'.$image;?>" alt="Card image cap">

                      </div>
                      <div class="card-block">
                        <h5 class="mt-4 mb-4"> <?php echo $row->st_name;?></h5>
                     </div>
                    </a>
                  </div> 
                </section>
              </div>
              <?php }?>

            </div>
        </div>
      </section> 
  </div>        
</section>


                           
                    
          

  