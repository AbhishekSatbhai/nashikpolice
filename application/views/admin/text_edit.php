<?php $this->load->view('admin/includes/header');?>
<section class="page">
	<?php $this->load->view('admin/includes/sidebar');?>
	<div id="wrapper">
		<div class="content-wrapper container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title">
						<h1>Edit Text <small></small></h1>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url()?>admin/dashboard"><i class="fa fa-home"></i></a></li>
							<li><a href="<?php echo base_url()?>admin/events"><i class="fa fa-users"></i></a></li>
							<li class="active">Edit Text</li>
						</ol>
					</div>
				</div>
				</div><!-- end .page title-->
				
				<?php if($this->session->flashdata('success_message')!=null){?>
				<div class="col-sm-12 margin-b-30">
					<div class="alert alert-success" style="margin-bottom:0px;">
						<?php echo $this->session->flashdata('success_message');?>
					</div>
				</div>
				<?php }?>
				
				
				
				<div class="col-md-12 margin-b-30">
					<div class="profile-edit">
						<form class="form-horizontal" method="post" name="frmSubmit" id="frmSubmit" enctype="multipart/form-data">
							<h3 class="mb-xlg">Information</h3> <h5 class="mb-xlg text-right"><span style="color:#FF0000;">*</span> fields should be mandatory</h5>
							<fieldset>
								<div class="form-group">
									<label class="col-md-3 control-label" for="st_title">Title:<span style="color:#FF0000;">*</span></label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="st_title" name="st_title" value="<?php echo($this->input->post('st_title')!=NULL) ? $this->input->post('st_title') : $result->st_title;?>" tabindex="1">
										<label id="st_name-error" class="error" for="st_title" style="display:<?php echo(form_error('st_title')) ? 'block' : 'none';?>"><?php echo form_error('st_title');?></label>
									</div>
								</div>
								
								
								<div class="form-group">
									<label class="col-md-3 control-label" for="st_description">Description:<span style="color:#FF0000;">*</span></label>
									<div class="col-md-8">
										<textarea rows="5" class="form-control" id="st_description" name="st_description" value="<?php echo($this->input->post('st_description')!=NULL) ? $this->input->post('st_description') : $result->st_description;?>"tabindex="2"><?php echo($this->input->post('st_description')!=NULL) ? $this->input->post('st_description') : $result->st_description;?></textarea>
									</div>
								</div>
								
								
								
							</fieldset>
							
							
							
							<div class="panel-footer">
								<div class="row">
									<div class="col-md-9 col-md-offset-3">
										<button type="submit" class="btn btn-primary" tabindex="13">Submit</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<?php $this->load->view('admin/includes/footer');?>
	<script src="<?php echo ADMIN_ASSETS;?>jquery_validation/js/jquery.validate.js?ver=<?php echo VERSION;?>"></script>
	<link href="<?php echo ADMIN_ASSETS;?>jquery_validation/css/cmxform.css?ver=<?php echo VERSION;?>" rel="stylesheet">
	<script src="<?php echo base_url();?>plugins/ckeditor/ckeditor.js?ver=<?php echo VERSION;?>"></script>
	<script>
	
	CKEDITOR.replace( 'st_description',{extraAllowedContent: 'section article header nav aside'});
	
	$("#frmSubmit").validate({
		rules: {
		   st_description: {
		   required: true
			},
		   st_title: {
		   required: true
		   }  
		},
		messages: {
		   st_title: "Please enter Title.",
		   st_description: "Please enter Description."  
		},
	});
	</script>
	</body>
</html>