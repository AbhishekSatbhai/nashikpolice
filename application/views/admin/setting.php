<?php $this->load->view('admin/includes/header');?> 

<section class="page">

	<?php $this->load->view('admin/includes/sidebar');?> 

	<div id="wrapper">
		<div class="content-wrapper container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title">
						<h1>Organisation Details<small></small></h1>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url()?>admin/dashboard"><i class="fa fa-home"></i></a></li>
							<li class="active">Organisation Details</li>
						</ol>
					</div>
				</div>
			</div><!-- end .page title-->
			
			<?php if($this->session->flashdata('success_message')!=null){?>
				<div class="col-sm-12 margin-b-30">
					<div class="alert alert-success" style="margin-bottom:0px;">
						<?php echo $this->session->flashdata('success_message');?>
					</div>
				</div>
			<?php }?>
			
			
			<div class="col-md-10 margin-b-30">
				<div class="profile-edit">
					<form class="form-horizontal" method="post" name="frmSubmit" id="frmSubmit" enctype="multipart/form-data">
						<fieldset>
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_organisation_name">Organisation Name:</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="st_organisation_name" name="st_organisation_name" value="<?php echo($this->input->post('st_organisation_name')!=NULL) ? $this->input->post('st_organisation_name') : $result->st_organisation_name;?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_address">Organisation Address:</label>
								<div class="col-md-8">
									<textarea class="form-control" id="st_address" name="st_address"><?php echo($this->input->post('st_address')!=NULL) ? $this->input->post('st_address') : $result->st_address;?></textarea>
								</div>
							</div>
							
							
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_contact_numbers">Contact Numbers:</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="st_contact_numbers" name="st_contact_numbers" value="<?php echo($this->input->post('	st_contact_numbers')!=NULL) ? $this->input->post('st_contact_numbers') : $result->st_contact_numbers;?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_slogan">Slogan</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="st_slogan" name="st_slogan" value="<?php echo($this->input->post('st_slogan')!=NULL) ? $this->input->post('st_slogan') : $result->st_slogan;?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_title">Title</label>
								<div class="col-md-8">
									<textarea rows="5" class="form-control" id="st_title" name="st_title"><?php echo($this->input->post('st_title')!=NULL) ? $this->input->post('st_title') : $result->st_title;?></textarea>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_logo">Logo</label>
								<div class="col-md-8">
									<input type="file" class="form-control" id="st_logo" name="st_logo" tabindex="9">
									</br>
									<div class="avtar text-center">
									<?php $image = (!empty($result->st_logo)) ? $result->st_logo : 'logo.png'?>
									<img src="<?php echo MEDIA.'logo/'.$image;?>" alt="Select L" class="img-thumbnail">
									<h3><?php echo $result->st_logo;?></h3>
									                               
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_email">Email:</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="st_email" name="st_email" value="<?php echo($this->input->post('st_email')!=NULL) ? $this->input->post('st_email') : $result->st_email;?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_twitter_link">Twitter Link	:</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="st_twitter_link" name="st_twitter_link" value="<?php echo($this->input->post('st_twitter_link	')!=NULL) ? $this->input->post('st_twitter_link') : $result->st_twitter_link;?>">
								</div>
							</div>
							
								
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_facebook_link">Facebook Link:</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="st_facebook_link" name="st_facebook_link" value="<?php echo($this->input->post('st_facebook_link	')!=NULL) ? $this->input->post('st_facebook_link	') : $result->st_facebook_link	;?>">
								</div>
							</div>
								
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_instagram_link">Instagram Link:</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="st_instagram_link" name="st_instagram_link" value="<?php echo($this->input->post('st_instagram_link')!=NULL) ? $this->input->post('st_instagram_link') : $result->st_instagram_link;?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_linkedin_link">Linkedin Link:</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="st_linkedin_link" name="st_linkedin_link" value="<?php echo($this->input->post('st_instagram_link')!=NULL) ? $this->input->post('st_linkedin_link	') : $result->st_linkedin_link	;?>">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_youtube_link">Youtube Link:</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="st_youtube_link" name="st_youtube_link" value="<?php echo($this->input->post('st_youtube_link')!=NULL) ? $this->input->post('st_youtube_link	') : $result->st_youtube_link	;?>">
								</div>
							</div>
							
													
						</fieldset>
						
						<div class="panel-footer">
							<div class="row">
								<div class="col-md-9 col-md-offset-3">
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div> 
	</div>
</section>

<?php $this->load->view('admin/includes/footer');?> 
</body>
</html>

