
    <!-- #EndLibraryItem -->
    <!-- Intro Header -->
    <!-- #BeginLibraryItem "/Library/Inner_right_menu.lbi" -->
    <header class="masthead">
    
    <aside class="right_bar">
        <a class="right_bar_toggler" href="javascript:void(0);">
          <i class="fa fa-bars"></i>
        </a>
    <a class="right_bar_admission" href="javascript:void(0);">
      Admissions
          <i class="fa fa-long-arrow-up"></i>
        </a>
    <a class="right_bar_search" href="javascript:void(0);">
          <i class="fa fa-search"></i>
        </a>
        <div class="container menu_items">
    <a class="close_btn" href="javascript:void(0);"></a>
    <div class="row">
        <div class="mega_menu_left col-lg-3">
          <span class="mega_menu_brand">
            <a href="<?php echo base_url();?>" class="main-site-logo"><img src="images/logo.png" alt="VOGCE University" /></a>
          </span>
        </div>
        <div class="mega_menu col-lg-9">
        </div>
      </div>

    </div>
    <div class="container admission_menu">
      
    </div>
    </aside>
  </header>
    <!-- #EndLibraryItem -->
    <!-- About Section -->
   
     
<section class="inner-page">
                    <div class="inner-hadding">
                          <div class="container">
                              <div class="top-menu-sec">
                                    <h4>Gallery</h4>
                                    
                              </div>  
                
              </div>
                    </div>
                    <section class="inner_section pt-3 infra-study-sec1 infra_study">
                      <div class="container">
                       <?php
                          if(!empty($events_data)) 
                         {?>
                        <h3><?php echo $events_data[0]->st_name;?></h3>
                        <?php }?>

                          <div class="row no-gutters pt-5 about_campus">
                        <?php foreach ($getgallery as $row) 
                         {?>
                          <div class="col-md-2 col-md-2 col-lg-2 col-xl-2 pr-2 colfull">
                            <?php $image = $row->st_image;?>
                            <a class="group1" href="<?php echo MEDIA.'gallary/'.$image;?>">
                            <img src="<?php echo MEDIA.'gallary/'.$image;?>" class="img-fluid">
                             </a>
                          </div>
                        <?php }?>
                        </div>
                      </div>

                    </section>
                            
</section>

  