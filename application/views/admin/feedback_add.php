<?php $this->load->view('admin/includes/header');?> 

<section class="page">

	<?php $this->load->view('admin/includes/sidebar');?> 

	<div id="wrapper">
		<div class="content-wrapper container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title">
						<h1>New feedback <small></small></h1>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url()?>admin/dashboard"><i class="fa fa-home"></i></a></li>
							<li><a href="<?php echo base_url()?>admin/feedback"><i class="fa fa-users"></i></a></li>
							<li class="active">Add New feedback</li>
						</ol>
					</div>
				</div>
			</div><!-- end .page title-->
			
			<?php if($this->session->flashdata('success_message')!=null){?>
				<div class="col-sm-12 margin-b-30">
					<div class="alert alert-success" style="margin-bottom:0px;">
						<?php echo $this->session->flashdata('success_message');?>
					</div>
				</div>
			<?php }?>
			
			
			<div class="col-md-8 margin-b-30">
				<div class="profile-edit">
					<form class="form-horizontal" method="post" name="frmSubmit" id="frmSubmit" enctype="multipart/form-data">
						<fieldset>
							<div class="col-lg-12"><h5 class="mb-xlg text-right"><span style="color:#FF0000;">*</span> fields should be mandatory</h5></div>
						<!-- <h3 class="mb-xlg">Personal Information</h3> -->
													
						<div class="form-group">
								<label class="col-md-3 control-label" for="st_name">Full Name: <span style="color:#FF0000;">*</span></label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="st_name" name="st_name" value="<?php echo($this->input->post('st_name')!=NULL) ? $this->input->post('st_name') : '';?>" tabindex="1">
									<label id="st_name-error" class="error" for="st_name" style="display:<?php echo(form_error('st_name')) ? 'block' : 'none';?>"><?php echo form_error('st_name')?></label>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-3 control-label" for="st_address">Address: <span style="color:#FF0000;">*</span></label>
								<div class="col-md-8">
									<textarea rows="3" class="form-control" id="st_address" name="st_address" value="<?php echo($this->input->post('st_address')!=NULL) ? $this->input->post('st_address') : '';?>" tabindex="2"></textarea>
								</div>
							</div>
							
						
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_image">Photo:</label>
								<div class="col-md-8">
									<input type="file" class="form-control" id="st_image" name="st_image" tabindex="4">
									<label id="st_image-error" class="error" for="st_image" style="display:<?php echo(form_error('st_image')) ? 'block' : 'none';?>"><?php echo form_error('st_image');?></label>
								</div>
							</div>
							
								<div class="form-group">
								<label class="col-md-3 control-label" for="st_message">Message: <span style="color:#FF0000;">*</span></label>
								<div class="col-md-8">
									<textarea rows="3" class="form-control" id="st_message" name="st_message" value="<?php echo($this->input->post('st_message')!=NULL) ? $this->input->post('st_message') : '';?>" tabindex="2"></textarea>
									
								</div>
							</div>
							
							
						
								
						</fieldset>
						
						
								
					

							

					
						<div class="panel-footer">
							<div class="row">
								<div class="col-md-8 col-md-offset-5">
									<button type="submit" class="btn btn-primary" tabindex="12">Submit</button>
									<button type="reset" class="btn btn-default">Reset</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div> 
	</div>
</section>

<?php $this->load->view('admin/includes/footer');?> 

<link href="<?php echo ADMIN_ASSETS;?>bootstrap-datepicker/css/bootstrap-datepicker.min.css?ver=<?php echo VERSION;?>" rel="stylesheet">
<script type="text/javascript" src="<?php echo ADMIN_ASSETS;?>bootstrap-datepicker/js/bootstrap-datepicker.min.js?ver=<?php echo VERSION;?>"></script>

<script src="<?php echo ADMIN_ASSETS;?>jquery_validation/js/jquery.validate.js?ver=<?php echo VERSION;?>"></script>
<link href="<?php echo ADMIN_ASSETS;?>jquery_validation/css/cmxform.css?ver=<?php echo VERSION;?>" rel="stylesheet">

<script>
$('#dt_birth_date').datepicker({orientation: 'bottom auto', autoclose: true});

$("#frmSubmit").validate({
	rules: {
		st_name: {
			required: true
		},
		st_address: {
			required: true
		},
		st_message: {
			required: true
		}

		
	},

	messages: {
		st_name: "Please enter name.",
		st_address	: "Please enter Address.",
		st_message	: "Please enter Message."
		
			},
});
</script>

</body>
</html>

