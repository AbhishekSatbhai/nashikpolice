<?php $company_details = get_settings();?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title><?php echo $company_details->st_organisation_name?></title>

        <!-- Bootstrap -->
        <link href="<?php echo ADMIN_ASSETS;?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo ADMIN_ASSETS;?>css/waves.min.css" type="text/css" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo ADMIN_ASSETS;?>css/nanoscroller.css">
        <!--        <link rel="stylesheet" href="css/nanoscroller.css">-->
        <link href="<?php echo ADMIN_ASSETS;?>css/style.css" type="text/css" rel="stylesheet">
        <link href="<?php echo ADMIN_ASSETS;?>font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
		
		<style>
		body {
			background-image: url('<?php echo ADMIN_ASSETS;?>/images/img_bg.jpg') !important;
			background-size: cover !important;
			background-repeat:no-repeat !important;
		}
		
		.form-control{
			border:1.5px solid #204d74;
			color:#000000;
		}
		
		h3{ color:#000000;}
		
		</style>
		
    </head>
    <body class="account">
        <div class="container">
            <div class="row">
                <div class="account-col text-center">
                    <img src="<?php echo ADMIN_ASSETS; ?>images/logo1.jpg" style="width:200px; height:80px; margin-bottom:20px; border-radius: 20px;" />
                    <h3>Log into your account</h3>
					<form class="form-signin" role="form" method="post">
						<div class="form-group">
							<input type="text" class="form-control" style="border-radius: 15px;" placeholder="Username" name="username" id="username">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" style="border-radius: 15px;" placeholder="Passowrd" name="password" id="password">
						</div>
						<button type="submit" style="border-radius: 20px;" class="btn btn-primary btn-block ">Login</button>
						
						<div class="resultlogin"></div>
						
						<p>Powered by Nashik City Police</p>
					</form>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<?php echo ADMIN_ASSETS;?>js/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo ADMIN_ASSETS;?>bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo ADMIN_ASSETS;?>js/pace.min.js"></script>
		
		<script type="text/javascript">
		$(function () {
		   //login functionality
			$(".form-signin").on('submit',function(){
			
				$(".resultlogin").html("<div class='alert alert-info loading wow fadeOut animated'>Please wait ...</div>");
				
				$.post("<?php echo base_url();?>admin/index/login",$(".form-signin").serialize(), function(response){
						
					var resp = $.parseJSON(response);
					
					if(!resp.status){
						$(".resultlogin").html("<div class='alert alert-danger loading wow fadeIn animated'>"+resp.msg+"</div>");
					}else{
						$(".resultlogin").html("<div class='alert alert-success login wow fadeIn animated'>"+resp.msg+"</div>");
						window.location.href="<?php echo base_url()?>admin/dashboard";
					}
			
				}); 
				
				return false;
			});
			// end login functionality
		
		});
		</script>
		
    </body>
</html>