<?php $this->load->view('admin/includes/header');?> 

<section class="page">

	<?php $this->load->view('admin/includes/sidebar');?> 

	<div id="wrapper">
		<div class="content-wrapper container">
			<div class="row">
				<div class="col-sm-6">
					<div class="page-title">
						<h1>Text <small></small></h1>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url()?>admin/dashboard"><i class="fa fa-home"></i></a></li>
							<li class="active">Text List</li>
						</ol>
					</div>
				</div>
				<!-- 
				<div class="col-sm-6 text-right">
					<a href="<?php echo base_url()?>admin/text/add" class="btn btn-success" <?php echo privilege('flg_new_customer');?>>New Text</a>
				</div> -->
			</div><!-- end .page title-->
			
			<?php if($this->session->flashdata('success_message')!=null){?>
			<div class="row">
				<div class="col-sm-12 margin-b-30">
					<div class="alert alert-success" style="margin-bottom:0px;">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<?php echo $this->session->flashdata('success_message');?>
					</div>
				</div>
			</div>
			<?php }?>
			<div class="row users-row">
			
				<div class="col-md-12">
					<div class="panel panel-card ">
					<!-- Start .panel -->
					<div class="panel-heading">
						<h4 class="panel-title">Details</h4>
						<div class="panel-actions">
							<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
						</div>
					</div>
					<div class="panel-body">
						<form method="get" name="searchForm" id="searchForm" action="<?php echo base_url();?>admin/text">
						<table id="result-datatables" class="table table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>#</th>
									<th style="font-weight:700;">Title</th>
									<th style="font-weight:700;">Description</th>
									<th style="font-weight:700;">Status</th>
									<th style="font-weight:700;">Action</th>
					
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($result)) {
							$i=($page==0) ? 1 : $page + 1;	
							foreach($result as $row) {
							?>	
								<tr>
									<td><?php echo $i;?></td>
									<td><?php echo $row->st_title;?></td>
									<td><?php echo substr("$row->st_description",0,60);?>...</td>
									<td><?php 
									if($row->flg_is_active==1)
									{
										$btn_color = 'info';
										$status =  'Active';
									}
									else
									{
										$btn_color = 'danger';
										$status =  'Inactive';
									}
									?>
									<a href="<?php echo base_url();?>admin/text/status/<?php echo $row->in_id;?>" class="btn btn-<?php echo $btn_color;?> btn-xs"><?php echo $status;?></a>
									</td>
									<td><a href="<?php echo base_url();?>admin/text/edit/<?php echo $row->in_id;?>" class="btn btn-info btn-xs" <?php echo privilege('flg_edit_customer');?>><i class="fa fa-edit"></i></a>
									<a href="<?php echo base_url();?>admin/text/delete/<?php echo $row->in_id?>" class="warning-alert btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete this?');" <?php echo privilege('flg_delete_customer');?>><i class="fa fa-trash"></i></a>
												
									</td>
								</tr>
							<?php $i++; } } ?>
								
							</tbody>
						</table>
						</form>
						
						<?php if(!empty($result)) {?>  
						<div class="row">
							<?php echo $pagination;?>
						</div>
						<?php }?>
						
					</div>
				</div><!-- End .panel -->  
                </div>
			
			
			</div>
		</div> 
	</div>
</section>

<?php $this->load->view('admin/includes/footer');?> 

<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/jquery.dataTables.js?ver=<?php echo VERSION;?>"></script>
<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/dataTables.tableTools.js?ver=<?php echo VERSION;?>"></script>
<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/dataTables.bootstrap.js?ver=<?php echo VERSION;?>"></script>
<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/dataTables.responsive.js?ver=<?php echo VERSION;?>"></script>
<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/tables-data.js?ver=<?php echo VERSION;?>"></script>

<script>
$(document).ready(function() {
    $('#result-datatables').DataTable( {
		"sDom": '<"H"lfr>t<"F">',
		"columnDefs": [ {
			"targets": [4],
			"orderable": false
		} ],
		language: {
			emptyTable: 'Record Not Found',
		  	zeroRecords: 'Record Not Found'
		},
		"pageLength": 100,
    } );
	
	$('select[name="result-datatables_length"]').change(function(){
		$('#searchForm').submit();
	});
	
	$('input[type="search"]').attr('name','search_keyword');
	
	$('input[name="search_keyword"]').keyup(function(e){
		if(e.keyCode==13){
			$('#searchForm').submit();
		}
	});
	
	$('select[name="result-datatables_length"]').val('<?php echo $search_per_page?>');
	$('input[name="search_keyword"]').val('<?php echo $search_keyword?>');
	
} );
</script>

</body>
</html>
