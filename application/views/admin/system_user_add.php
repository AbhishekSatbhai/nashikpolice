<?php $this->load->view('admin/includes/header');?> 

<section class="page">

	<?php $this->load->view('admin/includes/sidebar');?> 

	<div id="wrapper">
		<div class="content-wrapper container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title">
						<h1>New User <small></small></h1>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url()?>dashboard"><i class="fa fa-home"></i></a></li>
							<li class="active">Add New User</li>
						</ol>
					</div>
				</div>
			</div><!-- end .page title-->
			
			<?php if($this->session->flashdata('success_message')!=null){?>
				<div class="col-sm-12 margin-b-30">
					<div class="alert alert-success" style="margin-bottom:0px;">
						<?php echo $this->session->flashdata('success_message');?>
					</div>
				</div>
			<?php }?>
			
			
			<div class="col-md-12 margin-b-30">
				<div class="profile-edit">
					<form class="form-horizontal" method="post" name="frmSubmit" id="frmSubmit" enctype="multipart/form-data">
						<div class="row">
						<div class="col-md-12"><h5 class="mb-xlg text-right"><span style="color:#FF0000;">*</span> fields should be mandatory</h5></div>
						<div class="col-md-6">
						<h4 class="mb-xlg">Personal Information</h4> 
						<fieldset>
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_name">Full Name <span style="color:#FF0000;">*</span></label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="st_name" name="st_name" value="<?php echo($this->input->post('st_name')!=NULL) ? $this->input->post('st_name') : '';?>" tabindex="2">
									<label id="st_name-error" class="error" for="st_name" style="display:<?php echo(form_error('st_name')) ? 'block' : 'none';?>"><?php echo form_error('st_name');?></label>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_email">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="st_email" name="st_email" value="<?php echo($this->input->post('st_email')!=NULL) ? $this->input->post('st_email') : '';?>" tabindex="4">
									<label id="st_email-error" class="error" for="st_email" style="display:<?php echo(form_error('st_email')) ? 'block' : 'none';?>">Please enter valid email.</label>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="avatar">Photo</label>
								<div class="col-md-8">
									<input type="file" class="form-control" id="avatar" name="avatar" tabindex="9">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_image">Role</label>
								<div class="col-md-8">
									<?php $in_role_id = ($this->input->post('in_role_id')!=NULL) ? $this->input->post('in_role_id') : '';?>
									<select class="form-control" id="in_role_id" name="in_role_id" tabindex="11">
										<option value="">Select Role</option>
										<?php foreach($roles as $row) {?>
										<option value="<?php echo $row->in_id?>" <?php echo($row->in_id == $in_role_id) ? 'selected="selected"' : ''?> ><?php echo $row->st_role_name?></option>
										<?php }?>
									</select>
									
								</div>
							</div>
							
						</fieldset>
						</div>
						
						<div class="clearfix"></div>
						<div class="col-md-12"><hr class="dotted tall"></div>
						<div class="col-md-6">
						<fieldset class="mb-xl">
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_username">New Username</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="st_username" name="st_username" value="<?php echo($this->input->post('st_username')!=NULL) ? $this->input->post('st_username') : '';?>" tabindex="12">
									<label id="st_username-error" class="error" for="st_username" style="display:<?php echo(form_error('st_username')) ? 'block' : 'none';?>"><?php echo form_error('st_username');?></label>
								</div>
							</div>
						</fieldset>
						</div>
						<div class="col-md-6">
						<fieldset class="mb-xl">
							<div class="form-group">
								<label class="col-md-3 control-label" for="new_password">Password</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="new_password" name="new_password" value="<?php echo($this->input->post('new_password')!=NULL) ? $this->input->post('new_password') : '';?>" tabindex="13">
									<label id="new_password-error" class="error" for="new_password" style="display:<?php echo(form_error('new_password')) ? 'block' : 'none';?>"><?php echo form_error('new_password');?></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="confirm_password">New Password</label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="confirm_password" name="confirm_password" value="<?php echo($this->input->post('confirm_password')!=NULL) ? $this->input->post('confirm_password') : '';?>" tabindex="14">
									<label id="confirm_password-error" class="error" for="confirm_password" style="display:<?php echo(form_error('confirm_password')) ? 'block' : 'none';?>"><?php echo form_error('confirm_password');?></label>
								</div>
							</div>
						</fieldset>
						</div>
						</div>
						<div class="panel-footer">
							<div class="row">
								<div class="col-md-9 col-md-offset-3">
									<button type="submit" class="btn btn-primary" tabindex="15">Submit</button>
									<button type="reset" class="btn btn-default">Reset</button>
								</div>
							</div>
						</div>

					</form>
				</div>
			</div>
		</div> 
	</div>
</section>

<?php $this->load->view('admin/includes/footer');?> 

<script src="<?php echo ADMIN_ASSETS;?>jquery_validation/js/jquery.validate.js?ver=<?php echo VERSION;?>"></script>
<link href="<?php echo ADMIN_ASSETS;?>jquery_validation/css/cmxform.css?ver=<?php echo VERSION;?>" rel="stylesheet">

<script>
$("#frmSubmit").validate({
	rules: {
		st_name: {
			required: true
		},
		st_email: {
			email:	true
		},
		in_role_id: {
			required: true
		}
	},

	messages: {
		st_name: "Please enter name.",
		st_email: "Please enter valid email",
		in_role_id: "Please select role",
	},
});
</script>

</body>
</html>
