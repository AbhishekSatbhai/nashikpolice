<?php $this->load->view('admin/includes/header');?>
<section class="page">
	<?php $this->load->view('admin/includes/sidebar');?>
	<div id="wrapper">
		<div class="content-wrapper container">
			<div class="row">
				<div class="col-sm-6">
					<div class="page-title">
						<h1>Gallery <small></small></h1>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url()?>admin/dashboard"><i class="fa fa-home"></i></a></li>
							<li class="active">Gallery list</li>
						</ol>
					</div>
				</div>
				
				<div class="col-sm-6 text-right">
					<a href="<?php echo base_url()?>admin/gallery/add?pid=<?php echo $this->input->get('pid'); ?>&cid=<?php echo $this->input->get('cid'); ?>" class="btn btn-success" <?php echo privilege('flg_new_customer');?>>New Gallery</a>
				</div>
				</div><!-- end .page title-->
				
				<?php if($this->session->flashdata('success_message')!=null){?>
				<div class="row">
					<div class="col-sm-12 margin-b-30">
						<div class="alert alert-success" style="margin-bottom:0px;">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<?php echo $this->session->flashdata('success_message');?>
						</div>
					</div>
				</div>
				<?php }?>
				<div class="row users-row">
					
					<div class="col-md-12">
						<div class="panel panel-card ">
							<!-- Start .panel -->
							<div class="panel-heading">
								<h4 class="panel-title"> Gallery Details</h4>
								<div class="panel-actions">
									<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
								</div>
							</div>
							<div class="panel-body">
								<form method="get" name="searchForm" id="searchForm" action="<?php echo base_url();?>gallery">
									<table id="result-datatables" class="table table-bordered" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>#</th>
												<th style="font-weight:700;">Image</th>
												<th style="font-weight:700;">Action</th>
												
											</tr>
										</thead>
										<tbody>
											
											
											<?php if(!empty($result)) {
												$i=($page==0) ? 1 : $page + 1;
											foreach($result as $row) {
											?>
											<tr>
												<td><?php echo $i;?></td>
												<td><div class="avtar text-center">
													<?php $image = $row->st_image;?>
													<img src="<?php echo MEDIA.'gallary/'.$image;?>" height="160px" width="160px" alt="Select L" class="img-thumbnail">
												</div>
											</td>
											<td>
												<a href="<?php echo base_url();?>admin/gallery/delete/<?php echo $row->in_id;?>" class="warning-alert btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete this?');" <?php echo privilege('flg_delete_customer');?>><i class="fa fa-trash"></i></a>	

											</td>
											
										</tr>
										<?php $i++; } } ?>
										
									</tbody>
								</table>
							</form>
							
							<?php if(!empty($result)) {?>
							<div class="row">
								<?php echo $pagination;?>
							</div>
							<?php }?>
							
						</div>
						</div><!-- End .panel -->
					</div>
					
					
				</div>
			</div>
		</div>
	</section>
	<?php $this->load->view('admin/includes/footer');?>
	<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/jquery.dataTables.js?ver=<?php echo VERSION;?>"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/dataTables.tableTools.js?ver=<?php echo VERSION;?>"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/dataTables.bootstrap.js?ver=<?php echo VERSION;?>"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/dataTables.responsive.js?ver=<?php echo VERSION;?>"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/tables-data.js?ver=<?php echo VERSION;?>"></script>
</body>
</html>