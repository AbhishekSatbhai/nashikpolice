<?php $this->load->view('admin/includes/header');?>
<section class="page">
	<?php $this->load->view('admin/includes/sidebar');?>
	<div id="wrapper">
		<div class="content-wrapper container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title">
						<h1>Gallery Details<small></small></h1>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url()?>admin/dashboard"><i class="fa fa-home"></i></a></li>
							<li class="active">Gallery Details</li>
						</ol>
					</div>
				</div>
				</div><!-- end .page title-->
				
				<?php if($this->session->flashdata('success_message')!=null){?>
				<div class="col-sm-12 margin-b-30">
					<div class="alert alert-success" style="margin-bottom:0px;">
						<?php echo $this->session->flashdata('success_message');?>
					</div>
				</div>
				<?php }?>
				
				
				<div class="col-md-8 margin-b-30">
					<div class="profile-edit">
						<form class="form-horizontal" method="post" name="frmSubmit" id="frmSubmit" enctype="multipart/form-data">
							<fieldset>
								
								
								<div class="form-group">
									<div class="col-md-8">
										<input type="hidden" class="form-control" id="in_category" name="in_category" value="<?php echo $this->input->get('cid'); ?>">
										<input type="hidden" class="form-control" id="in_category_id" name="in_category_id" value="<?php echo $this->input->get('pid'); ?>">
									</select>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_image">Image</label>
								<div class="col-md-8">
									<input type="file" class="form-control" id="st_image" name="st_image" tabindex="9">
									
								</div>
							</div>
							
						</fieldset>
						
						<div class="panel-footer">
							<div class="row">
								<div class="col-md-9 col-md-offset-3">
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $this->load->view('admin/includes/footer');?>
</body>
</html>