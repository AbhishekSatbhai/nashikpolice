<?php $this->load->view('admin/includes/header');?> 

<section class="page">

	<?php $this->load->view('admin/includes/sidebar');?> 

	<div id="wrapper">
		<div class="content-wrapper container">
			<div class="row">
				<div class="col-sm-12">
					<div class="page-title">
						<h1>Edit feedback <small></small></h1>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url()?>admin/dashboard"><i class="fa fa-home"></i></a></li>
							<li><a href="<?php echo base_url()?>admin/feedback"><i class="fa fa-users"></i></a></li>
							<li class="active">Edit feedback</li>
						</ol>
					</div>
				</div>
			</div><!-- end .page title-->
			
			<?php if($this->session->flashdata('success_message')!=null){?>
				<div class="col-sm-12 margin-b-30">
					<div class="alert alert-success" style="margin-bottom:0px;">
						<?php echo $this->session->flashdata('success_message');?>
					</div>
				</div>
			<?php }?>
			
			
			<div class="col-md-4 margin-b-30">
				<div class="profile-overview">
					
					<table class="table profile-detail table-condensed table-hover">
						<thead>
							<tr>
								<h3 class="mb-xlg">Information</h3>
							</tr>
						</thead>
						<tbody>
								
						
							<tr>
								<td> Name:</td>
								<td><?php echo $result->st_name?></td>
							</tr>
							
							<tr>
								<td> Address:</td>
								<td><?php echo $result->st_address?></td>
							</tr>
							
							
							<tr>
								<td>Message:</td>
								<td><?php echo $result->st_message?></td>
							</tr>
							
							
						</tbody>
					</table>
					
						
				</div>
			</div>
			<div class="col-md-8 margin-b-30">
				<div class="profile-edit">
					<form class="form-horizontal" method="post" name="frmSubmit" id="frmSubmit" enctype="multipart/form-data">
						<h3 class="mb-xlg">Feedback Information</h3> <h5 class="mb-xlg text-right"><span style="color:#FF0000;">*</span> fields should be mandatory</h5>
						<fieldset>
						
							
						
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_name">Name: <span style="color:#FF0000;">*</span></label>
								<div class="col-md-8">
									<input type="text" class="form-control" id="st_name" name="st_name" value="<?php echo($this->input->post('st_name')!=NULL) ? $this->input->post('st_name') : $result->st_name;?>" tabindex="1">
									<label id="st_name-error" class="error" for="st_name" style="display:<?php echo(form_error('st_name')) ? 'block' : 'none';?>"><?php echo form_error('st_name');?></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_address">Address:<span style="color:#FF0000;">*</span></label>
								<div class="col-md-8">
									<textarea class="form-control" id="st_address" name="st_address"><?php echo($this->input->post('st_address')!=NULL) ? $this->input->post('st_address') : $result->st_address;?></textarea>
								</div>
							</div>
							
						
							<div class="form-group">
									<label class="col-md-3 control-label" for="st_image">Photo:</label>
									<div class="col-md-8">
										<input type="file" class="form-control" id="st_image" name="st_image" tabindex="9">
										</br>
										<div class="avtar text-center">
											<?php $image = (!empty($result->st_image)) ? $result->st_image : 'profile_pic.png'?>
											<img height="100px;" width="100px;" src="<?php echo MEDIA.'feedback_images/'.$image;?>" alt="Select L" class="img-thumbnail">
											<h3><?php echo $result->st_image;?></h3>
											
										</div>
									</div>
								</div>
						
							<div class="form-group">
								<label class="col-md-3 control-label" for="st_message">Message:<span style="color:#FF0000;">*</span></label>
								<div class="col-md-8">

									<textarea class="form-control" id="st_message" name="st_message"><?php echo($this->input->post('st_message')!=NULL) ? $this->input->post('st_message') : $result->st_message;?></textarea>
									
								</div>
							</div>
							
							
						</fieldset>
						
						<div class="panel-footer">
							<div class="row">
								<div class="col-md-9 col-md-offset-3">
									<button type="submit" class="btn btn-primary" tabindex="13">Submit</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div> 
	</div>
</section>

<?php $this->load->view('admin/includes/footer');?> 

<link href="<?php echo ADMIN_ASSETS;?>bootstrap-datepicker/css/bootstrap-datepicker.min.css?ver=<?php echo VERSION;?>" rel="stylesheet">
<script type="text/javascript" src="<?php echo ADMIN_ASSETS;?>bootstrap-datepicker/js/bootstrap-datepicker.min.js?ver=<?php echo VERSION;?>"></script>

<script src="<?php echo ADMIN_ASSETS;?>jquery_validation/js/jquery.validate.js?ver=<?php echo VERSION;?>"></script>
<link href="<?php echo ADMIN_ASSETS;?>jquery_validation/css/cmxform.css?ver=<?php echo VERSION;?>" rel="stylesheet">

<script>
$('#dt_birth_date').datepicker({orientation: 'bottom auto', autoclose: true,});

$("#frmSubmit").validate({
	rules: {
		st_name: {
			required: true
		},
		st_address: {
			required: true
		},
		st_message: {
			required: true
		}

		
	},

	messages: {
		st_name: "Please enter name.",
		st_address	: "Please enter Address.",
		st_message	: "Please enter Message."
		
			},
});
</script>

</body>
</html>

