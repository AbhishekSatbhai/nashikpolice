<?php $this->load->view('admin/includes/header');?>
<section class="page">
	<?php $this->load->view('admin/includes/sidebar');?>
	<div id="wrapper">
		<div class="content-wrapper container">
			<div class="row">
				<div class="col-sm-6">
					<div class="page-title">
						<h1>Completed Requests<small></small></h1>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url()?>admin/dashboard"><i class="fa fa-home"></i></a></li>
							<li class="active">Completed Request list</li>
						</ol>
					</div>
				</div>
				
				</div><!-- end .page title-->
				
				<?php if($this->session->flashdata('success_message')!=null){?>
				<div class="row">
					<div class="col-sm-12 margin-b-30">
						<div class="alert alert-success" style="margin-bottom:0px;">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							<?php echo $this->session->flashdata('success_message');?>
						</div>
					</div>
				</div>
				<?php }?>
				
				<div class="row users-row">
					
					<div class="col-md-12">
						<div class="panel panel-card ">
							<!-- Start .panel -->
							<div class="panel-heading">
								<h4 class="panel-title">Details</h4>
								<div class="panel-actions">
									<a href="<?php echo base_url();?>admin/completed_request/download_excel" class="btn btn-success" style="color:white;">Download Excel</a>
									<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
								</div>
							</div>
							<div class="panel-body">
								<form method="get" name="searchForm" id="searchForm" action="<?php echo base_url();?>admin/completed_request">
									<div class="table-responsive"> 
									<table id="result-datatables" class="table table-bordered" cellspacing="0" width="100%" class="w3-ul w3-card-4" style="width:50%">
										<thead>
											
											<tr>
												<th>#</th>
												<th style="font-weight:700;">Request Date</th>	
												<th style="font-weight:700;">Name</th>
												<th style="font-weight:700;">Mobile Number</th>
												<th style="font-weight:700;">Address</th>
												<th style="font-weight:700;">Email</th>
												<th style="font-weight:700;">Pincode</th>
												<th style="font-weight:700;">Collection Date</th>
												<th style="font-weight:700;">Remark</th>
												<th style="font-weight:700;">Nearest Police Station</th>
												<?php /*<th style="font-weight:700;">Identity No</th>
												<th style="font-weight:700;">Identity Photo</th>*/?>
												<th style="font-weight:700;">Status</th>
												<th style="font-weight:700;">Action</th>
												<th style="font-weight:700;">Item-Quantity</th>
											</tr>
										</thead>
										<tbody>
											<?php if(!empty($result)) {
												$i=($page==0) ? 1 : $page + 1;
											foreach($result as $row) {
											?>
											<tr>
												<td><?php echo $i;?></td>
												<td><?php echo date('d-m-Y', strtotime($row->dt_added_date));?></td>
												<td><?php echo $row->st_name;?></td>
												<td><?php echo $row->st_telephone;?></td>
												<td><?php echo $row->st_address;?></td>
												<td><?php echo $row->st_email;?></td>
												<td><?php echo $row->st_answer;?></td>
												<td><?php echo ($row->st_help_date != '0000-00-00') ? date('d-m-Y', strtotime($row->st_help_date)) :'';?></td>
												<td><?php echo $row->st_remark;?></td>
												<td><?php echo $row->police_station;?></td>
												<?php /*<td><?php echo $row->st_identity_number;?></td>
												<td>
													<?php if(!empty($row->st_identity_image)){?>
													<a data-fancybox href="<?php echo MEDIA?>provider_images/<?php echo $row->st_identity_image?>">
													<img src="<?php echo MEDIA?>provider_images/<?php echo $row->st_identity_image?>" height="100px"/>
													</a>
													<?php }?>
												</td> */?>
												<td>
													<select class="form-control status" data-id="<?php echo $row->in_id?>">
														<option value="1" <?php echo ($row->in_status == 1) ? 'selected' : '';?>>New Request</option>
														<option value="2" <?php echo ($row->in_status == 2) ? 'selected' : '';?>>Completed</option>
														<option value="3" <?php echo ($row->in_status == 3) ? 'selected' : '';?>>Cancel</option>
														<option value="4" <?php echo ($row->in_status == 4) ? 'selected' : '';?>>Hold</option>
													</select>
												</td>
												<td>
													<a href="<?php echo base_url();?>admin/completed_request/delete/<?php echo $row->in_id?>" class="warning-alert btn btn-danger btn-xs" onclick="return confirm('Are you sure you want to delete this?');" <?php echo privilege('flg_delete_customer');?>><i class="fa fa-trash"></i></a>
												</td>
												<td>
													<button class = "btn btn-primary btn-block item" font-size = "9px" data-id="<?php echo $row->in_id?>">View</button>
												</td>
											</tr>
											<?php $i++; } } ?>
											
										</tbody>
									</table>
									</div>
								</form>
								
								<?php if(!empty($result)) {?>
								<div class="row">
									<?php echo $pagination;?>
								</div>
								<?php }?>
								
							</div>
							</div><!-- End .panel -->
						</div>
						
						
					</div>
				</div>
			</div>
		</section>
		<?php $this->load->view('admin/includes/footer');?>
		<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/jquery.dataTables.js?ver=<?php echo VERSION;?>"></script>
		<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/dataTables.tableTools.js?ver=<?php echo VERSION;?>"></script>
		<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/dataTables.bootstrap.js?ver=<?php echo VERSION;?>"></script>
		<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/dataTables.responsive.js?ver=<?php echo VERSION;?>"></script>
		<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/tables-data.js?ver=<?php echo VERSION;?>"></script>
		<script>
		$(document).ready(function() {

		$('.status').change(function(){
			var status_id = $(this).val();
			var result_id = $(this).attr('data-id');
			bootbox.confirm("Are you sure want to change this status?", function(result) { if(result) {
			
				$.ajax({
				  type:'POST',
				  url : '<?php echo base_url();?>admin/supplier_request/change_status',
				  data: "result_id="+result_id+"&status_id="+status_id,
				  success: function(data_result){
					
					  data_result_array = JSON.parse(data_result);
					   if(data_result_array.result==0)
						{
							var data_result_array_message = data_result_array.message;
							
							bootbox.alert(data_result_array.message, function() { });
						}
						else
						{
							bootbox.alert(data_result_array.message, function() { 
								//window.location.href="<?php echo base_url()?>admin/category";
							});
						}
				  }
				});
			
			} });
		  
		  
		})

		$('.item').click(function(event){
			event.preventDefault();
			var result_id = $(this).attr('data-id');
			$.ajax({
				url:'<?php echo base_url();?>admin/completed_request/get_items',
				type: 'POST',
				data:"result_id="+result_id,
				success:function(data){
					$('#items').html(data);
					$('#dataModal').modal("show");
					event.preventDefault();
				}
			})
			
			

		});
		
		$('#result-datatables').DataTable( {
				"sDom": '<"H"lfr>t<"F">',
				"columnDefs": [ {
					"targets": [4],
					"orderable": false
				} ],
				language: {
					emptyTable: 'Record Not Found',
					zeroRecords: 'Record Not Found'
				},
				"pageLength": 100,
		} );
			
			$('select[name="result-datatables_length"]').change(function(){
				$('#searchForm').submit();
			});
			
			$('input[type="search"]').attr('name','search_keyword');
			
			$('input[name="search_keyword"]').keyup(function(e){
				if(e.keyCode==13){
					$('#searchForm').submit();
				}
			});
			
		$('select[name="result-datatables_length"]').val('<?php echo $search_per_page?>');
		$('input[name="search_keyword"]').val('<?php echo $search_keyword?>');
		
		} );
		</script>
	</body>
</html>
<div id = "dataModal" class = "modal fade">
		<div class = "modal-dialog">
			<div class = "modal-content">
				<div class = "modal-header">
					<button class = "close" data-dismiss = "modal">&times;</button>
					<h4 class = "modal-title">Items and Quantities</h4>
				</div>
				<div class = "modal-body" id = "items">
				</div>
				<div class = "modal-footer">
					<button class = "btn btn-default btn-block" data-dismiss = "modal">Close</button>
				</div>
			</div>
		</div>
</div>
