<?php $this->load->view('includes/header');?>	
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<div class="course-reserve">
		<div class="container">
			<div class="row">
				
				<div class="col-sm-6 col-sm-offset-3 request_form">
					<div class="col-sm-12 text-center">
						<h2 class="section-title">Request as <strong>Delivery Person</strong></h2>
					</div>	
					<form class="" method="post" id="registerFrm" name="registerFrm" enctype="multipart/form-data">
						
							
								<div class="form-group">
									<label class="control-label">Name <span style="color:#d93025">*</span></label>
									<input type="text" class="form-control" name="st_name" id="st_name" value="<?php echo($this->input->post('st_name')!=NULL) ? $this->input->post('st_name') : ''?>">
									<label id="st_name-error" class="error" for="st_name" style="display:<?php echo(form_error('st_name')) ? 'block' : 'none';?>"></label>
								</div>
								<div class="form-group">
									<label class="control-label">Contact Number <span style="color:#d93025">*</span></label>
									<input type="text" class="form-control" name="st_mobile_number" id="st_mobile_number" value="<?php echo($this->input->post('st_mobile_number')!=NULL) ? $this->input->post('st_mobile_number') : ''?>">
									<label id="st_mobile_number-error" class="error" for="st_mobile_number" style="display:<?php echo(form_error('st_mobile_number')) ? 'block' : 'none';?>"></label>
								</div>
								<div class="form-group">
									<label class="control-label">Your Location <span style="color:#d93025">*</span></label>
									<input type="text" class="form-control" name="st_address" id="st_address" value="<?php echo($this->input->post('st_address')!=NULL) ? $this->input->post('st_address') : ''?>">
									<label id="st_address-error" class="error" for="st_address" style="display:<?php echo(form_error('st_address')) ? 'block' : 'none';?>"></label>
								</div>
								
								<div class="form-group">
									<label class="control-label">Your Vehicle Name <span style="color:#d93025">*</span></label>
									<input type="text" class="form-control" name="st_vehicle" id="st_vehicle" value="<?php echo($this->input->post('st_vehicle')!=NULL) ? $this->input->post('st_vehicle') : ''?>">
									<label id="st_vehicle-error" class="error" for="st_vehicle" style="display:<?php echo(form_error('st_vehicle')) ? 'block' : 'none';?>"></label>
								</div>
								
								<div class="form-group">
									<label class="control-label">Vehicle Number <span style="color:#d93025">*</span></label>
									<input type="text" class="form-control" name="st_vehicle_number" id="st_vehicle_number" value="<?php echo($this->input->post('st_vehicle_number')!=NULL) ? $this->input->post('st_vehicle_number') : ''?>">
									<label id="st_vehicle_number-error" class="error" for="st_vehicle_number" style="display:<?php echo(form_error('st_vehicle_number')) ? 'block' : 'none';?>"></label>
								</div>
								
								<div class="form-group">
									<label class="control-label">Nearest Police Station <span style="color:#d93025">*</span></label>
									<?php $st_nearest_police_station = ($this->input->post('st_nearest_police_station')!=NULL) ? $this->input->post('st_nearest_police_station') : ''?>
									<select class="form-control"  name="st_nearest_police_station" id="st_nearest_police_station" >
										<?php if(!empty($police_stations)){ foreach($police_stations as $row){?>
										<option value="<?php echo $row->in_id?>" <?php echo ($st_nearest_police_station == $row->in_id) ? 'selected' : '';?>><?php echo $row->st_name?></option>
										<?php }}?>
									</select>
									<label id="st_nearest_police_station-error" class="error" for="st_nearest_police_station" style="display:<?php echo(form_error('st_nearest_police_station')) ? 'block' : 'none';?>"></label>
								</div>
								
								<div class="form-group">
									<label class="control-label">Your Aadhar Number <span style="color:#d93025">*</span></label>
									<input type="text" class="form-control" name="st_aadhar_number" id="st_aadhar_number" value="<?php echo($this->input->post('st_aadhar_number')!=NULL) ? $this->input->post('st_aadhar_number') : ''?>">
									<label id="st_aadhar_number-error" class="error" for="st_aadhar_number" style="display:<?php echo(form_error('st_aadhar_number')) ? 'block' : 'none';?>"></label>
								</div>
								
								<div class="form-group">
									<label class="control-label">Upload Aadhar Photo <span style="color:#d93025">*</span></label>
									<input type="file" name="st_aadhar_image" id="st_aadhar_image" >
									<small>only PNG, JPG, JPEG format accept</small>
									<label id="st_aadhar_image-error" class="error" for="st_aadhar_image" style="display:<?php echo(form_error('st_aadhar_image')) ? 'block' : 'none';?>"></label>
								</div>
								
								<div class="form-group">
									<label class="control-label">Upload Driving License Photo <span style="color:#d93025">*</span></label>
									<input type="file" name="st_driving_image" id="st_driving_image" >
									<small>only PNG, JPG, JPEG format accept</small>
									<label id="st_driving_image-error" class="error" for="st_driving_image" style="display:<?php echo(form_error('st_driving_image')) ? 'block' : 'none';?>"></label>
								</div>
								
								<div class="control-group">
									<div class="column one g-recaptcha" data-sitekey="<?php echo GOOGLE_CAPTCHA_SITE_KEY;?>" style="transform:scale(0.85);-webkit-transform:scale(0.85);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
									<div id="robot_message" class="alert alert-danger" style="display:none;"></div>
								</div>
							
							<div class="clearfix"></div>
							<div class="col-sm-12 text-center">
								<button type="submit" class="btn btn-blue btn-lg">Submit</button>
							</div>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	
<?php $this->load->view('includes/footer');?>	


<script>
$(function () {
	
	$("#registerFrm").validate({
		rules: {
			st_name: {
				required: true,
			},
			st_mobile_number: {
				required: true
			},					
			st_address: {
				required: true
			},
			st_vehicle: {
				required: true
			},
			st_vehicle_number: {
				required: true
			},
			st_nearest_police_station: {
				required: true
			},
			st_aadhar_number: {
				required: true
			},
			st_aadhar_image: {
				required: true
			},
			st_driving_image: {
				required: true
			},
			
		},
	
		messages: {
			st_name: "Please enter name",
			st_address: "Please enter address",
			st_mobile_number: "Enter your contact number",
			st_vehicle: "Please enter your Vehicle Name",
			st_vehicle_number: "Please enter Vehicle Number",
			st_nearest_police_station: "Please enter your Nearest Police Station",
			st_aadhar_number: "Please enter your Aadhar Number",
			st_aadhar_image: "Please upload your Aadhar Photo",
			st_driving_image: "Please upload your Driving License Photo",
		},
		
		 submitHandler: function (form) {
			
			//var formdata = $("#frmSubmit").serialize();
			
			$('#robot_message').html('');
			$('#robot_message').hide();
			$('.loader').show();
			var site_url = '<?php echo base_url();?>apply_for_delivery_person/action';
				
			var formData = new FormData($('#registerFrm')[0]);
				
			$.ajax({
				type:'POST',
				url : site_url,
				data: formData,
				processData: false,
				contentType: false,
				
				success: function(result_data){
					data_result_array = JSON.parse(result_data);
					if(data_result_array.result==0)
					{
						if(data_result_array.message){
							var data_result_array_message = data_result_array.message;
							for(var i in data_result_array_message)
							{
								$('#'+i+'-error').html(data_result_array_message[i]);
								$('#'+i+'-error').show();
							}
						}
						if(data_result_array.robot_message)
						{
							$('#robot_message').html(data_result_array.robot_message);
							$('#robot_message').show();
						}
						$('.loader').hide();
					}
					else
					{
						$('.loader').hide();
						window.location.href="<?php echo base_url().'apply_for_delivery_person'?>";
						
					}

				}
			});	
		},
	});	
		
});
</script>
</body>
</html>