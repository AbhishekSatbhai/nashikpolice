
<?php $this->load->view('includes/header');?>	
	<!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
	<div class="course-reserve">
		<div class="container">
			<div class="row">

		<div class="col-sm-3 request_form" style="background-color:#003663; color:white;">
				<nav class="navbar navbar-default navbar-static-top">
            <div class="">
                <div class="navbar-header">
				<a href="" class="navbar-brand"  style="background-color:#003663; color:white;">Required Quantity</a>
                    <button type="button" class="navbar-toggle collapsed " data-toggle="collapse" data-target="#navbar1" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span href="" style="color:Black;font-weight:bold;" class="blinking">Click here</span>
                    </button>
                </div>
                <div id="navbar1" class="navbar-collapse collapse" style="background-color:#003663; color:white;">
				<table class="table">
						<tr>
							<th>
								No
							</th>
							<th>
								Item
							</th>
							<th>
								Req. Quantity
							</th>
						</tr>
						<tbody>
							<?php if(!empty($result)) {
							$i=($page==0) ? 1 : $page + 1;	
							foreach($result as $row) {
							?>	
								<tr>
									<td><?php echo $i;?></td>
									<td><?php echo $row->st_name;?></td>
									
									<td><?php echo $row->in_qty_stock;?></td>		
								
								</tr>
							<?php $i++; } }?>
							</tbody>
					</table>
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>
					
		</div>

		<style>
		
.blinking{
    animation:blinkingText 1s infinite;
}
@keyframes blinkingText{
    0%{     color: #000;    }
    49%{    color: #000; }
    60%{    color: red; }
    99%{    color:red;  }
    100%{   color: #000;    }
}
</style>

				
				<div class="col-sm-6  request_form">
				
					<div class="col-sm-12 text-center">
						<h3 class="section-title"><?php echo lang('Request as Provider'); ?></h3>
					</div>
						
					<form class="" method="post" id="registerFrm" name="registerFrm" enctype="multipart/form-data">
								
					<div class="form-group">
									<input type="checkbox" id="food_cate" name="food_cate" value="food" onclick="ShowHideDiv1(this)">
									<label for="food_cate"> Fruits / फळे </label><br>

									<div class="form-group" id="food_cate_div" style="display:none">
										<div class="row">
											<div class="col-sm-6">
											<select  class="form-control" name="food1" id="food1" >
									
											</select>
										</div>
											<div class="col-sm-6">
												<input type="number" name="food1_qty" class="form-control" placeholder="Quantity (kg) / संख्या ">
												</div>
											<br>
											<div class="col-sm-6">
												<select  class="form-control" name="food2" id="food2" >
									
												</select>
											</div>
											<div class="col-sm-6">
												<input type="number" name="food2_qty" class="form-control" placeholder="Quantity (kg) / संख्या ">
											</div>

										</div>
									
									</div>

									<script type="text/javascript">
    									function ShowHideDiv1(food_cate) {
        								var food_cate_div = document.getElementById("food_cate_div");
        								food_cate_div.style.display = food_cate.checked ? "block" : "none";
   											 }
									</script>

								</div>

								<div class="form-group">
									<input type="checkbox" id="grocery_cate" name="grocery_cate" value="grocery" onclick="ShowHideDiv2(this)">
									<label for="grocery_cate">Food , Water / अन्न , पाणी </label><br>

									<div class="form-group" id="grocery_cate_div" style="display:none">
										<div class="row">
											<div class="col-sm-6">
												<select  class="form-control" name="grocery1" id="grocery1" >
									
												</select>
											</div>
											<div class="col-sm-6">
												<input type="number" name="grocery1_qty" class="form-control" placeholder="Quantity / संख्या ">
											</div>
											<br>
											<div class="col-sm-6">
												<select  class="form-control" name="grocery2" id="grocery2" >

												</select>
											</div>
											<div class="col-sm-6">
												<input type="number" name="grocery2_qty" class="form-control" placeholder="Quantity / संख्या ">
											</div>

										</div>
									
									</div>

									<script type="text/javascript">
    									function ShowHideDiv2(grocery_cate) {
        								var grocery_cate_div = document.getElementById("grocery_cate_div");
        								grocery_cate_div.style.display = grocery_cate.checked ? "block" : "none";
   											 }
									</script>

								</div>


								
								<div class="form-group">
									<input type="checkbox" id="medical_cate" name="medical_cate" value="medical" onclick="ShowHideDiv3(this)">
									<label for="medical_cate"> Medical / वैद्यकीय</label><br>

									<div class="form-group" id="medical_cate_div" style="display:none">
										<div class="row">
											<div class="col-sm-6">
												<select  class="form-control" name="medical1" id="medical1" >
									
												</select>
											</div>
											<div class="col-sm-6">
												<input type="number" name="medical1_qty" class="form-control" placeholder="Quantity / संख्या ">
											</div>
											<br>
											<div class="col-sm-6">
												<select  class="form-control" name="medical2" id="medical2" >

												</select>
											</div>
											<div class="col-sm-6">
												<input  type="number" name="medical2_qty" class="form-control" placeholder="Quantity / संख्या ">
											</div>

										</div>
									
									</div>

									<script type="text/javascript">
    									function ShowHideDiv3(medical_cate) {
        								var medical_cate_div = document.getElementById("medical_cate_div");
        								medical_cate_div.style.display = medical_cate.checked ? "block" : "none";
   											 }
									</script>

								</div>

												
								<div class="form-group">
									<input type="checkbox" id="dry_fruits_cate" name="dry_fruits_cate" value="dry_fruits" onclick="ShowHideDiv4(this)">
									<label for="dry_fruits_cate"> Dry Fruits / ड्राय फ्रुटस </label><br>

									<div class="form-group" id="dry_fruits_cate_div" style="display:none">
										<div class="row">
											<div class="col-sm-6">
												<select  class="form-control" name="dry_fruits1" id="dry_fruits1" >
									
												</select>
											</div>
											<div class="col-sm-6">
												<input type="number" name="dry_fruits1_qty" class="form-control" placeholder="Quantity / संख्या ">
											</div>
											<br>
											<div class="col-sm-6">
												<select  class="form-control" name="dry_fruits2" id="dry_fruits2" >

												</select>
											</div>
											<div class="col-sm-6">
												<input  type="number" name="dry_fruits2_qty" class="form-control" placeholder="Quantity / संख्या ">
											</div>

										</div>
									
									</div>

									<script type="text/javascript">
    									function ShowHideDiv4(dry_fruits_cate) {
        								var dry_fruits_cate_div = document.getElementById("dry_fruits_cate_div");
        								dry_fruits_cate_div.style.display = dry_fruits_cate.checked ? "block" : "none";
   											 }
									</script>

								</div>


								<div class="form-group">
									<input type="checkbox" id="other_cate" name="other_cate" value="other" onclick="ShowHideDiv5(this)">
									<label for="other_cate">Other / अन्य </label><br>

									<div class="form-group" id="other_cate_div" style="display:none">
										<div class="row">
											<div class="col-sm-6">
												<input type="text" placeholder="Other Items / इतर वस्तू चे नाव " name="other" id="other" class="form-control">
											</div>
											<div class="col-sm-6">
												<input type="number" name="other_qty" class="form-control" placeholder="Quantity / संख्या ">
											</div>
											<br>
											
										</div>
									
									</div>

									<script type="text/javascript">
    									function ShowHideDiv5(other_cate) {
        								var other_cate_div = document.getElementById("other_cate_div");
        								other_cate_div.style.display = other_cate.checked ? "block" : "none";
   											 }
									</script>

								</div>





								 <div class="form-group" style="display:none">
									<label class="control-label"><?php echo lang('Your Help By'); ?> <span style="color:#d93025">*</span></label>
									<?php $st_help_type = ($this->input->post('st_help_type')!=NULL) ? $this->input->post('st_help_type') : ''?>
									<select class="form-control" name="st_help_type" id="st_help_type" onchange="change_values(st_help_type.value)">
									<?php foreach($desc_list as $list){ ?>
											<option value="<?php echo $list->st_description ?>"><?php echo $list->st_description ?></option>
										<?php } ?>
									</select>	
									<label id="st_help_type-error" class="error" for="st_help_type" style="display:<?php echo(form_error('st_help_type')) ? 'block' : 'none';?>"></label>
								</div>
								<div class="form-group"  style="display:none">
									<label class="control-label"><?php echo lang('Other Help'); ?></label>
									
									 <select  class="form-control" name="st_help_remark" id="st_help_remark" >
									
									</select>
									
									
									
									<label id="st_help_remark-error" class="error" for="st_help_remark" style="display:<?php echo(form_error('st_help_remark')) ? 'block' : 'none';?>"></label>
								</div>
								
								
								
								<div class="form-group">
									<label class="control-label"><?php echo lang('Name'); ?> <span style="color:#d93025">*</span></label>
									<input type="text" class="form-control" name="st_name" id="st_name" value="<?php echo($this->input->post('st_name')!=NULL) ? $this->input->post('st_name') : ''?>">
									<label id="st_name-error" class="error" for="st_name" style="display:<?php echo(form_error('st_name')) ? 'block' : 'none';?>"></label>
								</div>
								<div class="form-group">
									<label class="control-label"><?php echo lang('Contact Number'); ?> <span style="color:#d93025">*</span></label>
									<input type="text" class="form-control" name="st_telephone" id="st_telephone" value="<?php echo($this->input->post('st_telephone')!=NULL) ? $this->input->post('st_telephone') : ''?>">
									<label id="st_telephone-error" class="error" for="st_telephone" style="display:<?php echo(form_error('st_telephone')) ? 'block' : 'none';?>"></label>
								</div>
								<div class="form-group">
									<label class="control-label"><?php echo lang('Your Location'); ?> <span style="color:#d93025">*</span></label>
									<input type="text" class="form-control" name="st_address" id="st_address" value="<?php echo($this->input->post('st_address')!=NULL) ? $this->input->post('st_address') : ''?>">
									<label id="st_address-error" class="error" for="st_address" style="display:<?php echo(form_error('st_address')) ? 'block' : 'none';?>"></label>
								</div>


								<div class="form-group">
									<label class="control-label"><?php echo lang('Pin Code'); ?><span style="color:#d93025">*</span></label>
									<input type="number" class="form-control" name="st_answer" id="st_answer" value="<?php echo($this->input->post('st_answer')!=NULL) ? $this->input->post('st_answer') : ''?>">
									<label id="st_answer-error" class="error" for="st_answer" style="display:<?php echo(form_error('st_answer')) ? 'block' : 'none';?>"></label>
								</div> 
								
								<div class="form-group">
									<label class="control-label"><?php echo lang('Email ID'); ?> *</label>
									<input type="text" class="form-control" name="st_email" id="st_email" value="<?php echo($this->input->post('st_email')!=NULL) ? $this->input->post('st_email') : ''?>">
									<label id="st_email-error" class="error" for="st_email" style="display:<?php echo(form_error('st_email')) ? 'block' : 'none';?>"></label>
								</div>
								
								
								
								<input type="hidden" value="1" class="form-control" name="st_tehsil" id="st_tehsil">
								
								<div class="form-group">
									<label class="control-label"><?php echo lang('Nearest Police Station'); ?> <span style="color:#d93025">*</span></label>
									<?php $st_nearest_police_station = ($this->input->post('st_nearest_police_station')!=NULL) ? $this->input->post('st_nearest_police_station') : ''?>
									<select class="form-control"  name="st_nearest_police_station" id="st_nearest_police_station" >
										<?php if(!empty($police_stations)){ foreach($police_stations as $row){?>
										<option value="<?php echo $row->in_id?>" <?php echo ($st_nearest_police_station == $row->in_id) ? 'selected' : '';?>><?php echo $row->st_name?></option>
										<?php }}?>
									</select>
									<label id="st_nearest_police_station-error" class="error" for="st_nearest_police_station" style="display:<?php echo(form_error('st_nearest_police_station')) ? 'block' : 'none';?>"></label>
								</div>
								
								<div class="form-group">
									<label class="control-label"><?php echo lang('Help Date'); ?></label>
									<input type="date" class="form-control" name="st_help_date" id="date" min="<?php echo date("Y-m-d"); ?>" value="" data-date-format="dd-mm-yyyy">
									<label id="st_help_date-error" class="error" for="st_help_date" style="display:<?php echo(form_error('st_help_date')) ? 'block' : 'none';?>"></label>
								</div>
								
								<div class="form-group">
									<label class="control-label"><?php echo lang('Remark'); ?></label>
									<textarea rows="5" class="form-control" name="st_remark" id="st_remark" value=""><?php echo($this->input->post('st_remark')!=NULL) ? $this->input->post('st_remark') : ''?></textarea>
									<label id="st_remark-error" class="error" for="st_remark" style="display:<?php echo(form_error('st_remark')) ? 'block' : 'none';?>"></label>
								</div>

								
								
								<?php /*<div class="form-group">
									<label class="control-label"><?php echo lang('Your Identity Number'); ?> <span style="color:#d93025">*</span></label>
									<input type="text" class="form-control" name="st_identity_number" id="st_identity_number" value="<?php echo($this->input->post('st_identity_number')!=NULL) ? $this->input->post('st_identity_number') : ''?>">
									<small><?php echo lang('Aadhar Number or NGO registration number'); ?></small>		
									<label id="st_identity_number-error" class="error" for="st_identity_number" style="display:<?php echo(form_error('st_identity_number')) ? 'block' : 'none';?>"></label>
								</div>
								
								<div class="form-group">
									<label class="control-label"><?php echo lang('Upload Identity Photo'); ?> <span style="color:#d93025">*</span></label>
									<input type="file" name="st_identity_image" id="st_identity_image" >
									<small><?php echo lang('only PNG, JPG, JPEG format accept'); ?> </small>
									<label id="st_identity_image-error" class="error" for="st_identity_image" style="display:<?php echo(form_error('st_identity_image')) ? 'block' : 'none';?>"></label>
								</div>
								
								<div class="control-group">
									<div class="column one g-recaptcha" data-sitekey="<?php echo GOOGLE_CAPTCHA_SITE_KEY;?>" style="transform:scale(0.85);-webkit-transform:scale(0.85);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
									<div id="robot_message" class="alert alert-danger" style="display:none;"></div>
								</div>*/ ?>
							<div class="col-sm-12 text-center">
								<button type="submit" class="btn btn-blue btn-lg">Submit</button>
								
							</div>
							</div>
							
					</form>
				</div>
			</div>
		</div>
	</div>
	
<?php $this->load->view('includes/footer');?>	

<link href="<?php echo ASSETS;?>bootstrap-datepicker/css/bootstrap-datepicker.min.css?ver=<?php echo VERSION;?>" rel="stylesheet">
<script type="text/javascript" src="<?php echo ASSETS;?>bootstrap-datepicker/js/bootstrap-datepicker.min.js?ver=<?php echo VERSION;?>"></script>
<script>
	var users = <?php echo json_encode($item_names); ?>;
	i=0
	var option_values = "<option value='None'>Select Item / निवडा</option>";
	for (x of users) {
		if(x.st_description == "Fruits"){
			option_values = option_values + "<option value='" + x.st_name + "'>" + x.st_name +"</option>";
			console.log(x.st_name + "<br >");
		}
		
  i++;
}
document.getElementById('food1').innerHTML= option_values;
document.getElementById('food2').innerHTML= option_values;

i=0
	var option_values1 =  "<option value='None'>Select Item / निवडा</option>";
	for (x of users) {
		if(x.st_description == "Grocery"){
			option_values1 = option_values1 + "<option value='" + x.st_name + "'>" + x.st_name +"</option>";
			console.log(x.st_name + "<br >");
		}
		
  i++;
}
document.getElementById('grocery1').innerHTML= option_values1;
document.getElementById('grocery2').innerHTML= option_values1;


i=0
	var option_values1 =  "<option value='None'>Select Item / निवडा</option>";
	for (x of users) {
		if(x.st_description == "Medical"){
			option_values1 = option_values1 + "<option value='" + x.st_name + "'>" + x.st_name +"</option>";
			console.log(x.st_name + "<br >");
		}
		
  i++;
}
document.getElementById('medical1').innerHTML= option_values1;
document.getElementById('medical2').innerHTML= option_values1;


i=0
	var option_values1 = "<option value='None'>Select Item / निवडा</option>";
	for (x of users) {
		if(x.st_description == "Dry Fruits"){
			option_values1 = option_values1 + "<option value='" + x.st_name + "'>" + x.st_name +"</option>";
			console.log(x.st_name + "<br >");
		}
		
  i++;
}
document.getElementById('dry_fruits1').innerHTML= option_values1;
document.getElementById('dry_fruits2').innerHTML= option_values1;



function change_values(item){
	var users = <?php echo json_encode($item_names); ?>;
	i=0
	var option_values = '';
	for (x of users) {
		if(x.st_description == item){
			option_values = option_values + "<option value='" + x.st_name + "'>" + x.st_name +"</option>";
			console.log(x.st_name + "<br >");
		}
		
  i++;
}
document.getElementById('st_help_remark').innerHTML= option_values;
}


$(function () {
	
	$('#st_help_date').datepicker({ autoclose:true, 'dateFormat': 'dd-mm-yy'});
	
	$("#registerFrm").validate({
		rules: {
			st_name: {
				required: true,
			},
			st_telephone: {
				required: true
			},					
			st_address: {
				required: true
			},
			st_email: {
				required: true
			},
			st_help_type: {
				required: true
			},
			st_answer: {
				required: true
			},
			/*st_help_place: {
				required: true
			},
			st_identity_number: {
				required: true
			},
			st_identity_image: {
				required: true
			},*/
		},
	
		messages: {
			st_name: "<?php echo lang('Please enter Name')?>",
			st_address: "<?php echo lang('Please enter your Location')?>",
			st_telephone: "<?php echo lang('Please enter Contact Number')?>",
			st_email: "<?php echo lang('Please enter valid email address')?>",
			st_help_type: "<?php echo lang('Please select your Help')?>",
			st_answer: "<?php echo lang('Please enter help count')?>",
			/*st_help_place: "<?php echo lang('Please enter help place')?>",
			st_identity_number: "<?php echo lang('Please enter your ID number')?>",
			st_identity_image: "<?php echo lang('Please upload your ID photo')?>", */
		},
		
		 submitHandler: function (form) {
			
			//var formdata = $("#frmSubmit").serialize();
			$('.loader').show();
			$('#robot_message').html('');
			$('#robot_message').hide();
			var site_url = '<?php echo base_url();?>apply_for_provider/action';
				
			var formData = new FormData($('#registerFrm')[0]);
				
			$.ajax({
				type:'POST',
				url : site_url,
				data: formData,
				processData: false,
				contentType: false,
				
				success: function(result_data){
					data_result_array = JSON.parse(result_data);
					if(data_result_array.result==0)
					{
						if(data_result_array.message){
							var data_result_array_message = data_result_array.message;
							for(var i in data_result_array_message)
							{
								$('#'+i+'-error').html(data_result_array_message[i]);
								$('#'+i+'-error').show();
							}
						}
						if(data_result_array.robot_message)
						{
							$('#robot_message').html(data_result_array.robot_message);
							$('#robot_message').show();
						}
						$('.loader').hide();
					}
					else
					{
						$('.loader').hide();
						//window.location.href="<?php echo base_url().'apply_for_provider'?>";
						window.location.href="";
						
					}

				}
			});	
		},
	});	
		
});


</script>

<!-- Info Modal open at the time of page load page loads -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel" style="text-align:center;font-weight:bold;color:#160054;">प्रदाता किंवा देणगीदार म्हणून विनंती</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
	  <img src="<?php echo FRONT_ASSETS; ?>images/info3.jpeg" style="width: 100%;" alt="">
                </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Click to Continue</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>
<script>
    $(document).ready(function(){
        $("#exampleModal").modal('show');
    });
</script>


</body>
</html>







<!-- Developed By dev#Pratik_Khose -->
