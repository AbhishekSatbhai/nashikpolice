<?php $this->load->view('staff/includes/header');?> 

<section class="page">

	<?php $this->load->view('staff/includes/sidebar');?> 

	<div id="wrapper">
		<div class="content-wrapper container">
			<div class="row">
				<div class="col-sm-6">
					<div class="page-title">
						<h1>Items  <small></small></h1>
						<ol class="breadcrumb">
							<li><a href="<?php echo base_url()?>dashboard" title="Home"><i class="fa fa-home"></i></a></li>
							<li class="active">Items</li>
						</ol>
					</div>
				</div>
				
				<div class="col-sm-6 text-right">
					<!-- <a href="javascript:void(0)" onclick="add_result();" class="btn btn-success">Add New Items</a> -->
				</div>
			</div><!-- end .page title-->
			
			<?php if($this->session->flashdata('success_message')!=null){?>
			<div class="row">
				<div class="col-sm-12 margin-b-30">
					<div class="alert alert-success" style="margin-bottom:0px;">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<?php echo $this->session->flashdata('success_message');?>
					</div>
				</div>
			</div>
			<?php }?>
			
			<div class="row users-row">
				<div class="col-md-12">
					<div class="panel panel-card ">
					<!-- Start .panel -->
					<div class="panel-body">
						<form method="get" name="searchForm" id="searchForm" action="<?php echo base_url();?>items">
						<table id="result-datatables" class="table table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th style="font-weight:700;">#</th>
									<th style="font-weight:700;">Item Name</th>
									<th style="font-weight:700;">Category</th>
									<th style="font-weight:700; text-align:right;">Required Quantity</th>
									<th style="font-weight:700;">Action</th>
								</tr>
							</thead>
							<tbody>
							<?php if(!empty($result)) {
							$i=($page==0) ? 1 : $page + 1;	
							foreach($result as $row) {
							?>	
								<tr>
									<td><?php echo $i;?></td>
									<td><?php echo $row->st_name;?></td>
									<td><?php echo $row->st_description;?></td>
									<td style="text-align:right;"><?php echo $row->in_qty_stock;?></td>		
									<td>
									<a disabled href="javascript:void(0)" onclick="get_result('<?php echo $row->in_id ?>');" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Edit Item"><i class="fa fa-edit"></i></a>
									<a disabled href="<?php echo base_url();?>admin/items/delete/<?php echo $row->in_id?>" class="warning-alert btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Delete Item" onclick="return confirm('Are you sure you want to delete this?');"><i class="fa fa-trash"></i></a>
									<a disabled onclick="stock_modal('<?php echo $row->in_id?>')" href="javascript:void(0)" class="btn btn-info btn-xs" data-toggle="tooltip" data-placement="top" title="Add Qty Into Stock"><i class="fa fa-plus"></i></a>
									</td>
								</tr>
							<?php $i++; } }?>
							</tbody>
						</table>
						</form>
						
						<?php if(!empty($result)) {?>  
						<div class="row">
							<?php echo $pagination;?>
						</div>
						<?php }?>
						
					</div>
					</div><!-- End .panel -->  
                </div>
			
			
			</div>
		</div> 
	</div>
</section>

<?php $this->load->view('staff/includes/footer');?> 

<div class="modal fade" id="formModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Item Management</h4>
        </div>
        <form method="post" role="form" name="formSubmit" id="formSubmit" action="" enctype="multipart/form-data" >
        <div class="modal-body">
          <!-- form group-Admin-->
           <section class="panel panel-default">
                    <header class="panel-heading font-bold">
                    Item
					<span id="modal_title"> Add</span>	  
                    </header>
                    <div class="panel-body">
                     
					 	<input type="hidden" name="in_id" id="in_id" value="">
                      	<div class="form-group">
                          <input type="text" class="form-control" placeholder="Name" name="st_name" id="st_name">
						  <label id="st_name-error" class="error" for="st_name" style="display:<?php echo(form_error('st_name')) ? 'block' : 'none';?>"></label>
                        </div>
						
						<div class="form-group">
                          <input type="text" class="form-control" placeholder="Description" name="st_description" id="st_description">
						  <label id="st_description-error" class="error" for="st_description" style="display:<?php echo(form_error('st_description')) ? 'block' : 'none';?>"></label>
                        </div>
						
						<div class="form-group">
                          <input type="text" class="form-control" placeholder="Qty in Stock" name="in_qty_stock" id="in_qty_stock">
						  <label id="in_qty_stock-error" class="error" for="in_qty_stock" style="display:<?php echo(form_error('in_qty_stock')) ? 'block' : 'none';?>"></label>
                        </div>

                    </div>
                  </section>
          <!-- end form group-Admin-->

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button><!-- close-->
          <button type="button" class="btn btn-info" onclick="$('#formSubmit').submit();">Save</button><!-- save-->
        </div>
        </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>

<div class="modal fade" id="frmModal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Add Quantity Into Stock</h4>
        </div>
        <form method="post" role="form" name="frmSubmit" id="frmSubmit" action="" enctype="multipart/form-data" >
        <div class="modal-body">
          <!-- form group-Admin-->
           <section class="panel panel-default">
                    <header class="panel-heading font-bold">
                    Item
					<span id="modal_title"> Stock</span>	  
                    </header>
                    <div class="panel-body">
                      	<div class="form-group">
                          <!-- first name-->
                          <input type="text" class="form-control" placeholder="Qty" name="qty" id="qty">
						  <label id="qty-error" class="error" for="qty" style="display:<?php echo(form_error('qty')) ? 'block' : 'none';?>"></label>
                        </div>
						<input type="hidden" id="item_id" name="item_id" value="">
						<div class="resultlogin"></div>
                    </div>
                  </section>
          <!-- end form group-Admin-->

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button><!-- close-->
          <button type="button" class="btn btn-info" onclick="$('#frmSubmit').submit();">Add</button><!-- save-->
        </div>
        </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>


<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/jquery.dataTables.js?ver=<?php echo VERSION;?>"></script>
<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/dataTables.tableTools.js?ver=<?php echo VERSION;?>"></script>
<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/dataTables.bootstrap.js?ver=<?php echo VERSION;?>"></script>
<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/dataTables.responsive.js?ver=<?php echo VERSION;?>"></script>
<script src="<?php echo ADMIN_ASSETS;?>js/data-tables/tables-data.js?ver=<?php echo VERSION;?>"></script>

<script src="<?php echo ADMIN_ASSETS;?>jquery_validation/js/jquery.validate.js?ver=<?php echo VERSION;?>"></script>
<link href="<?php echo ADMIN_ASSETS;?>jquery_validation/css/cmxform.css?ver=<?php echo VERSION;?>" rel="stylesheet">

<script>
$(document).ready(function() {
    $('#result-datatables').DataTable( {
		"sDom": '<"H"lfr>t<"F">',
		"columnDefs": [ {
			"targets": [4],  
			"orderable": false
		} ],
		language: {
			emptyTable: 'Record Not Found',
		  	zeroRecords: 'Record Not Found'
		},
		"pageLength": 100,
    } );
	
	$('select[name="result-datatables_length"]').change(function(){
		$('#searchForm').submit();
	});
	
	$('input[type="search"]').attr('name','search_keyword');
	
	$('input[name="search_keyword"]').keyup(function(e){
		if(e.keyCode==13){
			$('#searchForm').submit();
		}
	});
	
	$('select[name="result-datatables_length"]').val('<?php echo $search_per_page?>');
	$('input[name="search_keyword"]').val('<?php echo $search_keyword?>');
	
} );

function add_result()
{
	$('#in_id').val('');
	$('#st_name').val('');
	$('#st_description').val('');
	$('#in_qty_stock').val('');
	
	$('#st_name-error').html('');
	$('#st_name-error').css('display','none');
	
	$('#modal_title').html('Add');	
	
	$('#formModal').modal('show');	
}

function get_result(result_id)
{
	$('#in_id').val('');
	$('#st_name').val('');
	$('#st_description').val('');
	$('#in_qty_stock').val('');
	
  if(result_id!=""){	
  
	$('#in_id').val(result_id);	
	
	$.ajax({
	  type:'POST',
	  url : '<?php echo base_url();?>staff/items/get_result',
	  data: "result_id="+result_id,
	  success: function(data_result){
		
		data_result_array = JSON.parse(data_result);
		$('#st_name').val(data_result_array['result'][0].st_name);
		$('#st_description').val(data_result_array['result'][0].st_description);	
		$('#in_qty_stock').val(data_result_array['result'][0].in_qty_stock);			
		
		$('#modal_title').html('To Adjust');		
		
		$('#formModal').modal('show');	
	  }
	});
	 
  }  
  else
  {
	bootbox.alert('Record not valid', function() { });
  }
}

$("#formSubmit").validate({
	rules: {
		st_name: {
			required: true
		},
	},

	messages: {
		st_name: "This field should not empty",
	},
	
	 submitHandler: function (form) {
		//var formdata = $("#frmSubmit").serialize();
		$('.loader').show();
	
		var site_url = '<?php echo base_url();?>staff/items/action';
					
		$.ajax({
		  type:'POST',
		  url : site_url,
		  data: new FormData($('#formSubmit')[0]),
		  processData: false,
		  contentType: false,
		  success: function(result_data){
			data_result_array = JSON.parse(result_data);
			if(data_result_array.result==0)
			{
				var data_result_array_message = data_result_array.message;
				for(var i in data_result_array_message)
				{
					$('#'+i+'-error').html(data_result_array_message[i]);
					$('#'+i+'-error').show();
				}
				$('.loader').hide();
			}
			else
			{
				$('.loader').hide();
				$('#formModal').modal('hide');	
				bootbox.alert(data_result_array.message, function() { 
					window.location.href="<?php echo base_url()?>staff/items";
				});
			}
			
		  }
		});
	},
});

function stock_modal(item_id)
{
	$('#item_id').val(item_id);
	$('#frmModal').modal('show');
}

$("#frmSubmit").validate({
	rules: {
		qty: {
			required: true,
			number: true,
		},
	},

	messages: {
		qty: "Please enter Quantity in Number",		
	},
	
	 submitHandler: function (form) {
		//var formdata = $("#frmSubmit").serialize();
		$('.loader').show();
		
		var site_url = '<?php echo base_url();?>staff/items/add_qty';
		
			
		$.ajax({
		  type:'POST',
		  url : site_url,
		  data: new FormData($('#frmSubmit')[0]),
		  processData: false,
		  contentType: false,
		  success: function(result_data){
			data_result_array = JSON.parse(result_data);
			if(data_result_array.result==0)
			{
				var data_result_array_message = data_result_array.message;
				for(var i in data_result_array_message)
				{
					$('#'+i+'-error').html(data_result_array_message[i]);
					$('#'+i+'-error').show();
				}
				$('.loader').hide();
			}
			else
			{
				$('.loader').hide();
				
				$(".resultlogin").html("<div class='alert alert-success login wow fadeIn animated'>"+data_result_array.message+"</div>");
				window.location.href='';
				
			}
			
		  }
		});
	},
});
</script>

</body>
</html>
