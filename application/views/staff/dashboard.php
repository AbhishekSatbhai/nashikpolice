
<?php $this->load->view('staff/includes/header');?> 
<style>
	.card-body > a > img {height: 194px;}
	
	.tile_count {
		margin-bottom: 20px;
		margin-top: 20px;
		
		padding: 10px 20px 0;
	}
	
	@media (min-width: 992px)
.tile_count .tile_stats_count {
    margin-bottom: 10px;
    border-bottom: 0;
    padding-bottom: 10px;
}

.tile_count .tile_stats_count {
   
    padding: 0 10px 0 20px;
    position: relative;
}

.tile_count .tile_stats_count, ul.quick-list li {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}

@media (min-width: 768px){
.tile_count .tile_stats_count span {
    font-size: 13px;
}

.tile_count .tile_stats_count .count {
    font-size: 40px;
}
.tile_count{
    margin-left: -45px;
    margin-right: -45px;
}
}


.tile_count .tile_stats_count .count {
    
    line-height: 47px;
    font-weight: 600;
}


.green {
    color: #B18F6B;
}

.tile_count .tile_stats_count:before {
    content: "";
    position: absolute;
    left: 0;
    height: 65px;
    border-left: 2px solid #ADB2B5;
    margin-top: 10px;
}

.tile_count .tile_stats_count:first-child:before {
    border-left: 0;
}
	
	</style>
<section class="page">
	<?php $this->load->view('staff/includes/sidebar');?> 
	
	<div id="wrapper">
		<div class="content-wrapper container">
			<div class="row">
				<div class="col-sm-6">
					<div class="page-title">
						<h1>Dashboard <small></small></h1>
						<ol class="breadcrumb">
							<li><a href="#" title="Home"><i class="fa fa-home"></i></a></li>
							<li class="active">Dashboard</li>
						</ol>
					</div>
				</div>
				
				<div class="col-sm-6 text-right">
					<h3>Today's <?php echo date('d F Y')?></h3>
				</div>
				
			</div><!-- end .page title-->
			
				<div class="row tile_count">
				<?php /*<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<span class="count_top"><i class="fa fa-user"></i> Total Food Request</span>
				<div class="count green"><?php echo $total_food_request;?></div>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<span class="count_top"><i class="fa fa-user"></i> Total Delivery Request </span>
				<div class="count green"><?php echo $total_delivery_request;?></div>
				</div>
				<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<span class="count_top"><i class="fa fa-user"></i> Total Items </span>
				<div class="count green"><?php echo $total_items;?></div>
				</div>*/?>
				<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<span class="count_top"><i class="fa fa-clock-o"></i> Total Provider Request</span>
				<div class="count green"><?php echo $total_supplier_request;?></div>
				</div>

				<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
				<span class="count_top"><i class="fa fa-clock-o"></i> Total Completed Request</span>
				<div class="count green"><?php echo $total_completed_request;?></div>
				</div>
				
				</div>	
			
			<div class="row">
				
				<div class="col-sm-6 col-md-3 margin-b-30" <?php echo privilege('flg_new_customer');?>>
					<a href="<?php echo base_url()?>staff/supplier_request">
					<div class="tile">
						<div class="tile-body clearfix" style="background-color:#160053 !important;">
							<i class="fa fa-money"></i>
							<h4 class="pull-right" style="font-size:14px;">Provider Request</h4>
						</div><!--.tile-body-->
					</div><!-- .tile-->
					</a>
				</div><!--end .col-->

				<div class="col-sm-6 col-md-3 margin-b-30" <?php echo privilege('flg_new_customer');?>>
					<a href="<?php echo base_url()?>staff/completed_request">
					<div class="tile">
						<div class="tile-body clearfix" style="background-color:#160053 !important;">
							<i class="fa fa-money"></i>
							<h4 class="pull-right" style="font-size:14px;">Completed Request</h4>
						</div><!--.tile-body-->
					</div><!-- .tile-->
					</a>
				</div><!--end .col-->
				
			</div><!--end .row-->
			
				
			<div class="request_form" style="background-color:#160053; color:white;">
				<nav class="navbar navbar-default navbar-static-top">
            <div class="">
                <div class="navbar-header">
				<a href="" class="navbar-brand"  style="background-color:#160053; color:white;">Required Quantity</a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span href="" style="color:gray;">Click here</span>
                    </button>
                </div>

                <div id="navbar1" class="navbar-collapse collapse" style="background-color:#160053; color:white;">
				<table class="table table-bordered">
						<tr>
							<th>
								No
							</th>
							<th>
								Item
							</th>
							<th>
								Required Quantity
							</th>
							<th>
								Received Quantity
							</th>
						</tr>
						<tbody>
							<?php if(!empty($result)) {
							// $i=($page==0) ? 1 : $page + 1;	
							$i=0;
							foreach($result as $row) {
							?>	
								<tr>
									<td><?php echo $i;?></td>
									<td><?php echo $row->st_name;?></td>
									
									<td><?php echo $row->in_qty_stock;?></td>	
									<td><?php echo $row->received_qty;?></td>	
								
								</tr>
							<?php $i++; } }?>
							</tbody>
					</table>
								

                </div><!--/.nav-collapse -->
				</nav>
				</div>
			<div class="row"> <div class="">
	<div class="container">
	
			<div class="d-none">
				<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
			</div>
			<h2 class="text-primary text-center">Other Items</h2>
			<table class="table" style="color:black;">
							<tr>
								<th>
									No
								</th>
								<th>
									Other item Name
								</th>
								<th>
									Quantity
								</th>
							</tr>

							<?php
								$this->load->model('Get_item');
								$name = $this->get_item->get_other_name();
								// echo "<pre>";
								// print_r($name);
							?>

<?php if(!empty($result)) {
						// $i=($page==0) ? 1 : $page + 1;	
						$i=1;
						foreach($name as $row) {
							if($row->other_qty){
						?>	
							<tr>
								<td><?php echo $i;?></td>
								<td><?php echo $row->other;?></td>
								
								<td><?php echo $row->other_qty;?></td>	
							
							</tr>
						<?php $i++; } } }?>
			</table>
			</div>	</div>	</div>

					
	</div>	

</section>

<?php $this->load->view('admin/includes/footer');?> 



 <!-- Flot -->
	<script src="<?php echo ADMIN_ASSETS;?>js/flot/jquery.flot.js?ver=<?php echo VERSION;?>"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/flot/jquery.flot.tooltip.min.js?ver=<?php echo VERSION;?>"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/flot/jquery.flot.resize.js?ver=<?php echo VERSION;?>"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/flot/jquery.flot.pie.js?ver=<?php echo VERSION;?>"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/chartjs/Chart.min.js?ver=<?php echo VERSION;?>"></script>
	
	<script src="<?php echo ADMIN_ASSETS;?>js/morris_chart/raphael-2.1.0.min.js?ver=<?php echo VERSION;?>"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/morris_chart/morris.js?ver=<?php echo VERSION;?>"></script>

	
	<!--        <script src="js/jquery.nanoscroller.min.js"></script>-->
	
	
	<script src="<?php echo ADMIN_ASSETS;?>js/bootstrap-editable.min.js"></script>
	<script src="<?php echo ADMIN_ASSETS;?>js/custom-editable.js"></script>

</body>
</html>