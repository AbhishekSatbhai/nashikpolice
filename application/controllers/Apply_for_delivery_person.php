<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apply_for_delivery_person extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
	}
	
	public function index()
	{	
		$data = array();
		$data['police_stations'] = get_police_station();
		$this->load->view('apply_for_delivery_person',$data);
	}
	
	public function action()
	{
		$data = array();
		if(!empty($this->input->post()))
		{
			$post_data =  $this->input->post();	
			
			$this->form_validation->set_rules('st_name','title','required', 'Please enter Name');
			$this->form_validation->set_rules('st_mobile_number','title','required', 'Please enter Contact Number');
			$this->form_validation->set_rules('st_address','title','required', 'Please enter Address');
			$this->form_validation->set_rules('st_vehicle','title','required', 'Please enter your Vehicle');
			$this->form_validation->set_rules('st_vehicle_number','title','required', 'Please enter your Vehicle Number');
			$this->form_validation->set_rules('st_nearest_police_station','title','required', 'Please enter your Nearest Police Station');
			$this->form_validation->set_rules('st_aadhar_number','title','required', 'Please enter your aadhar number');
			$this->form_validation->set_rules('st_aadhar_image','title','required', 'Please upload your Aadhar Photo');
			$this->form_validation->set_rules('st_driving_image','title','required', 'Please upload your Driving License');
			
			
			if($_SERVER['HTTP_HOST']!="localhost"){
				//verify captcha
				if(!empty($_POST['g-recaptcha-response']))
				{
					$recaptcha_secret = GOOGLE_CAPTCHA_SECRETE_KEY;
					
					$captcha_data = array(
					'secret' => $recaptcha_secret,
					'response' => $_POST['g-recaptcha-response'],
					);

					$verify = curl_init();
					curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
					curl_setopt($verify, CURLOPT_POST, true);
					curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($captcha_data));
					curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);

					$output = curl_exec($verify);
					curl_close($verify);

					$response = json_decode($output);

					$captcha_response = true;
					
					if (isset($response->success) && $response->success){ $captcha_response = true; }
					else{
						$captcha_response = false;
					}
				}
				else
				{
					$captcha_response = false;
				}
			}
			else $captcha_response = true;
			
			if($captcha_response == false)
			{
				$data['result'] = 0;
				$data['robot_message'] = "You are a robot";
			}
			
			if($this->form_validation->run()==true && $captcha_response)
			{
				$insert_data['st_name'] = $post_data['st_name'];
				$insert_data['st_mobile_number'] = $post_data['st_mobile_number'];
				$insert_data['st_address'] = $post_data['st_address'];
				$insert_data['st_vehicle'] = $post_data['st_vehicle'];
				$insert_data['st_vehicle_number'] = $post_data['st_vehicle_number'];
				$insert_data['st_nearest_police_station'] = $post_data['st_nearest_police_station'];
				$insert_data['st_aadhar_number'] = $post_data['st_aadhar_number'];
				$insert_data['dt_added_date'] = date('Y-m-d H:i:s');
				$insert_data['in_status'] = 1;
				
				if(!empty($_FILES['st_aadhar_image']['name']))
				{
					$explode = explode('.',$_FILES['st_aadhar_image']['name']);
					$ext = end($explode);
					$slak = random_string('numeric', 6);
					$file_name = $slak."_".time().".".$ext;
					$insert_data['st_aadhar_image'] = $file_name;
					
					// UPLOAD Main Image
					$config['file_name'] = $file_name;
					$config['upload_path'] = getcwd()."/media/delivery_images/";
					$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
					
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					$this->upload->do_upload('st_aadhar_image');
				}
				
				if(!empty($_FILES['st_driving_image']['name']))
				{
					$explode = explode('.',$_FILES['st_driving_image']['name']);
					$ext = end($explode);
					$slak = random_string('numeric', 6);
					$file_name = $slak."_".time().".".$ext;
					$insert_data['st_driving_image'] = $file_name;
					
					// UPLOAD Main Image
					$config['file_name'] = $file_name;
					$config['upload_path'] = getcwd()."/media/delivery_images/";
					$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
					
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					$this->upload->do_upload('st_driving_image');
				}
				
				$this->common_model->insert_record("tbl_delivery_person", $insert_data);
				
				$this->session->set_flashdata(array('success_message'=>'Your Request Submitted Successfully.'));
			
				$data['result'] = 1;
				$data['message'] = 'Your request has been submitted. Thank you!';
			}
			else
			{
				$data['result'] = 0;
				$data['message'] = validation_errors();
			}
		}
		
		echo json_encode($data);
	}

}
?>