<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		//$this->load->model('system_user_model');
	}
	function index()
	{
		$update_data['flg_logged'] = 0;
		
		$this->common_model->edit_record("tbl_system_user", $update_data,"in_user_id = '".$this->session->userdata('user_id')."'");
		
		$this->session->unset_userdata('admin_logged');	
		
		get_db_backup();
		
		if(!empty($_GET['url_redirect']))
		{	
			$this->session->set_userdata(array('url_redirect' => $_GET['url_redirect']));
		}
		
		redirect('staff');
	}
}

