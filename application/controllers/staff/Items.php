<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Items extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('item_model');
		
		is_admin_logged_in();
	}
	
	public function index($page = 1)
	{	
		$data = array();
		$conditions = array();
		
		$suffix = '';
		
		$search_per_page = '100';
		$search_keyword = '';
		
		if(!empty($_GET))
		{
			$suffix = '?'.http_build_query($_GET);	
			if(!empty($_GET['result-datatables_length'])) $search_per_page = $this->input->get('result-datatables_length');
			if(!empty($_GET['search_keyword'])) $search_keyword = $this->input->get('search_keyword');
		}
		
		$data['search_per_page'] = $search_per_page;
		$data['search_keyword'] = $search_keyword;
		
		$per_page = $search_per_page; //10
		
		$limit_cond = $per_page;
		
		if($page>1)
		{
			$page = $page - 1;
			if($page==0) $page = 1;
			$page = $per_page * $page;
		}
		else
		$page = 0;	
	
		$data['page'] = $page;
		
		$per_page_cond = $page;
		
		$conditions['search_keyword'] = $search_keyword;
		
		$total_record = $this->item_model->get_result($conditions,'','', true );
		$data['result'] = $this->item_model->get_result($conditions,$per_page_cond,$limit_cond);
		
		$pagination_base_url = base_url().'items/index';
		$pagination_total_rows = $total_record;
		$pagination_per_page = $per_page;
		$pagination_suffix = $suffix;
		$pagination_uri_segment  = 3;
		$pagination_page  = $page;
		$pagination_showing_total_record  = count($data['result']);
		
		$data['pagination'] = set_pagination_helper($pagination_base_url, $pagination_total_rows, $pagination_per_page, $pagination_page, $pagination_suffix, $pagination_uri_segment, $pagination_showing_total_record);
		
		$this->load->view('staff/items',$data);
	}
	
	public function action()
	{
		
		if(!empty($this->input->post()))
		{
			//$post_data =  $this->security->xss_clean($_POST);
			$post_data =  $this->input->post();

			$this->form_validation->set_rules('st_name','title','required', "This field should not empty");
			
			if($this->form_validation->run()==true)
			{
				$insert_data['st_name']				= $post_data['st_name'];
				$insert_data['st_description']		= $post_data['st_description'];
				$insert_data['in_qty_stock']		= $post_data['in_qty_stock'];
				$insert_data['flg_is_delete']		= 0;
				
				if(!empty($this->input->post('in_id')))
				{
					$result_id = $this->input->post('in_id');
					$this->common_model->edit_record('tbl_items', $insert_data, 'in_id = '.$result_id);
				}
				else
				{
					$result_id = $this->common_model->insert_record('tbl_items',$insert_data);
				}

				$array = array('result'=>'1', 'message'=>"Success");
				echo json_encode($array);
			}
			else
			{
				$array = array('result'=>'0', 'message'=>$this->form_validation->error_array());
				echo json_encode($array);
			}
		}
		else
		{
			$array = array('result'=>'0', 'message'=>"Form empty");
			echo json_encode($array);
		}
	}
	
	public function delete($id)
	{
	
		if(empty($id))
			redirect('admin/items');
		
		$where_cond = "in_id = '".$id."' AND flg_is_delete = 0";
		$select_cond = "*";
		$result = $this->common_model->select_records('tbl_items',$where_cond,$select_cond,'','','','',true);
		
		if(empty($result))
			redirect('admin/items');
		
		$update_data['flg_is_delete'] 	 = '1';
		
		$this->common_model->edit_record('tbl_items', $update_data, "in_id = '".$id."'");
		
		$this->session->set_flashdata('success_message',"Deleted Success");
		redirect('admin/items');
	}
	
	public function delete_all()
	{
		if(!empty($_POST['result_id']))
		{
			$results= explode(',',$_POST['result_id']);	
			for($i=0;$i<count($results); $i++)
			{
				$result_id = $results[$i];
				
				$where_cond = "in_id = '".$id."' AND flg_is_delete = 0";
				$select_cond = "*";
				
				$result = $this->common_model->select_records('tbl_items',$where_cond,$select_cond,'','','','',true);
				
				if(!empty($result))
				{
					$update_data['flg_is_delete'] 	 = '1';
					$this->common_model->edit_record('tbl_items', $update_data, "in_id = '".$result_id."'");
				}
			}
			
			$array = array('result'=>'1', 'message'=>"Deleted Success");
			echo json_encode($array);
		}
		else
		{
			$array = array('result'=>'0', 'message'=>"Choose Record");
			echo json_encode($array);
		}
	}
	
	function get_result()
	{
		$result_id = $_POST['result_id'];
		$conditions['item_id'] = $result_id;
		$results['result'] = $this->item_model->get_result($conditions);
		echo json_encode($results);		
	}
	
	function get_item()
	{
		$get_value = strtolower(urldecode($_GET['q']));
		
		$where_cond = "flg_is_delete = 0 AND (st_name LIKE '%".$get_value."%')";
		$select_cond = "st_name, st_description, in_qty_stock";
		
		$array = array();
		
		$results = $this->common_model->select_records("tbl_items",$where_cond,$select_cond,'','','','');
		if(!empty($results))
		{
			foreach($results as $row)
			{
				$array[] = htmlentities($row->st_name)."|".$row->in_id."|".$row->st_description."|".$row->in_qty_stock;
			}
		}
		
		if(empty($array)) $array[] = $get_value."|0";
	   	echo json_encode($array);
	}

	public function add_qty()
	{
		$data = array();
		if(!empty($this->input->post()))
		{
			$post_data = $this->input->post();	
			
			$this->form_validation->set_rules('item_id','item id','required', 'Item id not exists');
			$this->form_validation->set_rules('qty','qty','required', 'Please enter Quantity');
			
			if($this->form_validation->run()==true)
			{
				$item_id = $this->input->post('item_id');
				$qty = $this->input->post('qty');
				
				$where_cond = "in_id = '".$item_id."'";
				$select_cond = "in_qty_stock";
				$limit_cond = '1';
				$perpage_cond = '0';
				$item_details = $this->common_model->select_records("tbl_items",$where_cond,$select_cond,'','',$perpage_cond,$limit_cond);
				
				$update_data['in_qty_stock'] = $qty + $item_details[0]->in_qty_stock;
				$this->common_model->edit_record("tbl_items",$update_data, "in_id = '".$item_id."'", 'tbl_items');
				
				
				$data['result'] = 1;
				$data['message'] = 'Item stock get added';
			}
			else
			{
				$data['result'] = 0;
				$data['message'] = $this->form_validation->error_array();
			}
		}
		
		echo json_encode($data);
	}	
}

