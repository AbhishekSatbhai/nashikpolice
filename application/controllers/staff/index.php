<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('login_model');
		$this->load->model('system_user_model');
		
		
	}
	
	public function index()
	{	
		$data = array();
		$this->load->view('staff/login',$data);
	}
	
	public function login()
	{
		$data = array();
		if(!empty($_POST))
		{
			$post_data =  $this->security->xss_clean($_POST);	
			
			$this->form_validation->set_rules('username','username','required', 'Please enter Username');
			$this->form_validation->set_rules('password','password','required', 'Please enter Password');
			$this->form_validation->set_rules('valid_user','valid_user','callback_valid_user');
			
			if($this->form_validation->run()==true)
			{
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				
				$where_cond = "st_username = '".$username."' AND st_password = '".$password."'";
				$select_cond = "in_user_id, st_name, st_image, in_role_id, in_master_user";
				$limit_cond = '1';
				$perpage_cond = '0';
				$user_details = $this->login_model->select_records($where_cond,$select_cond,'','',$perpage_cond,$limit_cond);
				
				$update_data['dt_last_logged'] = date('Y-m-d H:i:s');
				$update_data['flg_logged'] = 1;
				
				$this->system_user_model->edit_record($update_data,"in_user_id = '".$user_details[0]->in_user_id."'");
			
				$this->session->set_userdata(array('admin_logged'=>true,'admin_name'=>$user_details[0]->st_name, 'user_id'=>$user_details[0]->in_user_id, 'avatar'=>$user_details[0]->st_image, 'role_id'=>$user_details[0]->in_role_id, 'master_user'=>$user_details[0]->in_master_user));
				
				$data['status'] = 1;
				$data['msg'] = 'You have successfuly logged in!';
				
				//get_db_backup();
				
			}
			else
			{
				$data['status'] = 0;
				$data['msg'] = validation_errors();
			}
		}
		
		echo json_encode($data);
	}
	
	function valid_user()
	{	
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		if(!empty($username) && !empty($username))
		{
			$where_cond = "st_username = '".$username."' AND st_password = '".$password."'";
			$select_cond = "in_user_id, flg_is_active, flg_is_delete";
			$limit_cond = '1';
			$perpage_cond = '0';
			$data['user_details'] = $this->login_model->select_records($where_cond,$select_cond,'','',$perpage_cond,$limit_cond);
			
			if(!empty($data['user_details']) && ($data['user_details'][0]->flg_is_active==0 || $data['user_details'][0]->flg_is_delete == 1))
			{
				$this->form_validation->set_message('valid_user', 'Your account has been deactivated, please contact to admin.');
				return FALSE;
			}
			if(empty($data['user_details']))
			{
				$this->form_validation->set_message('valid_user', 'Invalid username and password.');
				return FALSE;
			}
			else 
			{
				return true;
			}
		}
	}
}

