<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Completed_request extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('supplier_request_model');
		is_admin_logged_in();
	}
	
	public function index($page = 1)
		{
		$data = array();
		$conditions = array();
		
		$suffix = '';
		
		$search_per_page = '100';
		$search_keyword = '';
		
		if(!empty($_GET))
		{
			$suffix = '?'.http_build_query($_GET);
			if(!empty($_GET['result-datatables_length'])) $search_per_page = $this->input->get('result-datatables_length');
			if(!empty($_GET['search_keyword'])) $search_keyword = $this->input->get('search_keyword');
			
		}
		
		$data['search_per_page'] = $search_per_page;
		$data['search_keyword'] = $search_keyword;
		
		$per_page = $search_per_page; //10
		
		$limit_cond = $per_page;
		
		if($page>1)
		{
			$page = $page - 1;
			if($page==0) $page = 1;
			$page = $per_page * $page;
		}
		else
			$page = 0;
	
		$data['page'] = $page;
		
		$per_page_cond = $page;
		
		$conditions['search_keyword'] = $search_keyword;
		$conditions['sort_by'] = "tbl1.dt_added_date";
		$conditions['sort_type'] = "DESC";
		
		$total_record = $this->supplier_request_model->get_completed($conditions,'','', true );
		$data['result'] = $this->supplier_request_model->get_completed($conditions,$per_page_cond,$limit_cond);
		
		$pagination_base_url = base_url().'staff/completed_request/index';
		$pagination_total_rows = $total_record;
		$pagination_per_page = $per_page;
		$pagination_suffix = $suffix;
		$pagination_uri_segment  = 3;
		$pagination_page  = $page;
		$pagination_showing_total_record  = count($data['result']);
		
		$data['pagination'] = set_pagination_helper($pagination_base_url, $pagination_total_rows, $pagination_per_page, $pagination_page, $pagination_suffix, $pagination_uri_segment, $pagination_showing_total_record);
		
		$data['get_help_type'] = get_help_type();
		$data['get_tehsil'] = get_tehsil();
		
		$this->load->view('staff/completed_request',$data);
	}
	
	public function download_excel(){
	

$conn = new mysqli('localhost', 'root', '');  
mysqli_select_db($conn, 'food');

$sql = "SELECT * FROM `tbl_supplier_request` WHERE in_status=2";  
$setRec = mysqli_query($conn, $sql);  
$columnHeader = '';  
$columnHeader =  "Request Date" . "\t" . "Name" . "\t" . "Mobile no" . "\t" . "Address" . "\t" . "Email" . "\t" . "Pin Code" . "\t" . "Item 1" . "\t" . "Item 1 quantity" . "\t" . "Item 2" . "\t" . "Item 2 quantity" . "\t" . "Item 3" . "\t" . "Item 3 quantity" . "\t" . "Item 4" . "\t" . "Item 4 quantity" . "\t" . "Item 5" . "\t" . "Item 5 quantity" . "\t" . "Item 6" . "\t" . "Item 6 quantity" . "\t" . "Item 7" . "\t" . "Item 7 quantity" . "\t" . "Item 8" . "\t" . "Item 8 quantity" . "\t" . "Other Item" . "\t" . "Other Quantity" . "\t" . "Collection Date" . "\t" . "Remark" . "\t" . "Nearest Police Station" . "\t";  
$setData = '';  
  while ($a=mysqli_fetch_assoc($setRec)) {  
    $rowData = '';  
    $value = $a['dt_added_date'] . "\t" . $a['st_name'] . "\t" . $a['st_telephone'] . "\t" . $a['st_address'] . "\t" . $a['st_email'] . "\t" . $a['st_answer'] . "\t". $a['food1'] . "\t". $a['food1_qty'] . "\t". $a['food2'] . "\t". $a['food1_qty'] . "\t". $a['grocery1'] . "\t". $a['grocery1_qty'] . "\t". $a['grocery2'] . "\t". $a['grocery2_qty'] . "\t". $a['medical1'] . "\t". $a['medical1_qty'] . "\t". $a['medical2'] . "\t". $a['medical2_qty'] . "\t". $a['dry_fruits1'] . "\t". $a['dry_fruits1_qty'] . "\t". $a['dry_fruits2'] . "\t". $a['dry_fruits2_qty'] . "\t". $a['other'] . "\t". $a['other_qty'] . "\t" . $a['st_help_date'] . "\t" . $a['st_remark'] . "\t" . $a['st_nearest_police_station'];
    $rowData .= $value;  
    $setData .= trim($rowData) . "\n";  
}  

echo ucwords($columnHeader) . "\n" . $setData . "\n";  
 header("Content-type: application/octet-stream");  
 header("Content-Disposition: attachment; filename=User_Detail.xls");  
 header("Pragma: no-cache");  
 header("Expires: 0");  


	}
	public function delete($id)
	{
	
		if(empty($id))
			redirect('staff/completed_request');
		
		$where_cond = "in_id = '".$id."' AND flg_is_delete = 0";
		$select_cond = "*";
		$result = $this->common_model->select_records('tbl_supplier_request',$where_cond,$select_cond,'','','','',true);
		
		if(empty($result))
			redirect('staff/completed_request');
		
		$update_data['flg_is_delete'] 	 = '1';
		
		$this->common_model->edit_record('tbl_supplier_request', $update_data, "in_id = '".$id."'");
		
		$this->session->set_flashdata('success_message',"Deleted Success");
		redirect('staff/completed_request');
	}
	
	public function delete_all()
	{
		if(!empty($_POST['result_id']))
		{
			$results= explode(',',$_POST['result_id']);	
			for($i=0;$i<count($results); $i++)
			{
				$result_id = $results[$i];
				
				$where_cond = "in_id = '".$id."' AND flg_is_delete = 0";
				$select_cond = "*";
				
				$result = $this->common_model->select_records('tbl_supplier_request',$where_cond,$select_cond,'','','','',true);
				
				if(!empty($result))
				{
					$update_data['flg_is_delete'] 	 = '1';
					$this->common_model->edit_record('tbl_supplier_request', $update_data, "in_id = '".$result_id."'");
				}
			}
			
			$array = array('result'=>'1', 'message'=>"Deleted Success");
			echo json_encode($array);
		}
		else
		{
			$array = array('result'=>'0', 'message'=>"Choose Record");
			echo json_encode($array);
		}
	}

	public function change_status()
	{
		if(!empty($this->input->post())){
		
			$result_id = $this->input->post('result_id');
			$status_id = $this->input->post('status_id');
			
			$update_data['in_status'] = $status_id;
			$this->common_model->edit_record('tbl_supplier_request', $update_data, "in_id = '".$result_id."'");
			
			$array = array('result'=>'1', 'message'=>"Status Changed!");
			echo json_encode($array);
		}
		else
		{
			$array = array('result'=>'0', 'message'=>"Choose Record");
			echo json_encode($array);
		}
	}

	public function get_items()
	{
		if(!empty($this->input->post())){
		    $output = '';
			$result_id = $this->input->post('result_id');
			$result = $this->supplier_request_model->get_items($result_id);
			$output.='
    <div class = "table-responsive">
        <table class = table table-bordered">';
   if(count($result)){
		foreach($result as $row){
   
        $output.='
            <tr>
                <th width = "50%"> Item </th>
                <th width = "50%"> Quantity </th>
            </tr>
            <tr>
                <td width = "50%">'.$row->food1.'</th>
                <td width = "50%">'. $row->food1_qty .'</th>
            </tr>
            <tr>
                <td width = "50%">'.$row->food2.' </th>
                <td width = "50%">'. $row->food2_qty .'</th>
            </tr>
            <tr>
                <td width = "50%">'.$row->grocery1.' </th>
                <td width = "50%">'. $row->grocery1_qty .'</th>
            </tr>
            <tr>
                <td width = "50%"> '.$row->grocery2.' </th>
                <td width = "50%">'. $row->grocery2_qty .'</th>
            </tr>
            <tr>
                <td width = "50%"> '.$row->medical1.'</th>
                <td width = "50%">'. $row->medical1_qty.'</th>
            </tr>
            <tr>
                <td width = "50%">'.$row->medical2.'</th>
                <td width = "50%">'. $row->medical2_qty.'</th>
            </tr>
            <tr>
                <td width = "50%">'.$row->dry_fruits1.'</th>
                <td width = "50%">'. $row->dry_fruits1_qty.'</th>
            </tr>
            <tr>
                <td width = "50%"> '.$row->dry_fruits2.'</th>
                <td width = "50%">'. $row->dry_fruits2_qty.'</th>
            </tr>
            <tr>
                <td width = "50%"> '.$row->other.' </th>
                <td width = "50%">'. $row->other_qty .'</th>
            </tr>
            
		'; 
		}
	}
	$output .= "</table></div>";
			echo $output;
		}
	}
}