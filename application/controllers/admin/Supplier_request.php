<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Supplier_request extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('supplier_request_model');
		is_admin_logged_in();
	}
	
	public function index($page = 1)
		{
		$data = array();
		$conditions = array();
		
		$suffix = '';
		
		$search_per_page = '100';
		$search_keyword = '';
		
		if(!empty($_GET))
		{
			$suffix = '?'.http_build_query($_GET);
			if(!empty($_GET['result-datatables_length'])) $search_per_page = $this->input->get('result-datatables_length');
			if(!empty($_GET['search_keyword'])) $search_keyword = $this->input->get('search_keyword');
			
		}
		
		$data['search_per_page'] = $search_per_page;
		$data['search_keyword'] = $search_keyword;
		
		$per_page = $search_per_page; //10
		
		$limit_cond = $per_page;
		
		if($page>1)
		{
			$page = $page - 1;
			if($page==0) $page = 1;
			$page = $per_page * $page;
		}
		else
			$page = 0;
	
		$data['page'] = $page;
		
		$per_page_cond = $page;
		
		$conditions['search_keyword'] = $search_keyword;
		$conditions['sort_by'] = "tbl1.dt_added_date";
		$conditions['sort_type'] = "DESC";
		
		$total_record = $this->supplier_request_model->get_result($conditions,'','', true );
		$data['result'] = $this->supplier_request_model->get_result($conditions,$per_page_cond,$limit_cond);
		
		$pagination_base_url = base_url().'admin/supplier_request/index';
		$pagination_total_rows = $total_record;
		$pagination_per_page = $per_page;
		$pagination_suffix = $suffix;
		$pagination_uri_segment  = 3;
		$pagination_page  = $page;
		$pagination_showing_total_record  = count($data['result']);
		
		$data['pagination'] = set_pagination_helper($pagination_base_url, $pagination_total_rows, $pagination_per_page, $pagination_page, $pagination_suffix, $pagination_uri_segment, $pagination_showing_total_record);
		
		$data['get_help_type'] = get_help_type();
		$data['get_tehsil'] = get_tehsil();
		
		$this->load->view('admin/supplier_request',$data);
	}
	
	public function delete($id)
	{
	
		if(empty($id))
			redirect('admin/supplier_request');
		
		$where_cond = "in_id = '".$id."' AND flg_is_delete = 0";
		$select_cond = "*";
		$result = $this->common_model->select_records('tbl_supplier_request',$where_cond,$select_cond,'','','','',true);
		
		if(empty($result))
			redirect('admin/supplier_request');
		
		$update_data['flg_is_delete'] 	 = '1';
		
		$this->common_model->edit_record('tbl_supplier_request', $update_data, "in_id = '".$id."'");
		
		$this->session->set_flashdata('success_message',"Deleted Success");
		redirect('admin/supplier_request');
	}
	
	public function delete_all()
	{
		if(!empty($_POST['result_id']))
		{
			$results= explode(',',$_POST['result_id']);	
			for($i=0;$i<count($results); $i++)
			{
				$result_id = $results[$i];
				
				$where_cond = "in_id = '".$id."' AND flg_is_delete = 0";
				$select_cond = "*";
				
				$result = $this->common_model->select_records('tbl_supplier_request',$where_cond,$select_cond,'','','','',true);
				
				if(!empty($result))
				{
					$update_data['flg_is_delete'] 	 = '1';
					$this->common_model->edit_record('tbl_supplier_request', $update_data, "in_id = '".$result_id."'");
				}
			}
			
			$array = array('result'=>'1', 'message'=>"Deleted Success");
			echo json_encode($array);
		}
		else
		{
			$array = array('result'=>'0', 'message'=>"Choose Record");
			echo json_encode($array);
		}
	}

	public function change_status()
	{
		if(!empty($this->input->post())){
		
			$result_id = $this->input->post('result_id');
			$status_id = $this->input->post('status_id');
			$name = $this->input->post('name');
			$email = $this->input->post('email');

				$emailData = "<center>
				<div style='width:800px; height:600px; padding:20px; text-align:center; border: 10px solid #160054'>
				<div style='width:750px; height:550px; padding:20px; text-align:center; border: 5px solid #160054'>
				<span style='font-size:50px; font-weight:bold'>Certificate of Appreciation<br></span>
				<span style='font-size:30px; font-weight:bold'>For Donation</span>
				<br><br>
				<span style='font-size:20px'><i>Presented to</i></span>
				<br><br>
				<span style='font-size:30px'><b>";
				$emailData .= $name;         
				$emailData .= "</b></span><br/><br/>
				<span style='font-size:25px'><i>For your generous donation to the</i></span> <br/>
				<span style='font-size:25px'><i>Nashik City Police</i></span><br><br>
				<span style='font-size:25px'><b><i>";
				$emailData .=  date("d-m-Y");    
				$emailData .="</i><b></span><br>
				<table style='margin-top:40px;float:left'>
				<tr>
				<td><br><br><br><img width='200px' height='50px' src='<?php echo FRONT_ASSETS; ?>images/info3.jpeg'></td>
				</tr>
				<tr>
				<td style='width:200px;float:left;border:0;border-bottom:1px solid #000;'></td>
				</tr>
				<tr>
				<td style='text-align:center'><span><b>Signature</b></td>
				</tr>
				</table>
				<br>
				<img width='200px' height='50px' src='http://covid19help.nashikcitypolice.gov.in/assets/front/images/Nashik-Police-logo.jpg'></td>  
				<table style='margin-top:40px;float:right'>
				<tr>
				<td><br><br><img width='200px' height='50px' src='http://covid19help.nashikcitypolice.gov.in/assets/front/images/Nashik-Police-logo.jpg'></td>
				</tr>
				<tr>
				<td style='width:200px;float:right;border:0;border-bottom:1px solid #000;'></td>
				</tr>
				<tr>
				<td style='text-align:center'><span><b>Signature</b></td>
				</tr>
				</table>
				</div>
				</div>
				</center>";

			$email_data['to'] = $email;
			$email_data['subject'] = 'Nashik City Police';
			$email_data['message'] = $emailData;
			send_email($email_data);

			$update_data['in_status'] = $status_id;
			$this->common_model->edit_record('tbl_supplier_request', $update_data, "in_id = '".$result_id."'");
			
			
		}
		else
		{
			$array = array('result'=>'0', 'message'=>"Choose Record");
			echo json_encode($array);
		}
	}

	public function get_items()
	{
		if(!empty($this->input->post())){
		    $output = '';
			$result_id = $this->input->post('result_id');
			$result = $this->supplier_request_model->get_items($result_id);
			$output.='
    <div class = "table-responsive">
        <table class = table table-bordered">';
   if(count($result)){
		foreach($result as $row){
   
        $output.='
            <tr>
                <th width = "50%"> Item </th>
                <th width = "50%"> Quantity </th>
            </tr>
            <tr>
                <td width = "50%">'.$row->food1.'</th>
                <td width = "50%">'. $row->food1_qty .'</th>
            </tr>
            <tr>
                <td width = "50%">'.$row->food2.' </th>
                <td width = "50%">'. $row->food2_qty .'</th>
            </tr>
            <tr>
                <td width = "50%">'.$row->grocery1.' </th>
                <td width = "50%">'. $row->grocery1_qty .'</th>
            </tr>
            <tr>
                <td width = "50%"> '.$row->grocery2.' </th>
                <td width = "50%">'. $row->grocery2_qty .'</th>
            </tr>
            <tr>
                <td width = "50%"> '.$row->medical1.'</th>
                <td width = "50%">'. $row->medical1_qty.'</th>
            </tr>
            <tr>
                <td width = "50%">'.$row->medical2.'</th>
                <td width = "50%">'. $row->medical2_qty.'</th>
            </tr>
            <tr>
                <td width = "50%">'.$row->dry_fruits1.'</th>
                <td width = "50%">'. $row->dry_fruits1_qty.'</th>
            </tr>
            <tr>
                <td width = "50%"> '.$row->dry_fruits2.'</th>
                <td width = "50%">'. $row->dry_fruits2_qty.'</th>
            </tr>
            <tr>
                <td width = "50%"> '.$row->other.' </th>
                <td width = "50%">'. $row->other_qty .'</th>
            </tr>
            
		'; 
		}
	}
	$output .= "</table></div>";
			echo $output;
		}
	}


}