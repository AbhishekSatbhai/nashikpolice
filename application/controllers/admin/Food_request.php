<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Food_request extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('food_request_model');
		is_admin_logged_in();
	}
	
	public function index($page = 1)
		{
		$data = array();
		$conditions = array();
		
		$suffix = '';
		
		$search_per_page = '100';
		$search_keyword = '';
		
		if(!empty($_GET))
		{
			$suffix = '?'.http_build_query($_GET);
			if(!empty($_GET['result-datatables_length'])) $search_per_page = $this->input->get('result-datatables_length');
			if(!empty($_GET['search_keyword'])) $search_keyword = $this->input->get('search_keyword');
			
		}
		
		$data['search_per_page'] = $search_per_page;
		$data['search_keyword'] = $search_keyword;
		
		$per_page = $search_per_page; //10
		
		$limit_cond = $per_page;
		
		if($page>1)
		{
			$page = $page - 1;
			if($page==0) $page = 1;
			$page = $per_page * $page;
		}
		else
			$page = 0;
	
		$data['page'] = $page;
		
		$per_page_cond = $page;
		
		$conditions['search_keyword'] = $search_keyword;
		
		$total_record = $this->food_request_model->get_result($conditions,'','', true );
		$data['result'] = $this->food_request_model->get_result($conditions,$per_page_cond,$limit_cond);
		
		$pagination_base_url = base_url().'admin/food_request/index';
		$pagination_total_rows = $total_record;
		$pagination_per_page = $per_page;
		$pagination_suffix = $suffix;
		$pagination_uri_segment  = 3;
		$pagination_page  = $page;
		$pagination_showing_total_record  = count($data['result']);
		
		$data['pagination'] = set_pagination_helper($pagination_base_url, $pagination_total_rows, $pagination_per_page, $pagination_page, $pagination_suffix, $pagination_uri_segment, $pagination_showing_total_record);
		
		$this->load->view('admin/food_request_list',$data);
	}
	
	public function delete($id)
	{
	
		if(empty($id))
			redirect('admin/food_request');
		
		$where_cond = "in_id = '".$id."' AND flg_is_delete = 0";
		$select_cond = "*";
		$result = $this->common_model->select_records('tbl_food_request',$where_cond,$select_cond,'','','','',true);
		
		if(empty($result))
			redirect('admin/food_request');
		
		$update_data['flg_is_delete'] 	 = '1';
		
		$this->common_model->edit_record('tbl_food_request', $update_data, "in_id = '".$id."'");
		
		$this->session->set_flashdata('success_message',"Deleted Success");
		redirect('admin/food_request');
	}
	
	public function delete_all()
	{
		if(!empty($_POST['result_id']))
		{
			$results= explode(',',$_POST['result_id']);	
			for($i=0;$i<count($results); $i++)
			{
				$result_id = $results[$i];
				
				$where_cond = "in_id = '".$id."' AND flg_is_delete = 0";
				$select_cond = "*";
				
				$result = $this->common_model->select_records('tbl_food_request',$where_cond,$select_cond,'','','','',true);
				
				if(!empty($result))
				{
					$update_data['flg_is_delete'] 	 = '1';
					$this->common_model->edit_record('tbl_food_request', $update_data, "in_id = '".$result_id."'");
				}
			}
			
			$array = array('result'=>'1', 'message'=>"Deleted Success");
			echo json_encode($array);
		}
		else
		{
			$array = array('result'=>'0', 'message'=>"Choose Record");
			echo json_encode($array);
		}
	}

	public function change_status()
	{
		if(!empty($this->input->post())){
		
			$result_id = $this->input->post('result_id');
			$status_id = $this->input->post('status_id');
			
			$update_data['in_status'] = $status_id;
			$this->common_model->edit_record('tbl_food_request', $update_data, "in_id = '".$result_id."'");
			
			$array = array('result'=>'1', 'message'=>"Status Changed!");
			echo json_encode($array);
		}
		else
		{
			$array = array('result'=>'0', 'message'=>"Choose Record");
			echo json_encode($array);
		}
	}

}