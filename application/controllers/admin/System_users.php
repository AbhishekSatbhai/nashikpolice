<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class System_users extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('system_user_model');
		$this->load->model('user_role_model');
		
		is_admin_logged_in();
	}
	
	public function index()
	{	
		$data = array();
		
		$conditions = array();
		$limit_cond = '';
		$perpage_cond = '';
		$conditions['master_user'] = 1;
		$data['result'] = $this->system_user_model->get_user_data($conditions,$perpage_cond,$limit_cond);
		
		$this->load->view('admin/system_user_list',$data);
	}
	
	public function delete($user_id)
	{
		if(empty($user_id))
			redirect('admin/system_users');
		
		$where_cond = "in_user_id = '".$user_id."' AND flg_is_delete = 0 AND in_master_user = 0";
		$select_cond = "*";
		
		$total_record = $this->system_user_model->select_records($where_cond,$select_cond,'','','','','',true);
		
		if(empty($total_record))
			redirect('admin/system_users');
		
		$update_data['flg_is_delete'] 	 = '1';
		
		$this->system_user_model->edit_record($update_data,"in_user_id = '".$user_id."'");
		$this->session->set_flashdata('success_message','Employee deleted successfully.');
		redirect('admin/system_users');
	}
	
	public function add()
	{	
		$data = array();
		
		$data['roles']  = $this->user_role_model->get_user_roles();
		
		if(!empty($_POST))
		{
			$post_data =  $this->security->xss_clean($_POST);	
			
			$this->form_validation->set_rules('st_name','name','required', 'Please enter name.');
			
			if(!empty($post_data['st_username']))
			{
				$this->form_validation->set_rules('st_username','user name','callback_username_exist');
			}
			
			if(!empty($post_data['new_password']))
			{
				$this->form_validation->set_rules('new_password','new password','required|min_length[3]');
			}
			
			if(!empty($post_data['st_email']))
			{
				$this->form_validation->set_rules('st_email','email','valid_email');
			}
			
			if(!empty($_FILES['avatar']['name']))
			{
				$this->form_validation->set_rules('avatar','avatar','callback_avatar_validation');
			}
			
			if($this->form_validation->run()==true)
			{
				
				$insert_data['st_name'] = $post_data['st_name'];
				$insert_data['st_email'] = $post_data['st_email'];
				$insert_data['in_role_id'] = $post_data['in_role_id'];
				
				if(!empty($post_data['st_username']))
				$insert_data['st_username'] = $post_data['st_username'];
				
				if(!empty($post_data['new_password']))
				$insert_data['st_password'] = $post_data['new_password'];
				
				$insert_data['flg_is_active'] = 1;
				$insert_data['flg_is_delete'] = 0;
				$insert_data['dt_added_date'] = date('Y-m-d H:i:s');
			
				
				if(!empty($_FILES['avatar']['name']))
				{
					$explode = explode('.',$_FILES['avatar']['name']);
					$ext = end($explode);
					$file_name = time().".".$ext;
					$insert_data['st_image'] = $file_name;
					
					// UPLOAD Main Image
					$config['file_name'] = $file_name;
					$config['upload_path'] = getcwd()."/media/employee_images/";
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					$this->upload->do_upload('avatar');
					
					//-- RESIZE --//
					$config2['overwrite'] = TRUE;
                    $config2['source_image'] = getcwd()."/media/employee_images/".$file_name;
                    $config2['maintain_ratio'] = TRUE;
                    $config2['width'] = 160;
                    $config2['height'] = 160;
                    $this->load->library('image_lib',$config2);
					
					$this->image_lib->resize();
				}
								
				$this->system_user_model->insert_record($insert_data);
				
				$this->session->set_flashdata(array('success_message'=>'New Employee Added Successfuly.'));				
				redirect('admin/system_users');
			}
		}
		
		$this->load->view('admin/system_user_add',$data);
	}
	
	public function edit($user_id = '')
	{	
		$data = array();
		
		if(empty($user_id))
			redirect('admin/system_users');
		
		$data['user_id'] = $user_id;
		
		$conditions = array();
		$conditions['user_id'] = $user_id;
		$limit_cond = '1';
		$perpage_cond = '0';
		
		$result = $this->system_user_model->get_user_data($conditions,$perpage_cond,$limit_cond);
		
		if(!empty($result))
		$data['result'] = $result[0];
		else
		redirect('admin/system_users');
		
		$data['roles']  = $this->user_role_model->get_user_roles();
		
		if(!empty($_POST))
		{
			$post_data =  $this->security->xss_clean($_POST);	
			
			$this->form_validation->set_rules('st_name','name','required', 'Please enter name.');
			
			if(!empty($post_data['st_email']))
			{
				$this->form_validation->set_rules('st_email','email','valid_email');
			}
			
			if(!empty($post_data['st_username']))
			{
				$this->form_validation->set_rules('st_username','user name','callback_username_exist');
			}
			
			if(!empty($post_data['new_password']) || !empty($post_data['confirm_password']))
			{
				$this->form_validation->set_rules('new_password','new password','required|min_length[3]');
				$this->form_validation->set_rules('confirm_password','confirm password','required|match[new_password]');
			}
			
			if(!empty($_FILES['avatar']['name']))
			{
				$this->form_validation->set_rules('avatar','avatar','callback_avatar_validation');
			}
			
			if($this->form_validation->run()==true)
			{
				
				if(!empty($post_data['st_name'])) 		$update_data['st_name'] = $post_data['st_name'];
				if(!empty($post_data['st_email'])) 		$update_data['st_email'] = $post_data['st_email'];
				if(!empty($post_data['in_role_id'])) 	$update_data['in_role_id'] = $post_data['in_role_id'];
				if(!empty($post_data['st_username'])) 	$update_data['st_username'] = $post_data['st_username'];
				if(!empty($post_data['new_password'])) 	$update_data['st_password'] = $post_data['new_password'];
			
				
				if(!empty($_FILES['avatar']['name']))
				{
					$explode = explode('.',$_FILES['avatar']['name']);
					$ext = end($explode);
					$file_name = time().".".$ext;
					$update_data['st_image'] = $file_name;
					
					// UPLOAD Main Image
					$config['file_name'] = $file_name;
					$config['upload_path'] = getcwd()."/media/employee_images/";
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					$this->upload->do_upload('avatar');
					
					//-- RESIZE --//
					$config2['overwrite'] = TRUE;
                    $config2['source_image'] = getcwd()."/media/employee_images/".$file_name;
                    $config2['maintain_ratio'] = TRUE;
                    $config2['width'] = 160;
                    $config2['height'] = 160;
                    $this->load->library('image_lib',$config2);
					
					$this->image_lib->resize();
					
					if(!empty($result[0]->st_image))
					{
						unlink(getcwd()."/media/employee_image/".$result[0]->st_image);
					}
					
				}				
				
				$this->system_user_model->edit_record($update_data,"in_user_id = '".$user_id."'");
				
				$this->session->set_flashdata(array('success_message'=>'User profile updated successfully.'));				
				redirect('admin/system_users/edit/'.$user_id);
			}
		}
		
		$this->load->view('admin/system_user_profile',$data);
	}
	
	function username_exist()
	{
		$user_id = $this->input->post('user_id');
		$user_name = $this->input->post('st_username');
		
		if(!empty($user_name))
		{
			$where_cond = "st_username = '".$user_name."' AND flg_is_active = 1 AND flg_is_delete = 0";
			if(!empty($user_id))  $where_cond .= " AND in_user_id  != '".$user_id."'";
			
			$select_cond = "in_user_id";
			$limit_cond = '1';
			$perpage_cond = '0';
			$data['user_details'] = $this->system_user_model->select_records($where_cond,$select_cond,'','',$perpage_cond,$limit_cond);
			
			if(!empty($data['user_details']))
			{
				$this->form_validation->set_message('username_exist', 'Username already exist.');
				return FALSE;
			}
			else 
			{
				return true;
			}
		}
		else
		{
			$this->form_validation->set_message('username_exist', 'Please enter username.');
			return FALSE;
		}
	}
	
	function avatar_validation()
	{
		$explode = explode('.',$_FILES['avatar']['name']);
		$ext = strtolower(end($explode));
		$ext_array = array('jpg','jpeg','png','gif');
		if(!in_array($ext,$ext_array))
		{
			$this->form_validation->set_message('avatar_validation', 'Profile Picture should be jpg/jpeg/png/gif.');
			return FALSE; 
		}
		else
			return TRUE;
	}
	
	public function status($id)
	{
		if(empty($id))
			redirect('admin/system_users');
		
		$where_cond = "in_user_id = '".$id."' AND flg_is_delete = 0";
		$select_cond = "flg_is_active";
		
		$result = $this->system_user_model->select_records($where_cond,$select_cond,'','','','','');
		
		if(empty($result))
			redirect('admin/system_users');
		
		if($result[0]->flg_is_active==1)
		{
			$this->session->set_flashdata('success_message','User Deactivated!');
			$update_data['flg_is_active'] 	 = '0';
		}
		else
		{
			$this->session->set_flashdata('success_message','User Activated!');
			$update_data['flg_is_active'] 	 = '1';
		}
		
		$this->system_user_model->edit_record($update_data,"in_user_id = '".$id."'");
		
		redirect('admin/system_users');
	}
}

