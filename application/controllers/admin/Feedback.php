<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Feedback extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('feedback_model');
		is_admin_logged_in();
	}
	
	public function index($page = 1)
		{
		$data = array();
		$conditions = array();
		
		$suffix = '';
		
		$search_per_page = '100';
		$search_keyword = '';
		
		if(!empty($_GET))
		{
				$suffix = '?'.http_build_query($_GET);
			if(!empty($_GET['result-datatables_length'])) $search_per_page = $this->input->get('result-datatables_length');
			if(!empty($_GET['search_keyword'])) $search_keyword = $this->input->get('search_keyword');
		}
		
		$data['search_per_page'] = $search_per_page;
		$data['search_keyword'] = $search_keyword;
		
		$per_page = $search_per_page; //10
		
		$limit_cond = $per_page;
		
		if($page>1)
		{
			$page = $page - 1;
			if($page==0) $page = 1;
			$page = $per_page * $page;
		}
		else
			$page = 0;
	
		$data['page'] = $page;
		
		$per_page_cond = $page;
		
		$conditions['search_keyword'] = $search_keyword;
		
		$total_record = $this->feedback_model->get_feedback($conditions,'','', true );
		$data['result'] = $this->feedback_model->get_feedback($conditions,$per_page_cond,$limit_cond);
	if(!empty($_GET['id'])) $in_user_type = $this->input->get('id');
		
		$pagination_base_url = base_url().'admin/feedback/index';
		$pagination_total_rows = $total_record;
		$pagination_per_page = $per_page;
		$pagination_suffix = $suffix;
		$pagination_uri_segment  = 3;
		$pagination_page  = $page;
		$pagination_showing_total_record  = count($data['result']);
		
		$data['pagination'] = set_pagination_helper($pagination_base_url, $pagination_total_rows, $pagination_per_page, $pagination_page, $pagination_suffix, $pagination_uri_segment, $pagination_showing_total_record);
		//print_r($data['result']); exit;
		$this->load->view('admin/feedback_list',$data);
	}
	
	public function add()
		{
		$data = array();
		
		if(!empty($_POST))
		{
				$post_data =  $this->security->xss_clean($_POST);
			$insert_data['in_id'] = $post_data['in_id'];
			
			$this->form_validation->set_rules('st_name','name','required', 'Please enter name');
			
			if(!empty($post_data['st_message']))
			{
				$this->form_validation->set_rules('st_message','message','required', 'please enter message!');
			}
			
			if(!empty($_FILES['st_image']['name']))
			{
				$this->form_validation->set_rules('st_image','st_image','callback_avatar_validation');
			}
			
			if($this->form_validation->run()==true)
			{
				
				$insert_data['st_name'] = $post_data['st_name'];
				$insert_data['st_address'] = $post_data['st_address'];
				$insert_data['st_message'] = $post_data['st_message'];
				$insert_data['flg_is_delete'] = 0;
				$insert_data['flg_is_active'] = 1;
				//$insert_data['in_user_type'] = $post_data['in_feedback_id'];
				
				if(!empty($_FILES['st_image']['name']))
				{
					$explode = explode('.',$_FILES['st_image']['name']);
					$ext = end($explode);
					$file_name = time().".".$ext;
					$insert_data['st_image'] = $file_name;
					
					// UPLOAD Main Image
					$config['file_name'] = $file_name;
					$config['upload_path'] = getcwd()."/media/feedback_images/";
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					$this->upload->do_upload('st_image');
					
					//-- RESIZE --//
					// $config2['overwrite'] = TRUE;
//                $config2['source_image'] = getcwd()."/media/user_images/".$file_name;
//                $config2['maintain_ratio'] = TRUE;
//                $config2['width'] = 160;
//                $config2['height'] = 160;
//                $this->load->library('image_lib',$config2);
					
					// $this->image_lib->resize();
				}
				
				
				$this->feedback_model->insert_record($insert_data);
				
				$this->session->set_flashdata(array('success_message'=>'New Feedback Added Successfully.'));
				
				redirect('admin/feedback');
			}
		}
		
			$data['results'] = $this->feedback_model->get_feedback();
		
	
		$this->load->view('admin/feedback_add',$data);
	}
	
	public function edit($id = '')
		{
		$data = array();
		
		if(empty($id))
			redirect('admin/feedback');
		
		$data['id'] = $id;
		
		$conditions = array();
		$conditions['in_id'] = $id;
		$limit_cond = '1';
		$perpage_cond = '0';
		
		$result = $this->feedback_model->get_feedback($conditions,$perpage_cond,$limit_cond);
		
		if(!empty($result))
		$data['result'] = $result[0];
		else
		redirect('admin/feedback');
		
		if(!empty($_POST))
		{
				$post_data =  $this->security->xss_clean($_POST);
			
			$this->form_validation->set_rules('st_name','name','required', 'Please enter name');
			
			if(!empty($post_data['st_message']))
			{
				$this->form_validation->set_rules('st_message','message','required', 'please enter a message!');
			}
			
			if(!empty($_FILES['st_image']['name']))
			{
				$this->form_validation->set_rules('st_image','st_image','callback_avatar_validation');
			}
			
			if($this->form_validation->run()==true)
			{
			
				$update_data['st_name'] = $post_data['st_name'];
				$update_data['st_address'] = $post_data['st_address'];
				$update_data['st_message'] = $post_data['st_message'];
				if(!empty($post_data['flg_is_active'])) $update_data['flg_is_active'] = $post_data['flg_is_active']; else $update_data['flg_is_active'] = 0;
				
				if(!empty($_FILES['st_image']['name']))
				{
					$explode = explode('.',$_FILES['st_image']['name']);
					$ext = end($explode);
					$file_name = time().".".$ext;
					$update_data['st_image'] = $file_name;
					
					// UPLOAD Main Image
					$config['file_name'] = $file_name;
					$config['upload_path'] = getcwd()."/media/feedback_images/";
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					$this->upload->do_upload('st_image');
					
					//-- RESIZE --//
					// $config2['overwrite'] = TRUE;
					// $config2['source_image'] = getcwd()."/media/distributor_images/".$file_name;
					// $config2['maintain_ratio'] = FALSE;
					// $config2['width'] = 160;
					// $config2['height'] = 160;
					// $this->load->library('image_lib',$config2);
					
					// $this->image_lib->resize();
					
					if(!empty($result[0]->st_image))
					{
						unlink(getcwd()."/media/feedback_images/".$result[0]->st_image);
					}
				}
				
				$this->feedback_model->edit_record($update_data,"md5(in_id) = '".$id."'");
				
				$this->session->set_flashdata(array('success_message'=>'Feedback Updated Successfully.'));
				redirect('admin/feedback');
			}
		}
		
		$data['results'] = $this->feedback_model->get_feedback();
		
		$this->load->view('admin/feedback_edit',$data);
	}
	
	public function delete($id)
	{
		if(empty($id))
			redirect('admin/feedback');
		
		$where_cond = "md5(in_id) = '".$id."' AND flg_is_delete = 0";
		$select_cond = "*";
		
		$total_record = $this->feedback_model->select_records($where_cond,$select_cond,'','','','','',true);
		
		if(empty($total_record))
			redirect('admin/feedback');
		
				$update_data['flg_is_delete'] 	 = '1';
		
		$this->feedback_model->edit_record($update_data,"md5(in_id) = '".$id."'");
		$this->session->set_flashdata('success_message','Feedback Deleted Successfully.');
		redirect('admin/feedback');
	}
	
	public function status($id)
	{
		if(empty($id))
			redirect('admin/feedback');
		
		$where_cond = "md5(in_id) = '".$id."' AND flg_is_delete = 0";
		$select_cond = "flg_is_active";
		
		$result = $this->feedback_model->select_records($where_cond,$select_cond,'','','','','');
		
		if(empty($result))
			redirect('admin/feedback');
		
		if($result[0]->flg_is_active==1)
		{
			$this->session->set_flashdata('success_message','Feedback Deactivated!');
					$update_data['flg_is_active'] 	 = '0';
		}
		else
		{
			$this->session->set_flashdata('success_message','Feedback Activated!');
					$update_data['flg_is_active'] 	 = '1';
		}
		
		$this->feedback_model->edit_record($update_data,"md5(in_id) = '".$id."'");
		
		redirect('admin/feedback');
	}
	
	function avatar_validation()
	{
		$explode = explode('.',$_FILES['st_image']['name']);
		$ext = strtolower(end($explode));
		$ext_array = array('jpg','jpeg','png','gif');
		if(!in_array($ext,$ext_array))
		{
			$this->form_validation->set_message('avatar_validation', 'Image should be jpg/jpeg/png/gif.');
			return FALSE;
		}
		else
			return TRUE;
	}
	
	function get_feedback()
	{
		$get_value = strtolower(urldecode($_GET['q']));
		
		$where_cond = "flg_is_delete = 0 AND (st_name LIKE '%".$get_value."%' OR st_address LIKE '%".$get_value."%')";
		$select_cond = "st_name, in_id, st_address, st_message, st_image";
		
		$array = array();
		
		$feedback_details = $this->feedback_model->select_records($where_cond,$select_cond,'','','','','');
		if(!empty($feedback_details))
		{
			foreach($feedback_details as $row)
			{
				$array[] = $row->st_name."|".$row->in_id."|".$row->st_address."|".$row->st_message;
			}
		}
		
		if(empty($array)) $array[] = $get_value."|0";
		echo json_encode($array);
	}
}