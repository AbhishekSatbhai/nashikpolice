<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('system_user_model');
		$this->load->model('login_model');
		$this->load->model('food_request_model');
		$this->load->model('supplier_request_model');
		$this->load->model('delivery_model');
		$this->load->model('item_model');
		
		is_admin_logged_in();		
	}
	
	public function index()
	{	
		$data = array();
		$data['todays'] = date('Y-m-d');
		$data['total_food_request'] = $this->food_request_model->get_result('','','',true);
		$data['total_supplier_request'] = $this->supplier_request_model->get_result('','','',true);
		$data['total_delivery_request'] = $this->delivery_model->get_result('','','',true);
		$data['total_items'] = $this->item_model->get_result('','','',true);
		$data['total_completed_request'] = $this->supplier_request_model->get_completed('','','',true);
		$this->load->model('get_item');
		$page = 0;	
	
		$data['page'] = $page;
		
		$per_page_cond = $page;
		$limit_cond = 10;
		$conditions = array();
		//$total_record = $this->item_model->get_result($conditions,'','', true );
		$data['result'] = $this->item_model->get_result($conditions,$per_page_cond,$limit_cond);
		//$data['result'] = $this->get_item->get_list();
		$this->load->view('admin/dashboard',$data);
	}
	
	public function authenticate()
	{
		$data = array();
		if(!empty($_POST))
		{
			$post_data =  $this->security->xss_clean($_POST);	
			
			$this->form_validation->set_rules('username','username','required', 'Please enter Username');
			$this->form_validation->set_rules('password','password','required', 'Please enter Password');
			$this->form_validation->set_rules('valid_user','valid_user','callback_valid_user');
			
			if($this->form_validation->run()==true)
			{
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				
				$where_cond = "st_username = '".$username."' AND st_password = '".$password."'";
				$select_cond = "in_user_id, st_name, st_image, in_role_id, in_master_user";
				$limit_cond = '1';
				$perpage_cond = '0';
				$user_details = $this->login_model->select_records($where_cond,$select_cond,'','',$perpage_cond,$limit_cond);
				
				$this->session->set_userdata(array('authenticate_user'=>true,'authenticate_user_id'=>$user_details[0]->in_user_id));
				
				$data['result'] = 1;
				$data['message'] = 'You have authenticated!';
			}
			else
			{
				$data['result'] = 0;
				$data['message'] = $this->form_validation->error_array();
			}
		}
		
		echo json_encode($data);
	}
	
	function valid_user()
	{	
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		if(!empty($username) && !empty($username))
		{
			$where_cond = "st_username = '".$username."' AND st_password = '".$password."'";
			$select_cond = "in_user_id, flg_is_active, flg_is_delete";
			$limit_cond = '1';
			$perpage_cond = '0';
			$data['user_details'] = $this->login_model->select_records($where_cond,$select_cond,'','',$perpage_cond,$limit_cond);
			
			if(!empty($data['user_details']) && ($data['user_details'][0]->flg_is_active==0 || $data['user_details'][0]->flg_is_delete == 1))
			{
				$this->form_validation->set_message('valid_user', 'Your account has been deactivated, please contact to admin.');
				return FALSE;
			}
			if(empty($data['user_details']))
			{
				$this->form_validation->set_message('valid_user', 'Invalid username and password.');
				return FALSE;
			}
			else 
			{
				return true;
			}
		}
	}
}

