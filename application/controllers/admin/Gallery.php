<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gallery extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('gallery_model');
		
		is_admin_logged_in();
	}
	
	public function index($page = 1)
		{
		$data = array();
		$conditions = array();
		
		$suffix = '';
		
		$search_per_page = '100';
		$search_keyword = '';
		
		if(!empty($_GET))
		{
				$suffix = '?'.http_build_query($_GET);
			if(!empty($_GET['result-datatables_length'])) $search_per_page = $this->input->get('result-datatables_length');
			if(!empty($_GET['search_keyword'])) $search_keyword = $this->input->get('search_keyword');
			if(!empty($_GET['cid'])) $in_category= $this->input->get('cid');
			if(!empty($_GET['pid'])) $in_category_id = $this->input->get('pid');
	
		}
		
		$data['search_per_page'] = $search_per_page;
		$data['search_keyword'] = $search_keyword;
		
		$per_page = $search_per_page; //10
		
		$limit_cond = $per_page;
		
		if($page>1)
		{
			$page = $page - 1;
			if($page==0) $page = 1;
			$page = $per_page * $page;
		}
		else
			$page = 0;
	
		$data['page'] = $page;
		
		$per_page_cond = $page;
		
		$conditions['search_keyword'] = $search_keyword;
		$conditions['in_category'] = $in_category;
		$conditions['in_category_id'] = $in_category_id;
		
		$total_record = $this->gallery_model->get_gallery($conditions,'','', true );
		$data['result'] = $this->gallery_model->get_gallery($conditions,$per_page_cond,$limit_cond);
		
		$pagination_base_url = base_url().'gallery/index';
		$pagination_total_rows = $total_record;
		$pagination_per_page = $per_page;
		$pagination_suffix = $suffix;
		$pagination_uri_segment  = 3;
		$pagination_page  = $page;
		$pagination_showing_total_record  = count($data['result']);
		
		$data['pagination'] = set_pagination_helper($pagination_base_url, $pagination_total_rows, $pagination_per_page, $pagination_page, $pagination_suffix, $pagination_uri_segment, $pagination_showing_total_record);
		
		$this->load->view('admin/gallery_list',$data);
	}
	
	public function add()
		{
		$data = array();
		
		if(!empty($_POST))
		{
				$post_data =  $this->security->xss_clean($_POST);
			
			$insert_data['in_category'] = $post_data['in_category'];
			$insert_data['in_category_id'] = $post_data['in_category_id'];
			$insert_data['flg_is_delete'] = 0;
			$insert_data['flg_is_active'] = 1;
			if(!empty($_FILES['st_image']['name']))
				{
					$explode = explode('.',$_FILES['st_image']['name']);
					$ext = end($explode);
					$file_name = time().".".$ext;
					$insert_data['st_image'] = $file_name;
					
					// UPLOAD Main Image
					$config['file_name'] = $file_name;
					$config['upload_path'] = getcwd()."/media/gallary/";
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					$this->upload->do_upload('st_image');
					
					
					//-- RESIZE --//
					// $config2['overwrite'] = TRUE;
//                $config2['source_image'] = getcwd()."/media/gallery/".$file_name;
//                $config2['maintain_ratio'] = TRUE;
					// $config2['width'] = 50;
//                $config2['height'] = 50;
//                $this->load->library('image_lib',$config2);
					// $this->image_lib->resize();
				}
		
			$this->gallery_model->insert_record($insert_data);
				
			$this->session->set_flashdata(array('success_message'=>'New Gallery Added Successfuly.'));
				}
	
		$this->load->view('admin/gallery_add',$data);
		}

	public function delete($id)
	{
		if(empty($id))
			redirect('admin/gallery');
		
		$where_cond = "in_id= '".$id."' AND flg_is_delete = 0";
		$select_cond = "*";
		
		$total_record = $this->gallery_model->select_records($where_cond,$select_cond,'','','','','',true);
		
		if(empty($total_record))
			redirect('admin/gallery');
		
				$update_data['flg_is_delete'] 	 = '1';
		
		$this->gallery_model->edit_record($update_data,"in_id = '".$id."'");
		$this->session->set_flashdata('success_message','Gallary Deleted successfully.');
		redirect($_SERVER['HTTP_REFERER']);
	}

	
	}
?>