<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('setting_model');
		
		is_admin_logged_in();
	}
	
	public function index()
	{	
		$data = array();
		
		$where_cond = "in_id = 1";
		$select_cond = "*";
		$result = $this->setting_model->select_records($where_cond,$select_cond,'','','','');
		$data['result'] = $result[0];
		
		if(!empty($_POST))
		{
			$post_data =  $this->security->xss_clean($_POST);	
				
			$update_data['st_organisation_name'] = $post_data['st_organisation_name'];
			$update_data['st_address'] = $post_data['st_address'];
			$update_data['st_contact_numbers'] = $post_data['st_contact_numbers'];
			$update_data['st_slogan'] = $post_data['st_slogan'];
			$update_data['st_title'] = $post_data['st_title'];
			$update_data['st_email'] = $post_data['st_email'];
			$update_data['st_twitter_link'] = $post_data['st_twitter_link'];
			$update_data['st_facebook_link'] = $post_data['st_facebook_link'];
			$update_data['st_instagram_link'] = $post_data['st_instagram_link'];
			$update_data['st_linkedin_link'] = $post_data['st_linkedin_link'];
			$update_data['st_youtube_link'] = $post_data['st_youtube_link'];
			
			
			if(!empty($_FILES['st_logo']['name']))
				{
					$explode = explode('.',$_FILES['st_logo']['name']);
					$ext = end($explode);
					$file_name = time().".".$ext;
					$update_data['st_logo'] = $file_name;
					
					// UPLOAD Main Image
					$config['file_name'] = $file_name;
					$config['upload_path'] = getcwd()."/media/logo/";
					$config['allowed_types'] = 'gif|jpg|png|jpeg';
					
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					$this->upload->do_upload('st_logo');
					
					//-- RESIZE --//
					$config2['overwrite'] = TRUE;
                    $config2['source_image'] = getcwd()."/media/logo/".$file_name;
                    $config2['maintain_ratio'] = TRUE;
                    $config2['width'] = 160;
                    $config2['height'] = 160;
                    $this->load->library('image_lib',$config2);
					
					$this->image_lib->resize();
				}
						
			$this->setting_model->edit_record($update_data,"in_id = 1");
			
			$this->session->set_flashdata(array('success_message'=>'Settings updated successfully.'));				
			redirect('admin/settings');
			
		}
		$this->load->view('admin/setting',$data);
	}
}

