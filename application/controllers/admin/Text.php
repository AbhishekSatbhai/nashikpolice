<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Text extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('text_model');
		
		is_admin_logged_in();
	}
	
public function index($page = 1)
		{
		$data = array();
		$conditions = array();
		
		$suffix = '';
		
		$search_per_page = '100';
		$search_keyword = '';
		
		if(!empty($_GET))
		{
				$suffix = '?'.http_build_query($_GET);
			if(!empty($_GET['result-datatables_length'])) $search_per_page = $this->input->get('result-datatables_length');
			if(!empty($_GET['search_keyword'])) $search_keyword = $this->input->get('search_keyword');
		}
		
		$data['search_per_page'] = $search_per_page;
		$data['search_keyword'] = $search_keyword;
		
		$per_page = $search_per_page; //10
		
		$limit_cond = $per_page;
		
		if($page>1)
		{
			$page = $page - 1;
			if($page==0) $page = 1;
			$page = $per_page * $page;
		}
		else
			$page = 0;
	
		$data['page'] = $page;
		
		$per_page_cond = $page;
		
		$conditions['search_keyword'] = $search_keyword;
		
		$total_record = $this->text_model->get_text($conditions,'','', true );
		$data['result'] = $this->text_model->get_text($conditions,$per_page_cond,$limit_cond);
		
		$pagination_base_url = base_url().'admin/text/index';
		$pagination_total_rows = $total_record;
		$pagination_per_page = $per_page;
		$pagination_suffix = $suffix;
		$pagination_uri_segment  = 3;
		$pagination_page  = $page;
		$pagination_showing_total_record  = count($data['result']);
		
		$data['pagination'] = set_pagination_helper($pagination_base_url, $pagination_total_rows, $pagination_per_page, $pagination_page, $pagination_suffix, $pagination_uri_segment, $pagination_showing_total_record);
		
		$this->load->view('admin/text_list',$data);
	}
	
	public function add()
		{
		$data = array();
		
		if(!empty($_POST))
		{
				$post_data =  $this->security->xss_clean($_POST);
			
			$this->form_validation->set_rules('st_title','name','required', 'Please Enter Title');
			if($this->form_validation->run()==true)
			{
				
			
			$insert_data['st_title'] = $post_data['st_title'];
			$insert_data['st_description'] = $post_data['st_description'];
			$insert_data['flg_is_delete'] = 0;
			$insert_data['flg_is_active'] = 1;
		
			$this->text_model->insert_record($insert_data);
				
			$this->session->set_flashdata(array('success_message'=>'New Text Added Successfully.'));
							redirect('admin/text');
				}
		}
		
		$this->load->view('admin/text_add',$data);
	}
	public function edit($id = '')
		{
		$data = array();
		
		if(empty($id))
			redirect('admin/text');
		
		$data['id'] = $id;
		
		$conditions = array();
		$conditions['text_id'] = $id;
		$limit_cond = '1';
		$perpage_cond = '0';
		
		$result = $this->text_model->get_text($conditions,$perpage_cond,$limit_cond);
		
		if(!empty($result))
		$data['result'] = $result[0];
		else
		redirect('admin/text');
		
		if(!empty($_POST))
		{
				$post_data =  $this->security->xss_clean($_POST);
			
			$this->form_validation->set_rules('st_title','name','required', 'Please enter Titile');
		
			
			if($this->form_validation->run()==true)
			{
			
				$update_data['st_title'] = $post_data['st_title'];
				$update_data['st_description'] = $post_data['st_description'];
				
				if(!empty($post_data['flg_is_active'])) $update_data['flg_is_active'] = $post_data['flg_is_active']; else $update_data['flg_is_active'] = 0;
				
				
				
				$this->text_model->edit_record($update_data,"in_id = '".$id."'");
				
								$this->session->set_flashdata(array('success_message'=>'Text Updated Successfully.'));
				redirect('admin/text');
			}
		}
		
		$this->load->view('admin/text_edit',$data);
	}
	
	public function delete($id)
	{
		if(empty($id))
			redirect('admin/text');
		
		$where_cond = "in_id = '".$id."' AND flg_is_delete = 0";
		$select_cond = "*";
		
		$total_record = $this->text_model->select_records($where_cond,$select_cond,'','','','','',true);
		
		if(empty($total_record))
			redirect('admin/text');
		
			$update_data['flg_is_delete'] 	 = '1';
		
		$this->text_model->edit_record($update_data,"in_id = '".$id."'");
		$this->session->set_flashdata('success_message','Text Deleted Successfully.');
		redirect('admin/text');
	}
	
	public function status($id)
	{
		if(empty($id))
			redirect('admin/text');
		
		$where_cond = "in_id = '".$id."' AND flg_is_delete = 0";
		$select_cond = "flg_is_active";
		
		$result = $this->text_model->select_records($where_cond,$select_cond,'','','','','');
		
		if(empty($result))
			redirect('admin/text');
		
		if($result[0]->flg_is_active==1)
		{
			$this->session->set_flashdata('success_message','Text Deactivated!');
				$update_data['flg_is_active'] 	 = '0';
		}
		else
		{
			$this->session->set_flashdata('success_message','Text Activated!');
				$update_data['flg_is_active'] 	 = '1';
		}
		
		$this->text_model->edit_record($update_data,"in_id = '".$id."'");
		
		redirect('admin/text');
	}
	
	
	}