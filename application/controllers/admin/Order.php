<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('applib');
		$this->load->model('item_model');
		$this->load->model('bill_model');
		$this->load->model('customer_model');
		$this->load->model('setting_model');
		
		is_logged_in();
	}
	
	public function index($page = 1)
	{	
		$data = array();
		$conditions = array();
		
		$suffix = '';
		
		$search_per_page = '100';
		$search_keyword = '';
		
		$data['customer_id'] = '';
		$data['from_date'] = '';
		$data['to_date'] = '';
		
		
		if(!empty($_GET))
		{
			$suffix = '?'.http_build_query($_GET);	
			if(!empty($_GET['result-datatables_length'])) $search_per_page = $this->input->get('result-datatables_length');
			if(!empty($_GET['search_keyword'])) $search_keyword = $this->input->get('search_keyword');
			$data['customer_id'] = $_GET['customer_id'];
			if(!empty($_GET['from_date'])) $data['from_date'] = $_GET['from_date'];
			if(!empty($_GET['to_date'])) $data['to_date'] = $_GET['to_date'];
		}
		
		$data['search_per_page'] = $search_per_page;
		$data['search_keyword'] = $search_keyword;
		
		
		$per_page = $search_per_page; //10
		
		$limit_cond = $per_page;
		
		if($page>1)
		{
			$page = $page - 1;
			if($page==0) $page = 1;
			$page = $per_page * $page;
		}
		else
		$page = 0;	
	
		$data['page'] = $page;
		
		$per_page_cond = $page;
		
		$conditions['search_keyword'] = $search_keyword;
		$conditions['customer_id'] = $data['customer_id'];
		$conditions['from_date'] = (!empty($data['from_date'])) ? date('Y-m-d',strtotime($data['from_date'])) : '';
		$conditions['to_date'] = (!empty($data['to_date'])) ? date('Y-m-d',strtotime($data['to_date'])) : '';
		
		$total_record = $this->bill_model->get_orders($conditions,'','', true );
		$data['result'] = $this->bill_model->get_orders($conditions,$per_page_cond,$limit_cond);
		
		$pagination_base_url = base_url().'bill/index';
		$pagination_total_rows = $total_record;
		$pagination_per_page = $per_page;
		$pagination_suffix = $suffix;
		$pagination_uri_segment  = 3;
		$pagination_page  = $page;
		$pagination_showing_total_record  = count($data['result']);
		
		$data['pagination'] = set_pagination_helper($pagination_base_url, $pagination_total_rows, $pagination_per_page, $pagination_page, $pagination_suffix, $pagination_uri_segment, $pagination_showing_total_record);
		
		$this->load->view('order_list',$data);
	}
	
	public function new_order($bill_id = '', $action = '')
	{	
		$data = array();
		
		$data['order_details'] = '';
		$data['bill_id'] = '';
		
		$data['action'] = $action;
		
		$data['customer_data'] = '';
		if(!empty($_GET['customer_id']))
		{
			$customer_data = $this->customer_model->get_customers(array('customer_id'=>md5($_GET['customer_id'])));
			if(!empty($customer_data))
			$data['customer_data'] = $customer_data[0];
		}
		
		if(!empty($bill_id))
		{
			$order_details = $this->bill_model->get_order_details(array('bill_id'=>$bill_id));
			$data['order_details'] = $order_details[0];
			$data['bill_id'] = $bill_id;
		}
		
		if(!empty($this->input->post()))
		{
			$post_data =  $this->input->post(); //$this->security->xss_clean($_POST);	
			
			$this->form_validation->set_rules('st_name','name','required', 'Please enter name');
			
			if(!empty($post_data['st_email']))
			{
				$this->form_validation->set_rules('st_email','email','valid_email', 'Email not valid!');
			}
			
			if($this->form_validation->run()==true)
			{
				
				if(!empty($post_data['st_name'])) 				$customer_apply_data['st_name'] = $post_data['st_name'];
				if(!empty($post_data['st_address'])) 			$customer_apply_data['st_address'] = $post_data['st_address'];
				if(!empty($post_data['st_mobile_number'])) 		$customer_apply_data['st_mobile_number'] = $post_data['st_mobile_number'];
				if(!empty($post_data['st_telephone_number'])) 	$customer_apply_data['st_telephone_number'] = $post_data['st_telephone_number'];
				if(!empty($post_data['st_email'])) 				$customer_apply_data['st_email'] = $post_data['st_email'];
				$customer_apply_data['in_branch_id'] = $this->session->userdata('branch_id');
			
				if(!empty($post_data['customer_id']))
				{
					$this->customer_model->edit_record($customer_apply_data,"in_id = '".$post_data['customer_id']."'");
				}
				else
				{
					$post_data['customer_id'] = $this->customer_model->insert_record($customer_apply_data);
				}
				
				$bill_apply_data['in_customer_id'] = $post_data['customer_id'];
				if(!empty($post_data['dt_bill_date'])) $bill_apply_data['dt_bill_date'] = date('Y-m-d',strtotime($post_data['dt_bill_date']));
				$bill_apply_data['st_invoice_number'] = $post_data['st_invoice_number'];
				$bill_apply_data['in_branch_id'] = $this->session->userdata('branch_id');
				$bill_apply_data['bill_type'] = 1;
				
				if(!empty($bill_id))
				{
					$this->bill_model->edit_record($bill_apply_data, 'in_id = '.$bill_id, 'tbl_bill');
				}
				else
				{
					$bill_id = $this->bill_model->insert_record($bill_apply_data, 'tbl_bill');
				}
				
				$this->session->set_flashdata(array('success_message'=>'Bill has been processed! Please add Items.'));				
				redirect('order/new_order_item/'.$bill_id.'/'.$action);
			}
			
			
		}
		
		$this->load->view('bill_new_order',$data);
	}
	
	function avatar_validation()
	{
		$explode = explode('.',$_FILES['st_image']['name']);
		$ext = strtolower(end($explode));
		$ext_array = array('jpg','jpeg','png','gif');
		if(!in_array($ext,$ext_array))
		{
			$this->form_validation->set_message('avatar_validation', 'Image should be jpg/jpeg/png/gif.');
			return FALSE; 
		}
		else
			return TRUE;
	}
	
	public function new_order_item($bill_id = '', $action = '')
	{	
		$data = array();
		
		$data['action'] = $action;
		
		if(empty($bill_id))
		{
			redirect('order/new_order');
			exit;
		}
		else
		{
			$order_details = $this->bill_model->get_order_details(array('bill_id'=>$bill_id));
			if(empty($order_details))
			{
				redirect('order/new_order');
				exit;
			}
			else
			{
				$data['order_details'] = $order_details[0];
				$data['bill_id'] = $bill_id;
			}
			
			$data['order_item_details'] = $this->bill_model->get_order_item_details(array('bill_id'=>$bill_id));
		}
		
		if(!empty($this->input->post()))
		{
			$sale_details = $this->bill_model->select_records('tbl_bill', "flg_is_delete = 0 AND flg_status = 2 AND bill_type = 1", "st_invoice_number", '', '', 0, 1, 'in_id', false, 'DESC');
			
			$post_data =  $this->input->post(); //$this->security->xss_clean($_POST);	
			
			if(!empty($post_data['dt_bill_date'])) $apply_data['dt_bill_date'] = date('Y-m-d',strtotime($post_data['dt_bill_date']));
			
			$apply_data['flg_status'] = 2;
			
			if(empty($data['order_details']->st_invoice_number))
			{
				$count_number = end(explode('-',$sale_details[0]->st_invoice_number));
				$count_number = $count_number + 1;
				$this->bill_model->edit_record(array('st_invoice_number'=>date('Y').'-'.$count_number), 'in_id = '.$bill_id, 'tbl_bill');
			}
			
			$this->session->set_flashdata(array('success_message'=>'Order Completed.'));				
			redirect('order/final_order/'.$bill_id);
			
		}
		
		$data['company_details'] = get_invoice_settings();
		
		$this->load->view('bill_new_order_item',$data);
	}
	
	public function add_order_selected_item()
	{
		if(!empty($_POST))
		{
			$apply_bill_data['in_bill_id'] = $_POST['bill_id'];
			$apply_bill_data['in_item_id'] = $_POST['item_id'];
			$apply_bill_data['in_qty'] = $_POST['in_qty'];
			
			$item_data = $this->item_model->get_items(array('item_id'=>$_POST['item_id']));
			if(!empty($item_data))
			{	
				$apply_item_data['in_qty_stock'] = $item_data[0]->in_qty_stock - $_POST['in_qty'];	
				if($apply_item_data['in_qty_stock']>=0)
				$this->item_model->edit_record($apply_item_data, "in_id = ".$apply_bill_data['in_item_id'], "tbl_items");
			}
			
			$this->bill_model->insert_record($apply_bill_data, "tbl_bill_details");
			$this->session->set_flashdata(array('success_message'=>'Item added successdully!.'));				
			redirect('order/new_order_item/'.$apply_bill_data['in_bill_id'].'/'.$_POST['action']);
		}
	}
	
	public function final_order($bill_id = '')
	{
		$order_details = $this->bill_model->get_order_details(array('bill_id'=>$bill_id));
		if(empty($order_details))
		{
			redirect('order/new_order');
			exit;
		}
		else
		{
			$data['order_details'] = $order_details[0];
			$data['bill_id'] = $bill_id;
		}
		
		$data['company_details'] = get_invoice_settings();
		
		$this->load->view('bill_final_order',$data);
	}
	
	public function delete($id)
	{
		if(empty($id))
			redirect('order');
		
		$where_cond = "in_id = '".$id."' AND flg_is_delete = 0";
		$select_cond = "*";
		
		$total_record = $this->bill_model->select_records('tbl_bill',$where_cond,$select_cond,'','','','','',true);
		
		if(empty($total_record))
			redirect('order');
		
		$update_data['flg_is_delete'] 	 = '1';
		
		$order_item_details = $this->bill_model->get_order_item_details(array('bill_id'=>$id));
		if(!empty($order_item_details))
		{
			foreach($order_item_details as $row)
			{
				$item_data = $this->item_model->get_items(array('item_id'=>$row->item_id));
				if(!empty($item_data))
				{
					$apply_item_data['in_qty_stock'] = $item_data[0]->in_qty_stock + $row->in_qty;	
		
					if($apply_item_data['in_qty_stock']>=0)
						$this->item_model->edit_record($apply_item_data, "in_id = ".$row->item_id, "tbl_items");
				}

				$this->bill_model->edit_record($update_data,"in_id = '".$row->in_details_id."'",'tbl_bill_details');
			}
			
		}
		
		$this->bill_model->edit_record($update_data,"in_id = '".$id."'",'tbl_bill');
		
		
		
		$this->session->set_flashdata('success_message','Order deleted successfully.');
		redirect('order');
	}
	
	public function delete_order_item($id='', $bill_id='', $action='')
	{
	
		$order_item_details = $this->bill_model->get_order_item_details(array('bill_id'=>$bill_id, 'bill_details_id'=>$id));
		if(!empty($order_item_details))
		{
			$item_data = $this->item_model->get_items(array('item_id'=>$order_item_details[0]->item_id));
			if(!empty($item_data))
			{
				$apply_item_data['in_qty_stock'] = $item_data[0]->in_qty_stock + $order_item_details[0]->in_qty;	
	
				if($apply_item_data['in_qty_stock']>=0)
					$this->item_model->edit_record($apply_item_data, "in_id = ".$order_item_details[0]->item_id, "tbl_items");
			}
			
		}
		
		if(!empty($id))
		{
			$update_data['flg_is_delete'] = 1;
			$this->bill_model->edit_record($update_data,"in_id = '".$id."'",'tbl_bill_details');
		}
		
		$this->session->set_flashdata('success_message','Order Item deleted!.');
		redirect('order/new_order_item/'.$bill_id.'/'.$action);
		
	}
	
	public function invoice($bill_id)
	{
		$order_details = $this->bill_model->get_order_details(array('bill_id'=>$bill_id));
		if(empty($order_details))
		{
			redirect('order');
			exit;
		}
		else
		{
			$data['order_details'] = $order_details[0];
			$data['bill_id'] = $bill_id;
		}
		
		$data['order_item_details'] = $this->bill_model->get_order_item_details(array('bill_id'=>$bill_id));
		
		$where_cond = "in_id = ".$this->session->userdata('branch_id');
		$select_cond = "*";
		$result = $this->setting_model->select_records($where_cond,$select_cond,'','','','');
		$data['invoice_data'] = $result[0];
		
		$html = $this->load->view('invoice_pdf', $data, true);

		$pdf = array(
			"html"      => $html,
			"title"     => "Invoice_".time(),
			"author"    => "Gurudatta E & S",
			"creator"   => "Gurudatta E & S",
			"filename"  => "Invoice_".time().'.pdf',
			//"badge"     => config_item('display_invoice_badge')
		);
	
		$this->applib->create_pdf($pdf);
	}
	
	public function view($bill_id='')
	{
		$order_details = $this->bill_model->get_order_details(array('bill_id'=>$bill_id));
		if(empty($order_details))
		{
			redirect('order/new_order');
			exit;
		}
		else
		{
			$data['order_details'] = $order_details[0];
			$data['bill_id'] = $bill_id;
		}
		
		$data['order_item_details'] = $this->bill_model->get_order_item_details(array('bill_id'=>$bill_id));
		
		$this->load->view('bill_view',$data);
	}
	
	public function export()
	{
	
		require(getcwd().'/plugins/phpexcel/PHPExcel.php');
		require(getcwd().'/plugins/phpexcel/PHPExcel/IOFactory.php');
		
		$customer_id = '';
		$from_date = '';
		$to_date = '';
		$search_keyword = '';
		
		if(!empty($_GET))
		{
			if(!empty($_GET['search_keyword'])) $search_keyword = $_GET('search_keyword');
			if(!empty($_GET['customer_id'])) $customer_id = $_GET['customer_id'];
			if(!empty($_GET['from_date'])) $from_date = $_GET['from_date'];
			if(!empty($_GET['to_date'])) $to_date = $_GET['to_date'];
		}
		
		$conditions['search_keyword'] = $search_keyword;
		$conditions['customer_id'] = $customer_id;
		$conditions['from_date'] = (!empty($from_date)) ? date('Y-m-d',strtotime($from_date)) : '';
		$conditions['to_date'] = (!empty($to_date)) ? date('Y-m-d',strtotime($to_date)) : '';
		
		$bill_details = $this->bill_model->get_orders($conditions);
		
		$bill_paid_details = $this->bill_model->get_bill_paid($conditions);
		
		$objPHPExcel = new PHPExcel();
		
		$objPHPExcel->setActiveSheetIndex(0);  //set first sheet as active
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Customer');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Date');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Bill Number');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Amount');
		
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => 'F28A8C'
			)
		));
		$objPHPExcel->getActiveSheet()->getStyle('B1')->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => 'F28A8C'
			)
		));
		$objPHPExcel->getActiveSheet()->getStyle('C1')->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => 'F28A8C'
			)
		));
		$objPHPExcel->getActiveSheet()->getStyle('D1')->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => 'F28A8C'
			)
		));
		
		$i = 2;			
		$total_bill_amount = 0;
		
		if($bill_details != NULL):
			foreach($bill_details as $row)
			{
				$customer_name=(!empty($row->st_name)) ? $row->st_name : '';
				$date=($row->dt_bill_date!='0000-00-00') ? date('d-m-Y',strtotime($row->dt_bill_date)) : '';
				$amount=(!empty($row->fl_net_amount)) ? $row->fl_net_amount : '';
				$bill_number=(!empty($row->st_invoice_number)) ? $row->st_invoice_number : '';
				
				$total_bill_amount = $total_bill_amount + $amount;
				
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $customer_name);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $date);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $bill_number);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $amount);
				
				$i++;
			}
		endif;
		
		$opening_balance = 0;
		
		if(!empty($customer_id))
		{
			$opening_balance = $opening_balance + get_customer_opening_balance($customer_id);
		}
		else
		{
			$opening_balance = $this->bill_model->get_total_opening_balance();
		}
				
		$i++; 
		
		$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => 'F28A8C'
			)
		));
		
		
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Total Bill Amount');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $total_bill_amount);
		
		$i++; $i++; 
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'Customer');
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, 'Date');
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Payment Type');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'Amount');
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, 'Remark');
		
		$objPHPExcel->getActiveSheet()->getStyle('A'.$i)->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => 'F28A8C'
			)
		));
		$objPHPExcel->getActiveSheet()->getStyle('B'.$i)->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => 'F28A8C'
			)
		));
		$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => 'F28A8C'
			)
		));
		$objPHPExcel->getActiveSheet()->getStyle('D'.$i)->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => 'F28A8C'
			)
		));
		$objPHPExcel->getActiveSheet()->getStyle('E'.$i)->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => 'F28A8C'
			)
		));
		
		$i++;
		
		$total_paid_amount = 0;
		$total_discount_amount = 0;
		
		if($bill_paid_details != NULL):
			foreach($bill_paid_details as $row)
			{
				$customer_name=(!empty($row->st_name)) ? $row->st_name : '';
				$date=($row->dt_paid_date!='0000-00-00') ? date('d-m-Y',strtotime($row->dt_paid_date)) : '';
				$amount=(!empty($row->fl_total_paid)) ? $row->fl_total_paid : '';
				
				$payment_type_array = array('1'=>'Cash', '2'=>'Bank Transfer', '3'=>'Cheque', '4'=>'Others');
				
				$payment_type=(!empty($row->in_payment_type)) ? $payment_type_array[$row->in_payment_type] : '';
				
				$st_remark=(!empty($row->st_remark)) ? $row->st_remark : '';
				
				$total_paid_amount = $total_paid_amount + $amount;
				$total_discount_amount = $total_discount_amount + $row->fl_discount;

				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $customer_name);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $date);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $payment_type);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $amount);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $st_remark);
				
				$i++;
			}
		endif;	
		
		$i++; $i++;
		
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Total Paid Amount');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $total_paid_amount);
		
		$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => 'F28A8C'
			)
		));
		
		$i++; $i++;
		
		$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => 'F28A8C'
			)
		));
		
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Total Open Balance');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $opening_balance);
		
		$i++; $i++;
		
		$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => 'F28A8C'
			)
		));
		
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Total Discount');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $total_discount_amount);
		
		$i++; $i++;
		
		$total_balance_amount = (($total_bill_amount + $opening_balance) - $total_discount_amount) - $total_paid_amount;
		
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'Total Balance Amount');
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $total_balance_amount);
		
		$objPHPExcel->getActiveSheet()->getStyle('C'.$i)->getFill()->applyFromArray(array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array(
				 'rgb' => 'F28A8C'
			)
		));
		
		// Save a xls file
        $filename = time().'_bill_report';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$filename.'.xls"');
        header('Cache-Control: max-age=0');
        
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        $objWriter->save('php://output');
        unset($this->objWriter);
        unset($this->objWorksheet);
        unset($this->objReader);
        unset($this->objPHPExcel);
		
	}
}

