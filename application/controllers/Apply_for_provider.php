<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Apply_for_provider extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Get_item');
	}
	
	public function index()
	{	
		$data = array();
		$data['get_help_type'] = get_help_type();
		$data['get_tehsil'] = get_tehsil();
		$data['police_stations'] = get_police_station();
		// $this->load->view('apply_for_provider',$data);
	}
	
	public function action()
	{
		$data = array();
		if(!empty($this->input->post()))
		{
			$post_data =  $this->input->post();	
			
			$this->form_validation->set_rules('st_name','title','required', lang('Please enter Name'));
			$this->form_validation->set_rules('st_address','title','required', lang('Please enter your Location'));
			$this->form_validation->set_rules('st_telephone','title','required', lang('Please enter Contact Number'));
			$this->form_validation->set_rules('st_email','title','required', lang('Please enter valid email address'));
			$this->form_validation->set_rules('st_help_type','title','required', lang('Please select your Help'));
			$this->form_validation->set_rules('st_answer','title','required', lang('Please enter help count'));
			/*$this->form_validation->set_rules('st_help_place','title','required', lang('Please enter help place'));
			$this->form_validation->set_rules('st_tehsil','title','required', lang('Please select Tehsil'));
			$this->form_validation->set_rules('st_identity_number','title','required', lang('Please enter your ID number'));
			//$this->form_validation->set_rules('st_identity_image','title','required', lang('Please upload your ID photo'));
			
			/*if($_SERVER['HTTP_HOST']!="localhost"){
				//verify captcha
				if(!empty($_POST['g-recaptcha-response']))
				{
					$recaptcha_secret = GOOGLE_CAPTCHA_SECRETE_KEY;
					
					$captcha_data = array(
					'secret' => $recaptcha_secret,
					'response' => $_POST['g-recaptcha-response'],
					);

					$verify = curl_init();
					curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
					curl_setopt($verify, CURLOPT_POST, true);
					curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($captcha_data));
					curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);

					$output = curl_exec($verify);
					curl_close($verify);

					$response = json_decode($output);

					$captcha_response = true;
					
					if (isset($response->success) && $response->success){ $captcha_response = true; }
					else{
						$captcha_response = false;
					}
				}
				else
				{
					$captcha_response = false;
				}
			}
			else */$captcha_response = true;
			
			if($captcha_response == false)
			{
				$data['result'] = 0;
				$data['robot_message'] = "You are a robot";
			}
			
			if($this->form_validation->run()==true && $captcha_response)
			{
				$insert_data['food1'] = $post_data['food1'];
				$insert_data['food1_qty'] = $post_data['food1_qty'];
				$this->Get_item->update_values($insert_data['food1'],$insert_data['food1_qty']);
				$this->Get_item->update_received_qty($insert_data['food1'],$insert_data['food1_qty']);
				$insert_data['food2'] = $post_data['food2'];
				$insert_data['food2_qty'] = $post_data['food2_qty'];
				$this->Get_item->update_values($insert_data['food2'],$insert_data['food2_qty']);
				$this->Get_item->update_received_qty($insert_data['food2'],$insert_data['food2_qty']);
				$insert_data['grocery1'] = $post_data['grocery1'];
				$insert_data['grocery1_qty'] = $post_data['grocery1_qty'];
				$this->Get_item->update_values($insert_data['grocery1'],$insert_data['grocery1_qty']);
				$this->Get_item->update_received_qty($insert_data['grocery1'],$insert_data['grocery1_qty']);
				$insert_data['grocery2'] = $post_data['grocery2'];
				$insert_data['grocery2_qty'] = $post_data['grocery2_qty'];
				$this->Get_item->update_values($insert_data['grocery2'],$insert_data['grocery2_qty']);
				$this->Get_item->update_received_qty($insert_data['grocery2'],$insert_data['grocery2_qty']);
				$insert_data['medical1'] = $post_data['medical1'];
				$insert_data['medical1_qty'] = $post_data['medical1_qty'];
				$this->Get_item->update_values($insert_data['medical1'],$insert_data['medical1_qty']);
				$this->Get_item->update_received_qty($insert_data['medical1'],$insert_data['medical1_qty']);
				$insert_data['medical2'] = $post_data['medical2'];
				$insert_data['medical2_qty'] = $post_data['medical2_qty'];
				$this->Get_item->update_values($insert_data['medical2'],$insert_data['medical2_qty']);
				$this->Get_item->update_received_qty($insert_data['medical2'],$insert_data['medical2_qty']);
				$insert_data['dry_fruits1'] = $post_data['dry_fruits1'];
				$insert_data['dry_fruits1_qty'] = $post_data['dry_fruits1_qty'];
				$this->Get_item->update_values($insert_data['dry_fruits1'],$insert_data['dry_fruits1_qty']);		
				$this->Get_item->update_received_qty($insert_data['dry_fruits1'],$insert_data['dry_fruits1_qty']);		
				$insert_data['dry_fruits2'] = $post_data['dry_fruits2'];
				$insert_data['dry_fruits2_qty'] = $post_data['dry_fruits2_qty'];
				$this->Get_item->update_values($insert_data['dry_fruits2'],$insert_data['dry_fruits2_qty']);
				$this->Get_item->update_received_qty($insert_data['dry_fruits2'],$insert_data['dry_fruits2_qty']);
				$insert_data['other'] = $post_data['other'];
				$insert_data['other_qty'] = $post_data['other_qty'];

				$insert_data['st_name'] = $post_data['st_name'];
				$insert_data['st_telephone'] = $post_data['st_telephone'];
				$insert_data['st_address'] = $post_data['st_address'];
				$insert_data['st_email'] = $post_data['st_email'];
				$insert_data['st_help_type'] = "NULL";
				//$insert_data['st_help_remark'] = $post_data['st_help_remark'];
				$insert_data['st_tehsil'] = $post_data['st_tehsil'];
				$insert_data['st_help_date'] = (!empty($post_data['st_help_date'])) ? date('Y-m-d', strtotime($post_data['st_help_date'])) : '';
				$insert_data['st_remark'] = $post_data['st_remark'];
				$insert_data['st_answer'] = $post_data['st_answer'];
				//$insert_data['st_identity_number'] = $post_data['st_identity_number'];
				$insert_data['st_nearest_police_station'] = $post_data['st_nearest_police_station'];
				$insert_data['dt_added_date'] = date('Y-m-d H:i:s');
				
				if(!empty($_FILES['st_identity_image']['name']))
				{
					$explode = explode('.',$_FILES['st_identity_image']['name']);
					$ext = end($explode);
					$slak = random_string('numeric', 6);
					$file_name = $slak."_".time().".".$ext;
					$insert_data['st_identity_image'] = $file_name;
					
					// UPLOAD Main Image
					$config['file_name'] = $file_name;
					$config['upload_path'] = getcwd()."/media/provider_images/";
					$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
					
					
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					
					$this->upload->do_upload('st_identity_image');
				}
				
				$this->common_model->insert_record("tbl_supplier_request", $insert_data);
				
				
				$this->session->set_flashdata(array('success_message'=>lang('Your Request Submitted Successfully')));
				
				$data['result'] = 1;
				$data['message'] = 'Your request has been submitted. Thank you!';



			}
			else
			{
				$data['result'] = 0;
				$data['message'] = validation_errors();
			}
		}
		
		echo json_encode($data);

		// send Email containt from here

		$email_data['to'] = $insert_data['st_email'];
		$email_data['subject'] = 'Nashik City Police';

		$mailContent = "<div>
			<img width='100%' height='30%' src='http://nashikpolice.webphoenix.in/assets/front/images/Nashik-Police-logo.jpg'>
	
			<h1 style='text-align:center;'>Thank you for your donation</h1>

			<div style='text-align:center;'><p style='color:blue;'> Dear ";
	$mailContent .= $insert_data['st_name'] ;
									
	$mailContent .=",Thank you for your request as Proider.</p></div></div>";
	$mailContent .="<div style='text-align:center;'><p style='color:green;'>Your donation is as follows : <br></p></div><br>";
	$mailContent .= "<table border=1 width='60%' style='margin:auto;text-align:center;'><tr><th>Item</th><th>Quantity</th><tr>";
		if ($insert_data['food1_qty'] != 0){
			$mailContent .= "<tr><td>";
			$mailContent .= $insert_data['food1'];
			$mailContent .= "</td><td>";
			$mailContent .= $insert_data['food1_qty'];
			$mailContent .= "</td></tr>";
		}

		if ($insert_data['food2_qty'] != 0){
			$mailContent .= "<tr><td>";
			$mailContent .= $insert_data['food2'];
			$mailContent .= "</td><td>";
			$mailContent .= $insert_data['food2_qty'];
			$mailContent .= "</td></tr>";
		}

		if ($insert_data['grocery1_qty'] != 0){
			$mailContent .= "<tr><td>";
			$mailContent .= $insert_data['grocery1'];
			$mailContent .= "</td><td>";
			$mailContent .= $insert_data['grocery1_qty'];
			$mailContent .= "</td></tr>";
		}

		if ($insert_data['grocery2_qty'] != 0){
			$mailContent .= "<tr><td>";
			$mailContent .= $insert_data['grocery2'];
			$mailContent .= "</td><td>";
			$mailContent .= $insert_data['grocery2_qty'];
			$mailContent .= "</td></tr>";
		}

		if ($insert_data['medical1_qty'] != 0){
			$mailContent .= "<tr><td>";
			$mailContent .= $insert_data['medical1'];
			$mailContent .= "</td><td>";
			$mailContent .= $insert_data['medical1_qty'];
			$mailContent .= "</td></tr>";
		}

		if ($insert_data['medical2_qty'] != 0){
			$mailContent .= "<tr><td>";
			$mailContent .= $insert_data['medical2'];
			$mailContent .= "</td><td>";
			$mailContent .= $insert_data['medical2_qty'];
			$mailContent .= "</td></tr>";
		}

		if ($insert_data['dry_fruits1_qty'] != 0){
			$mailContent .= "<tr><td>";
			$mailContent .= $insert_data['dry_fruits1'];
			$mailContent .= "</td><td>";
			$mailContent .= $insert_data['dry_fruits1_qty'];
			$mailContent .= "</td></tr>";
		}

		if ($insert_data['dry_fruits2_qty'] != 0){
			$mailContent .= "<tr><td>";
			$mailContent .= $insert_data['dry_fruits2'];
			$mailContent .= "</td><td>";
			$mailContent .= $insert_data['dry_fruits2_qty'];
			$mailContent .= "</td></tr>";
		}

		if ($insert_data['other_qty'] != 0){
			$mailContent .= "<tr><td>";
			$mailContent .= $insert_data['other'];
			$mailContent .= "</td><td>";
			$mailContent .= $insert_data['other_qty'];
			$mailContent .= "</td></tr>";
		}



	$mailContent .= "</table><br>";

	$mailContent .= "<div style='text-align:right;'><p>Thank you...!!!</p><p style='color:blue;'>Nashik City Police</p></div>";

	$email_data['message'] = $mailContent;

	//send_email($email_data);
	
// End of send email containt

		
	}

}
?>