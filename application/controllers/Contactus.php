<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactus extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
	}
	
	public function index(){
		$data=array();		 
		$this->load->view('contact',$data);
	}
	
	public function action()
	{
		$data = array();
		if(!empty($this->input->post()))
		{
			$post_data =  $this->input->post();	
			
			$this->form_validation->set_rules('st_name','title','required', 'Please enter Name');
			$this->form_validation->set_rules('st_mobile_number','title','required', 'Please enter Contact Number');
			$this->form_validation->set_rules('st_email','title','valid_email', 'Please enter valid email address');
			$this->form_validation->set_rules('st_message','title','required', 'Please select your Message');
			
			if($this->form_validation->run()==true)
			{
				$insert_data['st_name'] = $post_data['st_name'];
				$insert_data['st_email'] = $post_data['st_email'];
				$insert_data['st_mobile_number'] = $post_data['st_mobile_number'];
				$insert_data['st_message'] = $post_data['st_message'];
				$insert_data['dt_added_date'] = date('Y-m-d H:i:s');
				
				$this->common_model->insert_record("tbl_contacts", $insert_data);
				
				$this->session->set_flashdata(array('success_message'=>'Your Feedback Submitted Successfully.'));
				
				$data['result'] = 1;
				$data['message'] = 'Your request has been submitted. Thank you!';
			}
			else
			{
				$data['result'] = 0;
				$data['message'] = validation_errors();
			}
		}
		
		echo json_encode($data);
	}
}
?>