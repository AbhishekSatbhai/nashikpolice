<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		if(!empty($this->session->userdata('language')))
		$this->lang->load('front', $this->session->userdata('language'));
		$this->load->model('item_model');
	}
	
    public function index($page = 1){


		$data=array();
		
		$conditions = array();
		
		$suffix = '';
		
		$search_per_page = '100';
		$search_keyword = '';
		
		if(!empty($_GET))
		{
			$suffix = '?'.http_build_query($_GET);	
			if(!empty($_GET['result-datatables_length'])) $search_per_page = $this->input->get('result-datatables_length');
			if(!empty($_GET['search_keyword'])) $search_keyword = $this->input->get('search_keyword');
		}
		
		$data['search_per_page'] = $search_per_page;
		$data['search_keyword'] = $search_keyword;
		
		$per_page = $search_per_page; //10
		
		$limit_cond = $per_page;
		
		if($page>1)
		{
			$page = $page - 1;
			if($page==0) $page = 1;
			$page = $per_page * $page;
		}
		else
		$page = 0;	
	
		$data['page'] = $page;
		
		$per_page_cond = $page;
		
		$conditions['search_keyword'] = $search_keyword;
		
		$total_record = $this->item_model->get_result($conditions,'','', true );
		$data['result'] = $this->item_model->get_result($conditions,$per_page_cond,$limit_cond);
		
		$pagination_base_url = base_url().'items/index';
		$pagination_total_rows = $total_record;
		$pagination_per_page = $per_page;
		$pagination_suffix = $suffix;
		$pagination_uri_segment  = 3;
		$pagination_page  = $page;
		$pagination_showing_total_record  = count($data['result']);
		
		$data['pagination'] = set_pagination_helper($pagination_base_url, $pagination_total_rows, $pagination_per_page, $pagination_page, $pagination_suffix, $pagination_uri_segment, $pagination_showing_total_record);
		

		$this->load->model('get_item');
		$data['desc_list'] = $this->get_item->get_list();
		$data['item_names'] = $this->get_item->get_names();
		// echo "<pre>";
		// print_r($list);


		$data['get_help_type'] = get_help_type();
		$data['get_tehsil'] = get_tehsil();
		$data['police_stations'] = get_police_station();
    	$this->load->view('apply_for_provider',$data);
	}
	public function change_language($langauge)
	{
		$this->session->set_userdata('language', $langauge);
		$this->config->set_item('language', $langauge);
		redirect('');
	}

}
?>