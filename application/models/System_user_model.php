	<?php

class System_user_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		$this->load->database();
		$this->table_name = 'tbl_system_user';
    }

	public function select_records($where=NULL,$select=NULL,$jointable=NULL,$joinkey=NULL,$per_page=NULL,$limit=NULL,$order_by=NULL, $count = false)
	{

		if($where!=NULL)
			$this->db->where($where);

		if($select!=NULL)
			$this->db->select($select, FALSE);

		if($jointable!=NULL && $joinkey!=NULL)
			$this->db->join($jointable, $joinkey);

		if($limit!=NULL || $per_page!=NULL)
			$this->db->limit($limit, $per_page);

		if($order_by!=NULL)
			$this->db->order_by($order_by);

		$query = $this->db->get($this->table_name);

		if($count==true)
		{	
			return $query->num_rows();
		}
		else
		{
			//echo $this->db->last_query(); exit;
			return $query->result();
		}
	}

	public function insert_record($insert_data)
	{
		$this->db->insert($this->table_name,$insert_data);
		return $this->db->insert_id();
	}

	public function edit_record($update_data,$cond)
	{
		return $this->db->update($this->table_name, $update_data, $cond);
	}

	public function delete_record($where)
	{
		$this->db->delete($this->table_name, $where); 
	}

	public function custom_query($query)
	{
		$query = $this->db->query($query);
		return $query->result();
	}

	public function count_all_record()
	{
		return $this->db->count_all($this->table_name);
	}
	
	public function get_user_data($conditions=NULL, $per_page=NULL, $limit=NULL, $count = false)
	{
		$this->db->select("TSU.in_user_id, TSU.st_name, TSU.st_email, TSU.st_image, TSU.flg_is_active, TSU.flg_is_delete, TSU.in_master_user, TSU.in_role_id, TSU.flg_logged, TSU.dt_last_logged,  TSU.st_username, TSUR.st_role_name", FALSE);
		$this->db->where("TSU.flg_is_delete = 0");
		
		if(!empty($conditions['user_id']))
		$this->db->where("TSU.in_user_id = '".$conditions['user_id']."'");
		
		if(!empty($conditions['master_user']))
		$this->db->where("TSU.in_master_user != 1");
		
		$this->db->from("tbl_system_user as TSU");
		$this->db->join("tbl_system_user_roles as TSUR", "TSUR.in_id = TSU.in_role_id", "left");
		
		if($limit!=NULL || $per_page!=NULL)
		$this->db->limit($limit, $per_page);
		
		$query = $this->db->get();
		
		//echo $this->db->last_query(); exit;
		
		if($count==true)
		{	
			return $query->num_rows();
		}
		else
		{
			
			return $query->result();
		}
	}

	

}



?>