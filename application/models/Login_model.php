<?php
class login_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		$this->load->database();
		$this->table_name = 'tbl_system_user';
    }

	public function select_records($where=NULL,$select=NULL,$jointable=NULL,$joinkey=NULL,$per_page=NULL,$limit=NULL,$order_by=NULL)
	{
		if($where!=NULL)
			$this->db->where($where);

		if($select!=NULL)
			$this->db->select($select);

		if($jointable!=NULL && $joinkey!=NULL)
			$this->db->join($jointable, $joinkey);

		if($limit!=NULL || $per_page!=NULL)
			$this->db->limit($limit, $per_page);

		if($order_by!=NULL)
			$this->db->order_by($order_by);

		$query = $this->db->get($this->table_name);

		return $query->result();
	}

	public function insert_record($insert_data)
	{
		$this->db->insert($this->table_name,$insert_data);
		return $this->db->insert_id();
	}

	public function edit_record($update_data,$cond)
	{
		return $this->db->update($this->table_name, $update_data, $cond);
	}

	public function delete_record($where)
	{
		$this->db->delete($this->table_name, $where); 
	}

	public function custom_query($query)
	{
		$query = $this->db->query($query);
		return $query->result();
	}

	public function count_all_record()
	{
		return $this->db->count_all($this->table_name);
	}

	

}



?>