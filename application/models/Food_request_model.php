<?php

class Food_request_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		$this->load->database();		
    }

	public function get_result($conditions=NULL, $per_page=NULL, $limit=NULL, $count = false)
	{
		$this->db->select("tbl1.*, tbl2.st_name as police_station", FALSE);
		
		$this->db->from("tbl_food_request as tbl1");
		$this->db->join("tbl_police_stations as tbl2", "tbl2.in_id = tbl1.st_nearest_police_station", 'left');
		
		$this->db->where("tbl1.flg_is_delete = 0");
		
		if(!empty($conditions['search_keyword']))
			$this->db->where("(tbl1.st_name LIKE '%".$conditions['search_keyword']."%' OR tbl1.st_address LIKE '%".$conditions['search_keyword']."%' OR tbl1.st_mobile_number LIKE '%".$conditions['search_keyword']."%')");	
		
		if(!empty($conditions['sort_by']) && !empty($conditions['sort_type']))
			$this->db->order_by($conditions['sort_by'], $conditions['sort_type']);
		
		
		if($limit!=NULL || $per_page!=NULL)
		$this->db->limit($limit, $per_page);
		
		$query = $this->db->get();
		
		//echo $this->db->last_query(); exit;
		
		if($count==true)
		{	
			return $query->num_rows();
		}
		else
		{
			
			return $query->result();
		}
	}
}

?>