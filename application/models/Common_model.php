<?php
class Common_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		$this->load->database();
    }

	public function select_records($from=NULL, $where=NULL,$select=NULL,$jointable=NULL,$joinkey=NULL,$per_page=NULL,$limit=NULL, $count=false, $order_by=NULL)
	{
		if($where!=NULL)
			$this->db->where($where);

		if($select!=NULL)
			$this->db->select($select);

		if($jointable!=NULL && $joinkey!=NULL)
			$this->db->join($jointable, $joinkey);

		if($limit!=NULL || $per_page!=NULL)
			$this->db->limit($limit, $per_page);

		if($order_by!=NULL)
			$this->db->order_by($order_by);

		$query = $this->db->get($from);

		if($count==true)
		{	
			return $query->num_rows();
		}
		else
		{
			//echo $this->db->last_query(); exit;
			return $query->result();
		}
	}

	public function insert_record($from, $insert_data)
	{
		$this->db->insert($from,$insert_data);
		return $this->db->insert_id();
	}

	public function edit_record($from, $update_data,$cond)
	{
		return $this->db->update($from, $update_data, $cond);
	}

	public function delete_record($from, $where)
	{
		$this->db->delete($from, $where); 
	}

	public function custom_query($query)
	{
		$query = $this->db->query($query);
		return $query->result();
	}

	public function count_all_record($from)
	{
		return $this->db->count_all($from);
	}
}



?>