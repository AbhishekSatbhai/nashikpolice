<?php
class User_model extends CI_Model
{
	function __construct()
{
parent::__construct();
		$this->load->database();
		$this->table_name = 'tbl_users';
}
	public function select_records($where=NULL,$select=NULL,$jointable=NULL,$joinkey=NULL,$per_page=NULL,$limit=NULL,$order_by=NULL, $count = false)
	{
		if($where!=NULL)
			$this->db->where($where);
		if($select!=NULL)
			$this->db->select($select, FALSE);
		if($jointable!=NULL && $joinkey!=NULL)
			$this->db->join($jointable, $joinkey);
		if($limit!=NULL || $per_page!=NULL)
			$this->db->limit($limit, $per_page);
		if($order_by!=NULL)
			$this->db->order_by($order_by);
		$query = $this->db->get($this->table_name);
		if($count==true)
			{
			return $query->num_rows();
		}
		else
		{
			//echo $this->db->last_query(); exit;
			return $query->result();
		}
	}
	public function insert_record($insert_data)
	{
		$this->db->insert($this->table_name,$insert_data);
		return $this->db->insert_id();
	}
	public function edit_record($update_data,$cond)
	{
		return $this->db->update($this->table_name, $update_data, $cond);
	}
	public function delete_record($where)
	{
		$this->db->delete($this->table_name, $where);
	}
	public function custom_query($query)
	{
		$query = $this->db->query($query);
		return $query->result();
	}
	public function count_all_record()
	{
		return $this->db->count_all($this->table_name);
	}
	
	public function get_customers($conditions=NULL, $per_page=NULL,$limit=NULL,$count = false)
	{
		$this->db->select("`in_id`, `st_name`, `st_address`, `st_image`, `st_mobile_number`,`st_agent`, `st_whatsup_number`, `st_telephone_number`, `st_email`, `dt_birth_date`, `flg_is_delete`, `flg_is_active`, `dt_added_date`,`in_distributor_id`,`in_agent_id`,`company_name`");
		$this->db->from("tbl_users");
		$this->db->where("flg_is_delete = 0 and in_user_type=1");
		
		if(!empty($conditions['search_keyword']))
		$this->db->where("(st_name LIKE '%".$conditions['search_keyword']."%' OR  st_mobile_number LIKE '%".$conditions['search_keyword']."%' OR st_agent LIKE '%".$conditions['search_keyword']."%' OR st_whatsup_number LIKE '%".$conditions['search_keyword']."%' OR st_telephone_number LIKE '%".$conditions['search_keyword']."%' OR st_email LIKE '%".$conditions['search_keyword']."%') ");
		
		if(!empty($conditions['customer_id']))
		$this->db->where("md5(in_id) = '".$conditions['customer_id']."'");
		

		if(!empty($conditions['in_id']))
		$this->db->where("in_id = '".$conditions['in_id']."'");

		if(!empty($conditions['in_agent_id']))
		$this->db->where("in_agent_id = '".$conditions['in_agent_id']."'");
		
		if(!empty($conditions['in_distributor_id']))
		$this->db->where("in_distributor_id = '".$conditions['in_distributor_id']."'");
		

		if(!empty($conditions['flg_is_active']))
		$this->db->where("flg_is_active = 1");
	
		if($limit!=NULL || $per_page!=NULL)
			$this->db->limit($limit, $per_page);
		$this->db->order_by("st_name","ASC");
		
		if(!empty($conditions['group_by']))
			$this->db->group_by($conditions['group_by']);
		
		$query = $this->db->get();
		
		if($count==true)
			{
			return $query->num_rows();
		}
		else
		{
			return $query->result();
		}
	}
	
}
?>