<?php

class Get_item extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		$this->load->database();
		$this->table_name = 'tbl_items';
    }

	function get_list() {
        $this->db->select("st_description");
        $this->db->from('tbl_items');
        $this->db->where('flg_is_delete','0');
        $this->db->group_by('st_description');
        $query = $this->db->get();
        return $query->result();
    }

    function get_names(){
        $this->db->select("st_name,st_description");
        $this->db->from('tbl_items');
        $this->db->where('flg_is_delete','0');
        $query = $this->db->get();
        return $query->result();
    }

    function update_values($name,$qty){ 
        $i=0;
        for($i=0 ; $i<$qty ; $i++){
            $this->db->set('in_qty_stock', 'in_qty_stock-1', FALSE);
            $this->db->where('st_name', $name);
            $this->db->update('tbl_items');
        }
        
    }

     public function update_received_qty($name,$qty){ 
            $i=0;
            for($i=0 ; $i<$qty ; $i++){
                $this->db->set('received_qty', 'received_qty+1', FALSE);
                $this->db->where('st_name', $name);
                $this->db->update('tbl_items');
            }

        }
        
    public function get_data($in_id){
                $this->db->select('*');
                $this->db->where('in_id', $in_id);
                $this->db->from('tbl_supplier_request');
                $query = $this->db->get();
                return $query->result();
    }

    public function get_other_name(){
                $this->db->select('other,other_qty');
                $this->db->from('tbl_supplier_request');
                $query = $this->db->get();
                return $query->result();
    }
}
?>