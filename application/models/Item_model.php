<?php

class Item_model extends CI_Model
{
	function __construct()
    {
        parent::__construct();
		$this->load->database();
    }

	public function get_result($conditions=NULL, $per_page=NULL,$limit=NULL,$count = false)
	{
		$this->db->select("tbl1.*");
		$this->db->from("tbl_items as tbl1");
		$this->db->where("tbl1.flg_is_delete = 0");
		
		if(!empty($conditions['search_keyword']))
		$this->db->where("(tbl1.st_name LIKE '%".$conditions['search_keyword']."%' OR tbl1.st_description LIKE '%".$conditions['search_keyword']."%' OR tbl1.fl_price LIKE '%".$conditions['search_keyword']."%') ");
		
		if(!empty($conditions['item_id']))
		$this->db->where("tbl1.in_id = '".$conditions['item_id']."'");
		
		if($limit!=NULL || $per_page!=NULL)
			$this->db->limit($limit, $per_page);

		$this->db->order_by("tbl1.st_name","ASC");
		
		$query = $this->db->get();
		
		if($count==true)
		{	
			return $query->num_rows();
		}
		else
		{
			return $query->result();
		}
	}
	
}



?>