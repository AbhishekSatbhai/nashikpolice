// ----------------------------------------------
// # Search
// ----------------------------------------------

(function () {
	var video = document.getElementById('video'),
	canvas = document.getElementById('canvas'),
	context = canvas.getContext('2d'), 
	photo = document.getElementById('photo'),
	vendorUrl = window.URL || window.webkitURL;
	picture = document.getElementById('picture'),
	
	navigator.getMedia = navigator.getUserMedia ||
						navigator.webkitGetUserMedia ||
						navigator.mozGetUserMedia ||
						navigator.msGetUserMedia;
							
	navigator.getMedia({
		video: true,
		audio: false
	}, function (stream){
		//video.src = vendorUrl.createObjectURL(stream);
		video.srcObject=stream;
		video.play();
	}, function (error){
		
	});
	
	document.getElementById('capture').addEventListener('click', function(){
		context.drawImage(video, 0, 0, 160, 160);
		photo.setAttribute('src', canvas.toDataURL('image/png'));
		picture.setAttribute('value', canvas.toDataURL('image/png'));
	})
}());